# qmini3d

#### 介绍
一个简单的固定渲染管线软渲染器，感谢韦易笑韦大的分享，这里只是移植到了Qt下，

#### 软件架构
软件架构说明


#### 子项目说明

1.  qmini3d：qt下的win32项目，交互器是键盘上的方向键。
2.  qmini3dgui：为了借用Qt的MouseEvent，加入了GUI，使用QWidget。
3.  qmini3dtrackbox: 封装好class的qmini3dgui，封装了QLabel和QWidget。此时的交互器只有2个固定方向，放大缩小。
4.  qmini3dtrackball：引入轨迹球交互器，方向是自由的。
5.  txtReader：txt解析器。

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
