#pragma once

#include <vector>
#include "assert.h"

#include "device_t.h"
#include "fishstruct.h"
#include "fishtransform.h"
#include "fishvertex.h"
#include "fishmatrix.h"


using namespace std;

class struct_demo
{
public:
    struct_demo();
    ~struct_demo();

    void matrix_set_lookat(matrix_t *m, const vector_t *eye, const vector_t *at, const vector_t *up);

    void device_init(device_t *device, int width, int height, void *fb);

    void device_destroy(device_t *device);

    void device_clear(device_t *device, int mode);

    void draw_box(device_t *device, float theta);

    void draw_plane(device_t *device, int a, int b, int c, int d);

    void device_draw_primitive(device_t *device, const vertex_t *v1,
                               const vertex_t *v2, const vertex_t *v3);

    void device_draw_line(device_t *device, int x1, int y1, int x2, int y2, IUINT32 c);

    void device_pixel(device_t *device, int x, int y, IUINT32 color);

    void camera_at_zero(device_t *device, float x, float y, float z);

};

