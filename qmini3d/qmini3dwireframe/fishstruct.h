#pragma once


typedef struct { float m[4][4]; } matrix_t;
typedef struct { float x, y, z, w; } vector_t;
typedef vector_t point_t;


//=====================================================================
// 坐标变换
//=====================================================================
typedef struct
{
    matrix_t world;         // 世界坐标变换
    matrix_t view;          // 摄影机坐标变换
    matrix_t projection;    // 投影变换
    matrix_t transform;     // transform = world * view * projection
    float w, h;             // 屏幕大小
}	transform_t;


//=====================================================================
// 几何计算：顶点、扫描线、边缘、矩形、步长计算
//=====================================================================
typedef struct { float r, g, b; } color_t;//颜色
typedef struct { float u, v; } texcoord_t;//纹理坐标
typedef struct { point_t pos; texcoord_t tc; color_t color; float rhw; } vertex_t;//顶点




