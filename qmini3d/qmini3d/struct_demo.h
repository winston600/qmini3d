#pragma once

#include <vector>
#include "assert.h"

#include "device_t.h"
#include "fishstruct.h"
#include "fishtransform.h"
#include "fishvertex.h"
#include "fishmatrix.h"

#include "DataCenter.h"


using namespace std;

class struct_demo
{
public:
    struct_demo();
    ~struct_demo();

    void draw_plane(device_t *device, int a, int b, int c, int d);
    void draw_box(device_t *device, float theta);

    void device_draw_primitive(device_t *device, const vertex_t *v1,
                               const vertex_t *v2, const vertex_t *v3);

    int trapezoid_init_triangle(trapezoid_t *trap, const vertex_t *p1,
                                const vertex_t *p2, const vertex_t *p3);

    void device_render_trap(device_t *device, trapezoid_t *trap);

    void device_draw_line(device_t *device, int x1, int y1, int x2, int y2, IUINT32 c);

    void device_pixel(device_t *device, int x, int y, IUINT32 color);

    void trapezoid_edge_interp(trapezoid_t *trap, float y);

    void trapezoid_init_scan_line(const trapezoid_t *trap, scanline_t *scanline, int y);

    void device_draw_scanline(device_t *device, scanline_t *scanline);

    IUINT32 device_texture_read(const device_t *device, float u, float v);

    void draw_colume(device_t *device, float x, float y, float z, float theta);

    void matrix_set_lookat(matrix_t *m, const vector_t *eye, const vector_t *at, const vector_t *up);

    void device_init(device_t *device, int width, int height, void *fb);

    void device_destroy(device_t *device);

    void device_set_texture(device_t *device, void *bits, long pitch, int w, int h);

    void device_clear(device_t *device, int mode);

    void camera_at_zero(device_t *device, float x, float y, float z);

    void init_texture(device_t *device);




};

