#pragma once
#include "fishstruct.h"
#include <vector>

#include <QObject>
#include <math.h>
#define PI 3.1415926

using namespace std;

class DataCenter : public QObject
{
    Q_OBJECT

public:
    static DataCenter *GetDataCenter();
    ~DataCenter();

    float point1_x, point1_y, point2_x, point2_y, point3_x, point3_y, point4_x, point4_y,
     point5_x, point5_y, point6_x, point6_y, point7_x, point7_y, point8_x, point8_y,
     point9_x, point9_y, point10_x, point10_y, point11_x, point11_y, point12_x, point12_y,
     point13_x, point13_y, point14_x, point14_y, point15_x, point15_y, point16_x, point16_y,
     point17_x, point17_y, point18_x, point18_y, point19_x, point19_y, point20_x, point20_y,
     point21_x, point21_y, point22_x, point22_y, point23_x, point23_y, point24_x, point24_y,
     point25_x, point25_y, point26_x, point26_y;

    vertex_t mesh2[52];

    void calpoints();

    vector< pair<float, float> >  cal_meshpoints(int side);

private:
    static DataCenter *datacenter;
    DataCenter(QObject *parent = 0);


};
