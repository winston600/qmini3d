#include "DataCenter.h"

DataCenter::DataCenter(QObject *parent)
    : QObject(parent)
{

    point1_x, point1_y, point2_x, point2_y, point3_x, point3_y, point4_x, point4_y,
            point5_x, point5_y, point6_x, point6_y, point7_x, point7_y, point8_x, point8_y,
            point9_x, point9_y, point10_x, point10_y, point11_x, point11_y, point12_x, point12_y,
            point13_x, point13_y, point14_x, point14_y, point15_x, point15_y, point16_x, point16_y,
            point17_x, point17_y, point18_x, point18_y, point19_x, point19_y, point20_x, point20_y,
            point21_x, point21_y, point22_x, point22_y, point23_x, point23_y, point24_x, point24_y,
            point25_x, point25_y, point26_x, point26_y = 0;

    calpoints();

}

DataCenter::~DataCenter()
{
}

DataCenter *DataCenter::datacenter = 0;

DataCenter *DataCenter::GetDataCenter()
{
    if (datacenter == 0)
    {
        datacenter = new DataCenter();
        ////qDebug()<<"DataControl created";
    }
    return datacenter;
}

void DataCenter::calpoints()
{
    vector< pair<float, float> > meshpoints = cal_meshpoints(26);

    point1_x = meshpoints.at(0).first;
    point1_y = meshpoints.at(0).second;
    point2_x = meshpoints.at(1).first;
    point2_y = meshpoints.at(1).second;
    point3_x = meshpoints.at(2).first;
    point3_y = meshpoints.at(2).second;
    point4_x = meshpoints.at(3).first;
    point4_y = meshpoints.at(3).second;
    point5_x = meshpoints.at(4).first;
    point5_y = meshpoints.at(4).second;
    point6_x = meshpoints.at(5).first;
    point6_y = meshpoints.at(5).second;
    point7_x = meshpoints.at(6).first;
    point7_y = meshpoints.at(6).second;
    point8_x = meshpoints.at(7).first;
    point8_y = meshpoints.at(7).second;
    point9_x = meshpoints.at(8).first;
    point9_y = meshpoints.at(8).second;
    point10_x = meshpoints.at(9).first;
    point10_y = meshpoints.at(9).second;
    point11_x = meshpoints.at(10).first;
    point11_y = meshpoints.at(10).second;
    point12_x = meshpoints.at(11).first;
    point12_y = meshpoints.at(11).second;

    point13_x = meshpoints.at(12).first;
    point13_y = meshpoints.at(12).second;
    point14_x = meshpoints.at(13).first;
    point14_y = meshpoints.at(13).second;
    point15_x = meshpoints.at(14).first;
    point15_y = meshpoints.at(14).second;
    point16_x = meshpoints.at(15).first;
    point16_y = meshpoints.at(15).second;
    point17_x = meshpoints.at(16).first;
    point17_y = meshpoints.at(16).second;
    point18_x = meshpoints.at(17).first;
    point18_y = meshpoints.at(17).second;
    point19_x = meshpoints.at(18).first;
    point19_y = meshpoints.at(18).second;
    point20_x = meshpoints.at(19).first;
    point20_y = meshpoints.at(19).second;
    point21_x = meshpoints.at(20).first;
    point21_y = meshpoints.at(20).second;
    point22_x = meshpoints.at(21).first;
    point22_y = meshpoints.at(21).second;
    point23_x = meshpoints.at(22).first;
    point23_y = meshpoints.at(22).second;
    point24_x = meshpoints.at(23).first;
    point24_y = meshpoints.at(23).second;
    point25_x = meshpoints.at(24).first;
    point25_y = meshpoints.at(24).second;
    point26_x = meshpoints.at(25).first;
    point26_y = meshpoints.at(25).second;

    mesh2[0] = { { point1_x,point1_y,-2,1 },{ 0, 0 },{ 1.0f, 0.2f, 0.2f }, 1 };
    mesh2[1] = { { point2_x, point2_y, -2, 1 },{ 0, 1 },{ 0.2f, 1.0f, 0.2f }, 1 };
    mesh2[2] = { { point3_x,point3_y,-2,1 },{ 1, 1 },{ 0.2f, 0.2f, 1.0f }, 1 };
    mesh2[3] = { { point4_x ,point4_y,-2,1 },{ 0, 1 },{ 0.2f, 1.0f, 0.2f }, 1 };
    mesh2[4] = { { point5_x,point5_y,-2,1 },{ 0, 0 },{ 1.0f, 0.2f, 0.2f }, 1 };

    mesh2[5] = { { point6_x ,point6_y,-2,1 },{ 0, 1 },{ 0.2f, 1.0f, 0.2f }, 1 };
    mesh2[6] = { { point7_x,point7_y,-2,1 },{ 0, 0 },{ 1.0f, 0.2f, 0.2f }, 1 };
    mesh2[7] = { { point8_x ,point8_y,-2,1 },{ 0, 1 },{ 0.2f, 1.0f, 0.2f }, 1 };
    mesh2[8] = { { point9_x,point9_y,-2,1 },{ 0, 0 },{ 1.0f, 0.2f, 0.2f }, 1 };
    mesh2[9] = { { point10_x ,point10_y,-2,1 },{ 0, 1 },{ 0.2f, 1.0f, 0.2f }, 1 };

    mesh2[10] = { { point11_x,point11_y,-2,1 },{ 0, 0 },{ 1.0f, 0.2f, 0.2f }, 1 };
    mesh2[11] = { { point12_x ,point12_y,-2,1 },{ 0, 1 },{ 0.2f, 1.0f, 0.2f }, 1 };
    mesh2[12] = { { point13_x,point13_y,-2,1 },{ 0, 0 },{ 1.0f, 0.2f, 0.2f }, 1 };
    mesh2[13] = { { point14_x ,point14_y,-2,1 },{ 0, 1 },{ 0.2f, 1.0f, 0.2f }, 1 };
    mesh2[14] = { { point15_x,point15_y,-2,1 },{ 0, 0 },{ 1.0f, 0.2f, 0.2f }, 1 };

    mesh2[15] = { { point16_x ,point16_y,-2,1 },{ 0, 1 },{ 0.2f, 1.0f, 0.2f }, 1 };
    mesh2[16] = { { point17_x,point17_y,-2,1 },{ 0, 0 },{ 1.0f, 0.2f, 0.2f }, 1 };
    mesh2[17] = { { point18_x ,point18_y,-2,1 },{ 0, 1 },{ 0.2f, 1.0f, 0.2f }, 1 };
    mesh2[18] = { { point19_x,point19_y,-2,1 },{ 0, 0 },{ 1.0f, 0.2f, 0.2f }, 1 };
    mesh2[19] = { { point20_x ,point20_y,-2,1 },{ 0, 1 },{ 0.2f, 1.0f, 0.2f }, 1 };

    mesh2[20] = { { point21_x,point21_y,-2,1 },{ 0, 0 },{ 1.0f, 0.2f, 0.2f }, 1 };
    mesh2[21] = { { point22_x ,point22_y,-2,1 },{ 0, 1 },{ 0.2f, 1.0f, 0.2f }, 1 };
    mesh2[22] = { { point23_x,point23_y,-2,1 },{ 0, 0 },{ 1.0f, 0.2f, 0.2f }, 1 };
    mesh2[23] = { { point24_x ,point24_y,-2,1 },{ 0, 1 },{ 0.2f, 1.0f, 0.2f }, 1 };
    mesh2[24] = { { point25_x,point25_y,-2,1 },{ 0, 0 },{ 1.0f, 0.2f, 0.2f }, 1 };

    mesh2[25] = { { point26_x ,point26_y,-2,1 },{ 0, 1 },{ 0.2f, 1.0f, 0.2f }, 1 };

    mesh2[26] = { { point1_x,point1_y,2,1 },{ 0, 0 },{ 1.0f, 0.2f, 0.2f }, 1 };
    mesh2[27] = { { point2_x ,point2_y,2,1 },{ 0, 1 },{ 0.2f, 1.0f, 0.2f }, 1 };
    mesh2[28] = { { point3_x,point3_y,2,1 },{ 1, 1 },{ 0.2f, 0.2f, 1.0f }, 1 };
    mesh2[29] = { { point4_x ,point4_y,2,1 },{ 0, 1 },{ 0.2f, 1.0f, 0.2f }, 1 };
    mesh2[30] = { { point5_x,point5_y,2,1 },{ 0, 0 },{ 1.0f, 0.2f, 0.2f }, 1 };

    mesh2[31] = { { point6_x ,point6_y,2,1 },{ 0, 1 },{ 0.2f, 1.0f, 0.2f }, 1 };
    mesh2[32] = { { point7_x,point7_y,2,1 },{ 0, 0 },{ 1.0f, 0.2f, 0.2f }, 1 };
    mesh2[33] = { { point8_x ,point8_y,2,1 },{ 0, 1 },{ 0.2f, 1.0f, 0.2f }, 1 };
    mesh2[34] = { { point9_x,point9_y,2,1 },{ 0, 0 },{ 1.0f, 0.2f, 0.2f }, 1 };
    mesh2[35] = { { point10_x ,point10_y,2,1 },{ 0, 1 },{ 0.2f, 1.0f, 0.2f }, 1 };

    mesh2[36] = { { point11_x,point11_y,2,1 },{ 0, 0 },{ 1.0f, 0.2f, 0.2f }, 1 };
    mesh2[37] = { { point12_x ,point12_y,2,1 },{ 0, 1 },{ 0.2f, 1.0f, 0.2f }, 1 };
    mesh2[38] = { { point13_x,point13_y,2,1 },{ 0, 0 },{ 1.0f, 0.2f, 0.2f }, 1 };
    mesh2[39] = { { point14_x ,point14_y,2,1 },{ 0, 1 },{ 0.2f, 1.0f, 0.2f }, 1 };
    mesh2[40] = { { point15_x,point15_y,2,1 },{ 0, 0 },{ 1.0f, 0.2f, 0.2f }, 1 };

    mesh2[41] = { { point16_x ,point16_y,2,1 },{ 0, 1 },{ 0.2f, 1.0f, 0.2f }, 1 };
    mesh2[42] = { { point17_x,point17_y,2,1 },{ 0, 0 },{ 1.0f, 0.2f, 0.2f }, 1 };
    mesh2[43] = { { point18_x ,point18_y,2,1 },{ 0, 1 },{ 0.2f, 1.0f, 0.2f }, 1 };
    mesh2[44] = { { point19_x,point19_y,2,1 },{ 0, 0 },{ 1.0f, 0.2f, 0.2f }, 1 };
    mesh2[45] = { { point20_x ,point20_y,2,1 },{ 0, 1 },{ 0.2f, 1.0f, 0.2f }, 1 };

    mesh2[46] = { { point21_x,point21_y,2,1 },{ 0, 0 },{ 1.0f, 0.2f, 0.2f }, 1 };
    mesh2[47] = { { point22_x ,point22_y,2,1 },{ 0, 1 },{ 0.2f, 1.0f, 0.2f }, 1 };
    mesh2[48] = { { point23_x,point23_y,2,1 },{ 0, 0 },{ 1.0f, 0.2f, 0.2f }, 1 };
    mesh2[49] = { { point24_x ,point24_y,2,1 },{ 0, 1 },{ 0.2f, 1.0f, 0.2f }, 1 };
    mesh2[50] = { { point25_x,point25_y,2,1 },{ 0, 0 },{ 1.0f, 0.2f, 0.2f }, 1 };

    mesh2[51] = { { point26_x ,point26_y,2,1 },{ 0, 1 },{ 0.2f, 1.0f, 0.2f }, 1 };

}


vector< pair<float, float> >  DataCenter::cal_meshpoints(int side)
{
    vector< pair<float, float> > result;
    for (int i = 0; i < side; i++)
    {
        pair<float, float> point_tmp;
        point_tmp.first = cos((2 * PI / side)*i);
        point_tmp.second = sin((2 * PI / side)*i);
        result.push_back(point_tmp);
    }

    return result;
}
