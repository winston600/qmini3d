#include "qmini3dgui.h"
#include "ui_qmini3dgui.h"

#include <QVector3D>
#include <stdio.h>
#include <math.h>
#include "fishStruct.h"

using namespace std;

qmini3dgui::qmini3dgui(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::qmini3dgui)
{
    ui->setupUi(this);
    alpha = 1;
    pos = 3.5;

    kbhit = 0;
    indicator = 0;
    states[0] = RENDER_STATE_COLOR;
    states[1] = RENDER_STATE_WIREFRAME;

	sensitivity = 1.0f;
}

qmini3dgui::~qmini3dgui()
{
    delete ui;
}
void qmini3dgui::keyPressEvent(QKeyEvent *event)
{
    if (event->key() == Qt::Key_Left)
    {
        alpha += 0.1f;
        ui->label->Setalpha(alpha);
    }

    if (event->key() == Qt::Key_Right)
    {
        alpha -= 0.1f;
        ui->label->Setalpha(alpha);
    }

    if (event->key() == Qt::Key_Up)
    {
        pos -= 0.1f;
        ui->label->Setpos(pos);
    }

    if (event->key() == Qt::Key_Down)
    {
        pos += 0.1f;
        ui->label->Setpos(pos);
    }
    if (event->key() == Qt::Key_W)
    {
        if (++indicator >= 2) indicator = 0;
        ui->label->m_renderer->GetfishDevice()->render_state = states[indicator];
    }

}

void qmini3dgui::mousePressEvent(QMouseEvent *event)
{
	if (event->button() == Qt::LeftButton) {
		lastPos = event->pos(); // 记录下鼠标按下的位置
	}else if (event->button() == Qt::RightButton) {
		lastPos = event->pos(); // 记录右键按下的位置
	}
}
//实现vtkInteractorStyleTrackballCamera
void qmini3dgui::mouseMoveEvent(QMouseEvent *event)
{
	if (event->buttons() & Qt::LeftButton) 
	{
		// 灵敏度因子
		float sensitivity = 15.5f; // 更高的值表示更灵敏

		// 计算鼠标移动的距离
		float delta_x = (event->x() - lastPos.x()) * sensitivity;
		float delta_y = (event->y() - lastPos.y()) * sensitivity;

		quaternion_t *temp_quaternion = new quaternion_t;
		calculate_rotation(lastPos.x()+ delta_x,lastPos.y()+ delta_y,event->x()+ delta_x,event->y()+ delta_y,temp_quaternion);

		// 更新四元数
		ui->label->Setxyz(temp_quaternion->x,temp_quaternion->y,temp_quaternion->z);
		ui->label->Setalpha(temp_quaternion->w);

		lastPos = event->pos(); // 更新最后位置
		update(); // 请求重绘
	}
	else if (event->buttons() & Qt::RightButton)
	{
		int dx = event->x() - lastPos.x();
		int dy = event->y() - lastPos.y();

		if (dy > 0)
		{
			pos -= 0.03f;
			ui->label->Setpos(pos);
		}
		else
		{
			pos += 0.03f;
			ui->label->Setpos(pos);
		}
		update(); // 请求重绘
	}
}

//void qmini3dgui::mouseMoveEvent(QMouseEvent *event)
//{
//	if (event->buttons() & Qt::LeftButton) 
//	{
//		float dx = event->x() - lastPos.x();
//		float dy = event->y() - lastPos.y();
//
//		// 根据鼠标移动量计算旋转轴和角度
//		QVector3D axis = QVector3D(dy, dx, 0).normalized(); // 旋转轴
//		alpha += sqrt(dx * dx + dy * dy) * 0.01f; // 旋转角度  0.01控制灵敏度
//		// 更新四元数
//		ui->label->Setalpha(alpha);
//
//		lastPos = event->pos(); // 更新最后位置
//		update(); // 请求重绘
//	}
//	else if (event->buttons() & Qt::RightButton)
//	{
//		int dx = event->x() - lastPos.x();
//		int dy = event->y() - lastPos.y();
//
//		if (dy > 0)
//		{
//			pos -= 0.03f;
//			ui->label->Setpos(pos);
//		}
//		else
//		{
//			pos += 0.03f;
//			ui->label->Setpos(pos);
//		}
//		update(); // 请求重绘
//	}
//}

//void qmini3dgui::normalizeAngle(int *angle)
//{
//	while (*angle < 0)
//		*angle += 360 * 16;
//	while (*angle > 360 * 16)
//		*angle -= 360 * 16;
//}
//
//void qmini3dgui::setXRotation(int angle)
//{
//	normalizeAngle(&angle);
//	if (angle != xRot) {
//		xRot = angle;
//		emit xRotationChanged(angle);
//		neheWidget->setxRot(xRot);
//		neheWidget->updateGL();
//	}
//}
//
//void qmini3dgui::setYRotation(int angle)
//{
//	normalizeAngle(&angle);
//	if (angle != yRot) {
//		yRot = angle;
//		emit yRotationChanged(angle);
//		neheWidget->setyRot(yRot);
//		neheWidget->updateGL();
//	}
//}
//
//void qmini3dgui::setZRotation(int angle)
//{
//	normalizeAngle(&angle);
//	if (angle != zRot) {
//		zRot = angle;
//		emit zRotationChanged(angle);
//		neheWidget->setzRot(zRot);
//		neheWidget->updateGL();
//	}
//}

// 将笛卡尔坐标归一化到单位球面
void qmini3dgui::map_to_sphere(int x, int y, int width, int height, float *sx, float *sy) 
{
	// 计算鼠标坐标的中心偏移
	float nx = (2.0f * x / width - 1.0f) * 0.5f; // [-1, 1]
	float ny = (1.0f - 2.0f * y / height) * 0.5f; // [-1, 1]

	// 在单位球上计算 Z 坐标
	float r = nx * nx + ny * ny;
	float nz = r <= 1.0f ? sqrt(1.0f - r) : 0.0f;

	*sx = nx;
	*sy = ny;
}
//
// 计算两点之间的旋转轴和角度
void qmini3dgui::calculate_rotation(float start_x, float start_y, float end_x, float end_y,
	quaternion_t *rotation) 
{
		float start_sx, start_sy, end_sx, end_sy;
		map_to_sphere(start_x, start_y, 512, 512, &start_sx, &start_sy);
		map_to_sphere(end_x, end_y, 512, 512, &end_sx, &end_sy);

		// 计算旋转轴
		float ax = start_sy * 1.0f - end_sy * 1.0f; // 这里假定z=1.0f
		float ay = end_sx * 1.0f - start_sx * 1.0f;
		float az = start_sx * end_sy - start_sy * end_sx;

		// 计算旋转角度
		float dot_product = start_sx * end_sx + start_sy * end_sy 
			+ sqrt(1.0f - (start_sx * start_sx + start_sy * start_sy)) * sqrt(1.0f - (end_sx * end_sx + end_sy * end_sy));
		float angle = acos(my_fmax(-1.0f, my_fmin(1.0f, dot_product)))* sensitivity;

		// 归一化旋转轴
		float length = sqrt(ax * ax + ay * ay + az * az);
		if (length > 1e-6f) {
			ax /= length;
			ay /= length;
			az /= length;
		} else {
			ax = ay = az = 0.0f; // 没有有效的旋转
		}

		// 构造四元数
		rotation->w = cos(angle / 2.0f);
		rotation->x = ax * sin(angle / 2.0f);
		rotation->y = ay * sin(angle / 2.0f);
		rotation->z = az * sin(angle / 2.0f);
}