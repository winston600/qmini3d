#ifndef QMINI3DGUI_H
#define QMINI3DGUI_H

#include <QWidget>
#include <QKeyEvent>
#include <QMouseEvent>

namespace Ui {
    class qmini3dgui;
}

typedef struct {
	float x, y, z, w;
} quaternion_t;



class qmini3dgui : public QWidget
{
    Q_OBJECT

public:
    explicit qmini3dgui(QWidget *parent = 0);
    ~qmini3dgui();
protected:
    void keyPressEvent(QKeyEvent *event);
    void mousePressEvent(QMouseEvent * event);
    void mouseMoveEvent(QMouseEvent *event);
private:
    Ui::qmini3dgui *ui;

    float alpha;
    float pos;
    QPoint lastPos;
    int kbhit;
    int indicator;
    int states[2];

	void map_to_sphere(int x, int y, int width, int height, float *sx, float *sy) ;
	void calculate_rotation(float start_x, float start_y, float end_x, float end_y,
		quaternion_t *rotation) ;

	float my_fmax(float a, float b) {
		return (a > b) ? a : b;
	}

	float my_fmin(float a, float b) {
		return (a < b) ? a : b;
	}

	float sensitivity;// ����������
};

#endif // QMINI3DGUI_H
