#include "fishCube.h"

fishCube::fishCube(QObject *parent)
    : QObject(parent)
{
    //一个立方体
	{
		cube_mesh[0].pos.x =  1;
		cube_mesh[0].pos.y = -1;
		cube_mesh[0].pos.z =  1;
		cube_mesh[0].pos.w =  1;
		cube_mesh[0].tc.u = 0;
		cube_mesh[0].tc.v = 0;
		cube_mesh[0].color.r = 1.0;
		cube_mesh[0].color.g = 0.2;
		cube_mesh[0].color.b = 0.2;
		cube_mesh[0].rhw = 1;
	}

	{
		cube_mesh[1].pos.x = -1;
		cube_mesh[1].pos.y = -1;
		cube_mesh[1].pos.z =  1;
		cube_mesh[1].pos.w =  1;
		cube_mesh[1].tc.u = 0;
		cube_mesh[1].tc.v = 1;
		cube_mesh[1].color.r = 0.2;
		cube_mesh[1].color.g = 1.0;
		cube_mesh[1].color.b = 0.2;
		cube_mesh[1].rhw = 1;
	}

	{
		cube_mesh[2].pos.x = -1;
		cube_mesh[2].pos.y =  1;
		cube_mesh[2].pos.z =  1;
		cube_mesh[2].pos.w =  1;
		cube_mesh[2].tc.u = 1;
		cube_mesh[2].tc.v = 1;
		cube_mesh[2].color.r = 0.2;
		cube_mesh[2].color.g = 0.2;
		cube_mesh[2].color.b = 1.0;
		cube_mesh[2].rhw = 1;
	}

	{
		cube_mesh[3].pos.x =  1;
		cube_mesh[3].pos.y =  1;
		cube_mesh[3].pos.z =  1;
		cube_mesh[3].pos.w =  1;
		cube_mesh[3].tc.u = 1;
		cube_mesh[3].tc.v = 0;
		cube_mesh[3].color.r = 1.0;
		cube_mesh[3].color.g = 0.2;
		cube_mesh[3].color.b = 1.0;
		cube_mesh[3].rhw = 1;
	}

	{
		cube_mesh[4].pos.x =  1;
		cube_mesh[4].pos.y = -1;
		cube_mesh[4].pos.z = -1;
		cube_mesh[4].pos.w =  1;
		cube_mesh[4].tc.u = 0;
		cube_mesh[4].tc.v = 0;
		cube_mesh[4].color.r = 1.0;
		cube_mesh[4].color.g = 1.0;
		cube_mesh[4].color.b = 0.2;
		cube_mesh[4].rhw = 1;
	}

	{
		cube_mesh[5].pos.x = -1;
		cube_mesh[5].pos.y = -1;
		cube_mesh[5].pos.z = -1;
		cube_mesh[5].pos.w =  1;
		cube_mesh[5].tc.u = 0;
		cube_mesh[5].tc.v = 1;
		cube_mesh[5].color.r = 0.2;
		cube_mesh[5].color.g = 1.0;
		cube_mesh[5].color.b = 1.0;
		cube_mesh[5].rhw = 1;
	}

	{
		cube_mesh[6].pos.x = -1;
		cube_mesh[6].pos.y =  1;
		cube_mesh[6].pos.z = -1;
		cube_mesh[6].pos.w =  1;
		cube_mesh[6].tc.u = 1;
		cube_mesh[6].tc.v = 1;
		cube_mesh[6].color.r = 1.0;
		cube_mesh[6].color.g = 0.3;
		cube_mesh[6].color.b = 0.3;
		cube_mesh[6].rhw = 1;
	}

	{
		cube_mesh[7].pos.x =  1;
		cube_mesh[7].pos.y =  1;
		cube_mesh[7].pos.z = -1;
		cube_mesh[7].pos.w =  1;
		cube_mesh[7].tc.u = 1;
		cube_mesh[7].tc.v = 0;
		cube_mesh[7].color.r = 0.2;
		cube_mesh[7].color.g = 1.0;
		cube_mesh[7].color.b = 0.3;
		cube_mesh[7].rhw = 1;
	}

    //cube_mesh[0] = { {  1, -1,  1, 1 }, { 0, 0 }, { 1.0, 0.2, 0.2 }, 1 };
    //cube_mesh[1] = { { -1, -1,  1, 1 }, { 0, 1 }, { 0.2, 1.0, 0.2 }, 1 };
    //cube_mesh[2] = { { -1,  1,  1, 1 }, { 1, 1 }, { 0.2, 0.2, 1.0 }, 1 };
    //cube_mesh[3] = { {  1,  1,  1, 1 }, { 1, 0 }, { 1.0, 0.2, 1.0 }, 1 };
    //cube_mesh[4] = { {  1, -1, -1, 1 }, { 0, 0 }, { 1.0, 1.0, 0.2 }, 1 };
    //cube_mesh[5] = { { -1, -1, -1, 1 }, { 0, 1 }, { 0.2, 1.0, 1.0 }, 1 };
    //cube_mesh[6] = { { -1,  1, -1, 1 }, { 1, 1 }, { 1.0, 0.3, 0.3 }, 1 };
    //cube_mesh[7] = { {  1,  1, -1, 1 }, { 1, 0 }, { 0.2, 1.0, 0.3 }, 1 };

}

fishCube::~fishCube()
{
}

fishCube *fishCube::fish_cube = 0;

fishCube *fishCube::GetfishCube()
{
    if (fish_cube == 0)
    {
        fish_cube = new fishCube();
        //qDebug()<<"DataControl created";
    }
    return fish_cube;
}



