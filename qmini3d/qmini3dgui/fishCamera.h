#ifndef FISHCAMERA_H
#define FISHCAMERA_H

#include "fishIncludes.h"
#include "fishStruct.h"


class fishRenderer;
class fishCamera
{
public:
    fishCamera();
    void SetRenderer(fishRenderer *pRenderer);

    void matrix_set_lookat(matrix_t *m, const vector_t *eye, const vector_t *at, const vector_t *up);
    void camera_at_zero(float x, float y, float z);

private:

    fishRenderer *m_renderer;

};

#endif // FISHCAMERA_H
