#include "fishCamera.h"
#include "fishRenderer.h"

fishCamera::fishCamera()
{
    m_renderer = NULL;
}

// ���������
void fishCamera::matrix_set_lookat(matrix_t *m, const vector_t *eye, const vector_t *at, const vector_t *up)
{
    vector_t xaxis, yaxis, zaxis;

    m_renderer->GetfishVector()->vector_sub(&zaxis, at, eye);
    m_renderer->GetfishVector()->vector_normalize(&zaxis);
    m_renderer->GetfishVector()->vector_crossproduct(&xaxis, up, &zaxis);
    m_renderer->GetfishVector()->vector_normalize(&xaxis);
    m_renderer->GetfishVector()->vector_crossproduct(&yaxis, &zaxis, &xaxis);

    m->m[0][0] = xaxis.x;
    m->m[1][0] = xaxis.y;
    m->m[2][0] = xaxis.z;
    m->m[3][0] = -m_renderer->GetfishVector()->vector_dotproduct(&xaxis, eye);

    m->m[0][1] = yaxis.x;
    m->m[1][1] = yaxis.y;
    m->m[2][1] = yaxis.z;
    m->m[3][1] = -m_renderer->GetfishVector()->vector_dotproduct(&yaxis, eye);

    m->m[0][2] = zaxis.x;
    m->m[1][2] = zaxis.y;
    m->m[2][2] = zaxis.z;
    m->m[3][2] = -m_renderer->GetfishVector()->vector_dotproduct(&zaxis, eye);

    m->m[0][3] = m->m[1][3] = m->m[2][3] = 0.0f;
    m->m[3][3] = 1.0f;
}

void fishCamera::camera_at_zero(float x, float y, float z)
{
    point_t eye = { x, y, z, 1 }, at = { 0, 0, 0, 1 }, up = { 0, 0, 1, 1 };
    this->matrix_set_lookat(&m_renderer->GetfishDevice()->GetFishTransform()->view, &eye, &at, &up);
    m_renderer->GetfishTransform()->transform_update();
}

void fishCamera::SetRenderer(fishRenderer *pRenderer)
{
    m_renderer = pRenderer;
}

