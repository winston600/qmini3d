#pragma once

#include <QLabel>
#include <QMouseEvent>
#include <QPaintEvent>
#include <QKeyEvent>
//#include "mini3d.h"
#include <QPainter>


#include "fishRenderer.h"
#include "fishCamera.h"
#include "fishBlocks.h"
#include "fishTexture.h"

//#define NONE 0
//#define ROTATE 1
//#define ZOOM 2

enum ActionType { NONE, ROTATE, ZOOM };

class fishLabel : public QLabel
{
    Q_OBJECT

public:
    fishLabel(QWidget *parent);
    ~fishLabel();
    void Setalpha(float al) {  alpha=al; }
    void Setpos(float po){ pos= po; }
    void Setxyz(float x,float y,float z=1);

    fishRenderer *m_renderer;
    fishCamera *m_camera;
    fishBlocks *m_blocks;
    fishTexture *m_texture;

protected:
    void paintEvent(QPaintEvent *e);
    //        void keyPressEvent(QKeyEvent * event);
    /*void mousePressEvent(QMouseEvent * event);
 void mouseMoveEvent(QMouseEvent *e);*/


private:
    //enum ActionType actionMode;
    //int lastMouseX;
    //int lastMouseY;
    //float tbLastPosition[3], tbAxis[3];
    ////void tbPointToVector(int, int, float*);

    ///*unsigned char *img;*/





    float alpha;
    float pos;


    unsigned char *screen_fb;

    QImage *img;
    int img_width;
    int img_height;

    float dx;
    float dy;
    float dz;

    //QPoint lastPos;

};
