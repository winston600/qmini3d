#ifndef FISHVERTEX_H
#define FISHVERTEX_H

#include "fishStruct.h"
#include "fishMatrix.h"

class fishVertex
{
public:
    fishVertex();

    void SetInput(fishMatrix *pmatrix);

    void vertex_rhw_init(vertex_t *v);
    void vertex_interp(vertex_t *y, const vertex_t *x1, const vertex_t *x2, float t);
    void vertex_division(vertex_t *y, const vertex_t *x1, const vertex_t *x2, float w);
    void vertex_add(vertex_t *y, const vertex_t *x);

private:
    fishMatrix *m_matrix;

};

#endif // FISHVERTEX_H
