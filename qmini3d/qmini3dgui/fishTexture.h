#ifndef FISHTEXTURE_H
#define FISHTEXTURE_H



#include "fishIncludes.h"

#include "fishStruct.h"
#include "fishDevice.h"


class fishRenderer;
class fishTexture
{
public:
    fishTexture();
    void SetRenderer(fishRenderer *pRenderer);

    void init_texture();
    // 设置当前纹理
    void device_set_texture(void *bits, long pitch, int w, int h);
    // 根据坐标读取纹理
    IUINT32 fishDeviceexture_read(float u, float v);


private:
    fishRenderer *m_renderer;
};

#endif // FISHTEXTURE_H
