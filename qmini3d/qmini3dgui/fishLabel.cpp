#include "fishLabel.h"
#include <qDebug>

#include "fishIncludes.h"


fishLabel::fishLabel(QWidget *parent)
    :QLabel(parent)

{
    //actionMode = NONE;
    std::cout<<"fishLabel::fishLabel"<<std::endl;
    //开辟内存 存储图像指针
    screen_fb = new unsigned char[512*512*8];		// frame buffer
    memset(screen_fb, 0, 512 * 512 * 8);

    alpha = 1;
    pos = 3.5;

    dx = -1;
    dy = -0.5;
    dz = -1;

    img = new QImage(screen_fb,512, 512, QImage::Format_RGB32);

    m_blocks = new fishBlocks;
    m_camera = new fishCamera;//顺序不能错

    m_renderer = new fishRenderer;
    m_renderer->SetfishCamera(m_camera);
    m_renderer->device_init(512, 512, screen_fb);

    m_renderer->SetfishBlocks(m_blocks);
    m_camera->camera_at_zero(3, 0, 0);//顺序不能错

    m_texture = new fishTexture;
    m_renderer->SetfishTexture(m_texture);
    //m_renderer->init_texture();

}

fishLabel::~fishLabel()
{
}

void fishLabel::paintEvent(QPaintEvent *e)
{
    //qDebug()<<"fishLabel::paintEvent";
    //std::cout<<"fishLabel::paintEvent"<<std::endl;
    m_renderer->device_clear(1);
    m_camera->camera_at_zero(pos, 0, 0);
    //m_renderer->Update(alpha);
	m_renderer->Update(dx,dy,dz,alpha);

    //绘制图像
    QPainter painter(this);
    img_width = img->width();
    img_height = img->height();
    painter.drawPixmap(0, 0, img_width, img_height, QPixmap::fromImage(*img));

    this->update();
}

//------------------------------------------------------------------
//QLabel中无法接收QKeyEvent？？ 2020年4月10日
//------------------------------------------------------------------
void fishLabel::Setxyz(float x, float y, float z)
{
    dx += x;
    dy += y;
    dz += z;
}

//void fishLabel::keyPressEvent(QKeyEvent *event)
//{
//    qDebug()<<"hello";
//}


//void fishLabel::keyPressEvent(QKeyEvent *event)
//{
//	if (event->key() == Qt::Key_Left)
//	{
//		alpha += 0.1f;
//		this->Setalpha(alpha);
//
//	}
//
//	if (event->key() == Qt::Key_Right)
//	{
//		alpha -= 0.1f;
//		this->Setalpha(alpha);
//	}
//
//	if (event->key() == Qt::Key_Up)
//	{
//		pos -= 0.1f;
//		this->Setpos(pos);
//	}
//
//	if (event->key() == Qt::Key_Down)
//	{
//		pos += 0.1f;
//		this->Setpos(pos);
//	}
//
//}

//void fishLabel::mousePressEvent(QMouseEvent *event)
//{
//	lastPos = event->pos();
//}
//
//void fishLabel::mouseMoveEvent(QMouseEvent *event)
//{
//	int dx = event->x() - lastPos.x();
//	int dy = event->y() - lastPos.y();
//
//	switch (event->buttons())
//	{
//	case Qt::LeftButton:
//		/*setXRotation(xRot + 2 * dy);
//		setYRotation(yRot + 2 * dx);
//		setZRotation(zRot + 2 * dx);*/
//
//		/*dx = dx/sqrt(dx*dx+dy*dy);
//		dy=dy / sqrt(dx*dx + dy*dy);*/
//		//ui.label->Setxyz(dx,dy);
//
//		if (dx > 0)
//		{
//			alpha -= 0.03f;
//			this->Setalpha(alpha);
//		}
//		else
//		{
//			alpha += 0.03f;
//			this->Setalpha(alpha);
//		}
//		break;
//	case Qt::RightButton:
//		if (dy > 0)
//		{
//			pos -= 0.03f;
//			this->Setpos(pos);
//		}
//		else
//		{
//			pos += 0.03f;
//			this->Setpos(pos);
//		}
//		break;
//	}
//	lastPos = event->pos();
//}






//void fishLabel::mousePressEvent(QMouseEvent *event)
//{
//
//	int x=event->x();
//	int y = event->y();
//
//	setFocus();
//
//	switch (event->button())
//	{
//
//	case Qt::LeftButton:
//	{
//		//if (/*settings->enablePicking*/)
//		//{
//		//	/*if (settings->mesh != NULL)
//		//	{
//		//	pickObject(x, y);
//		//	updateGL();
//		//	}*/
//		//}
//		//else
//		//{
//			actionMode = ROTATE;
//			tbPointToVector(x, y, tbLastPosition);
//		/*}*/
//		break;
//	}
//
//		
//
//	//case Qt::RightButton:
//	//	{
//	//		actionMode = ZOOM;
//	//		lastMouseX = x;
//	//		lastMouseY = y;
//	//		break;
//	//	}
//	//	
//
//	}
//}
//
//void fishLabel::mouseMoveEvent(QMouseEvent *e)
//{
//	if (actionMode == NONE)
//		return;
//
//	int x = e->x();
//	int y = e->y();
//
//	switch (actionMode)
//	{
//	/*case ZOOM:
//		settings->zShift += (y - lastMouseY) / (double)height() * 3;
//		lastMouseX = x;
//		lastMouseY = y;
//		break;*/
//
//
//	case ROTATE:
//		float currentPosition[3], angle, dx, dy, dz;
//
//		tbPointToVector(x, y, currentPosition);
//
//		dx = currentPosition[0] - tbLastPosition[0];
//		dy = currentPosition[1] - tbLastPosition[1];
//		dz = currentPosition[2] - tbLastPosition[2];
//
//		angle = 180.0 * sqrt(dx * dx + dy * dy + dz * dz);
//
//		tbAxis[0] = tbLastPosition[1] * currentPosition[2] -
//			tbLastPosition[2] * currentPosition[1];
//		tbAxis[1] = tbLastPosition[2] * currentPosition[0] -
//			tbLastPosition[0] * currentPosition[2];
//		tbAxis[2] = tbLastPosition[0] * currentPosition[1] -
//			tbLastPosition[1] * currentPosition[0];
//
//		tbLastPosition[0] = currentPosition[0];
//		tbLastPosition[1] = currentPosition[1];
//		tbLastPosition[2] = currentPosition[2];
//
//		glPushMatrix();
//		glLoadIdentity();
//		glRotatef(angle, tbAxis[0], tbAxis[1], tbAxis[2]);
//		glMultMatrixf((GLfloat*)settings->tbTransform);
//		glGetFloatv(GL_MODELVIEW_MATRIX, (GLfloat *)settings->tbTransform);
//		glPopMatrix();
//
//		break;
//	}
//
//	updateGL();
//}
//
//void fishLabel::mouseReleaseEvent(QMouseEvent *e)
//{
//	actionMode = NONE;
//}
//
//
//void fishLabel::tbPointToVector(int x, int y , float *pos)
//{
//	pos[0] = x;
//	pos[1] = y;
//	pos[2] = 0;
//}
