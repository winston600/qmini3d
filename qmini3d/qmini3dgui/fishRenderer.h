#pragma once

#include <vector>

#include "assert.h"

#include "fishStruct.h"


#include "fishCube.h"

#include "fishTransform.h"
#include "fishVertex.h"
#include "fishMatrix.h"
#include "fishVector.h"
#include "fishCamera.h"

#include "fishDevice.h"

#include "fishBlocks.h"
#include "fishTexture.h"

using namespace std;


class fishRenderer
{
public:
    fishRenderer();
    ~fishRenderer();
    void Update(float alpha);
	void Update(float x,float y,float z,float alpha);


    void device_render_trap(trapezoid_t *trap);

    void trapezoid_edge_interp(trapezoid_t *trap, float y);
    void trapezoid_init_scan_line(const trapezoid_t *trap, scanline_t *scanline, int y);
    void device_draw_scanline(scanline_t *scanline);


    void device_init(int width, int height, void *fb);
    void device_destroy();

    void device_clear(int mode);




//    void draw_colume(float x, float y, float z, float theta);





public:

    void SetfishBlocks(fishBlocks *pblocks);
    fishBlocks *GetfishBlocks(){ return m_blocks; }

    void SetfishCamera(fishCamera *pcamera);
    fishCamera *GetfishCamera(){ return m_camera; }

    void SetfishTexture(fishTexture *ptexture);
    fishTexture *GetfishTexture(){ return m_texture; }


    fishDevice *GetfishDevice(){ return m_device; }
    fishVector *GetfishVector(){ return m_vector; }
    fishMatrix *GetfishMatrix(){ return m_matrix; }
    fishVertex *GetfishVertex(){ return m_vertex; }
    fishTransform *GetfishTransform(){ return m_transform; }


private:
    fishTransform *m_transform;
    fishVertex *m_vertex;
    fishMatrix *m_matrix;
    fishVector *m_vector;
    fishDevice *m_device;

    fishCamera *m_camera;
    fishBlocks *m_blocks;
    fishTexture *m_texture;



};

