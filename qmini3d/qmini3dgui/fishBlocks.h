#ifndef FISHBLOCKS_H
#define FISHBLOCKS_H

#include "fishStruct.h"
#include "fishDevice.h"

class fishRenderer;

class fishBlocks
{
public:
    fishBlocks();
    void SetRenderer(fishRenderer *pRenderer);
    void Update();

    // 画点
    void device_pixel(int x, int y, IUINT32 color);
    // 绘制线段
    void device_draw_line(int x1, int y1, int x2, int y2, IUINT32 c);
    // 根据 render_state 绘制原始三角形
    void device_draw_primitive(const vertex_t *v1,
                                            const vertex_t *v2, const vertex_t *v3);
    // 根据三角形生成 0-2 个梯形，并且返回合法梯形的数量
    int trapezoid_init_triangle(trapezoid_t *trap, const vertex_t *p1,
                                             const vertex_t *p2, const vertex_t *p3);

    //面
    void draw_plane(int a, int b, int c, int d);
    //cube
    void draw_cube(float theta);
	void draw_cube(float x,float y,float z,float theta);
private:
    fishRenderer *m_renderer;
};

#endif // FISHBLOCKS_H
