#ifndef FISHDEVICE_H
#define FISHDEVICE_H

#include "fishStruct.h"
#include "fishTransform.h"

typedef unsigned int IUINT32;
//=====================================================================
// 渲染设备
//=====================================================================

class fishDevice
{
public:
    fishDevice();
    void SetInput(fishTransform *ptransform);
    fishTransform *GetFishTransform();


    int width;                  // 窗口宽度
    int height;                 // 窗口高度
    IUINT32 **framebuffer;      // 像素缓存：framebuffer[y] 代表第 y行
    float **zbuffer;            // 深度缓存：zbuffer[y] 为第 y行指针
    IUINT32 **texture;          // 纹理：同样是每行索引
    int tex_width;              // 纹理宽度
    int tex_height;             // 纹理高度
    float max_u;                // 纹理最大宽度：tex_width - 1
    float max_v;                // 纹理最大高度：tex_height - 1
    int render_state;           // 渲染状态
    IUINT32 background;         // 背景颜色
    IUINT32 foreground;         // 线框颜色

private:
    fishTransform *m_transform;      // 坐标变换器

};

#endif // FISHDEVICE_H
