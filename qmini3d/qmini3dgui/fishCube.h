#pragma once
#include "fishStruct.h"
#include <vector>

#include <QObject>
#include <math.h>
#define PI 3.1415926

using namespace std;

class fishCube : public QObject
{
    Q_OBJECT

public:
    static fishCube *GetfishCube();
    ~fishCube();

    vertex_t cube_mesh[8];

private:
    static fishCube *fish_cube;
    fishCube(QObject *parent = 0);


};
