#include "fishMatrix.h"

#include <math.h>
#include <stdio.h>
//=====================================================================
// 数学库：此部分应该不用详解，熟悉 D3D 矩阵变换即可
//=====================================================================

fishMatrix::fishMatrix()
{
    m_vector = NULL;
}

int fishMatrix::CMID(int x, int min, int max)
{
    return (x < min) ? min : ((x > max) ? max : x);
}



// c = a + b
void fishMatrix::matrix_add(matrix_t *c, const matrix_t *a, const matrix_t *b)
{
    int i, j;
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 4; j++)
            c->m[i][j] = a->m[i][j] + b->m[i][j];
    }
}

// c = a - b
void fishMatrix::matrix_sub(matrix_t *c, const matrix_t *a, const matrix_t *b)
{
    int i, j;
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 4; j++)
            c->m[i][j] = a->m[i][j] - b->m[i][j];
    }
}

// c = a * b
void fishMatrix::matrix_mul(matrix_t *c, const matrix_t *a, const matrix_t *b)
{
    matrix_t z;
    int i, j;
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 4; j++)
        {
            z.m[j][i] = (a->m[j][0] * b->m[0][i]) +
                    (a->m[j][1] * b->m[1][i]) +
                    (a->m[j][2] * b->m[2][i]) +
                    (a->m[j][3] * b->m[3][i]);
        }
    }
    c[0] = z;
}

// c = a * f
void fishMatrix::matrix_scale(matrix_t *c, const matrix_t *a, float f)
{
    int i, j;
    for (i = 0; i < 4; i++)
    {
        for (j = 0; j < 4; j++)
            c->m[i][j] = a->m[i][j] * f;
    }
}

// y = x * m
void fishMatrix::matrix_apply(vector_t *y, const vector_t *x, const matrix_t *m)
{
    float X = x->x, Y = x->y, Z = x->z, W = x->w;
    y->x = X * m->m[0][0] + Y * m->m[1][0] + Z * m->m[2][0] + W * m->m[3][0];
    y->y = X * m->m[0][1] + Y * m->m[1][1] + Z * m->m[2][1] + W * m->m[3][1];
    y->z = X * m->m[0][2] + Y * m->m[1][2] + Z * m->m[2][2] + W * m->m[3][2];
    y->w = X * m->m[0][3] + Y * m->m[1][3] + Z * m->m[2][3] + W * m->m[3][3];
}
//把参数矩阵变成4*4单位矩阵
void fishMatrix::matrix_set_identity(matrix_t *m)
{
    m->m[0][0] = m->m[1][1] = m->m[2][2] = m->m[3][3] = 1.0f;
    m->m[0][1] = m->m[0][2] = m->m[0][3] = 0.0f;
    m->m[1][0] = m->m[1][2] = m->m[1][3] = 0.0f;
    m->m[2][0] = m->m[2][1] = m->m[2][3] = 0.0f;
    m->m[3][0] = m->m[3][1] = m->m[3][2] = 0.0f;
}
//4*4 0矩阵
void fishMatrix::matrix_set_zero(matrix_t *m)
{
    m->m[0][0] = m->m[0][1] = m->m[0][2] = m->m[0][3] = 0.0f;
    m->m[1][0] = m->m[1][1] = m->m[1][2] = m->m[1][3] = 0.0f;
    m->m[2][0] = m->m[2][1] = m->m[2][2] = m->m[2][3] = 0.0f;
    m->m[3][0] = m->m[3][1] = m->m[3][2] = m->m[3][3] = 0.0f;
}

// 平移变换
void fishMatrix::matrix_set_translate(matrix_t *m, float x, float y, float z)
{
    matrix_set_identity(m);
    m->m[3][0] = x;
    m->m[3][1] = y;
    m->m[3][2] = z;
}

// 缩放变换
void fishMatrix::matrix_set_scale(matrix_t *m, float x, float y, float z)
{
    matrix_set_identity(m);
    m->m[0][0] = x;
    m->m[1][1] = y;
    m->m[2][2] = z;
}

// 旋转矩阵:实现绕(x,y,z)向量旋转theta度的变换矩阵，一般情况下，使用四元数的旋转计算公式即可
void fishMatrix::matrix_set_rotate(matrix_t *m, float x, float y, float z, float theta)
{
    float qsin = (float)sin(theta * 0.5f);
    float qcos = (float)cos(theta * 0.5f);
    vector_t vec = { x, y, z, 1.0f };
    float w = qcos;
    m_vector->vector_normalize(&vec);
    x = vec.x * qsin;
    y = vec.y * qsin;
    z = vec.z * qsin;
    m->m[0][0] = 1 - 2 * y * y - 2 * z * z;
    m->m[1][0] = 2 * x * y - 2 * w * z;
    m->m[2][0] = 2 * x * z + 2 * w * y;
    m->m[0][1] = 2 * x * y + 2 * w * z;
    m->m[1][1] = 1 - 2 * x * x - 2 * z * z;
    m->m[2][1] = 2 * y * z - 2 * w * x;
    m->m[0][2] = 2 * x * z - 2 * w * y;
    m->m[1][2] = 2 * y * z + 2 * w * x;
    m->m[2][2] = 1 - 2 * x * x - 2 * y * y;
    m->m[0][3] = m->m[1][3] = m->m[2][3] = 0.0f;
    m->m[3][0] = m->m[3][1] = m->m[3][2] = 0.0f;
    m->m[3][3] = 1.0f;
}

// D3DXMatrixPerspectiveFovLH
void fishMatrix::matrix_set_perspective(matrix_t *m, float fovy, float aspect, float zn, float zf)
{
    float fax = 1.0f / (float)tan(fovy * 0.5f);
    matrix_set_zero(m);
    m->m[0][0] = (float)(fax / aspect);
    m->m[1][1] = (float)(fax);
    m->m[2][2] = zf / (zf - zn);
    m->m[3][2] = -zn * zf / (zf - zn);
    m->m[2][3] = 1;
}

void fishMatrix::SetInput(fishVector *pvector)
{
     m_vector = pvector;
}
