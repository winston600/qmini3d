/********************************************************************************
** Form generated from reading UI file 'qmini3dgui.ui'
**
** Created by: Qt User Interface Compiler version 4.8.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QMINI3DGUI_H
#define UI_QMINI3DGUI_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGridLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>
#include "fishLabel.h"

QT_BEGIN_NAMESPACE

class Ui_qmini3dgui
{
public:
    QVBoxLayout *verticalLayout;
    QWidget *widget3d;
    QGridLayout *gridLayout;
    fishLabel *label;

    void setupUi(QWidget *qmini3dgui)
    {
        if (qmini3dgui->objectName().isEmpty())
            qmini3dgui->setObjectName(QString::fromUtf8("qmini3dgui"));
        qmini3dgui->resize(512, 512);
        verticalLayout = new QVBoxLayout(qmini3dgui);
        verticalLayout->setSpacing(0);
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        widget3d = new QWidget(qmini3dgui);
        widget3d->setObjectName(QString::fromUtf8("widget3d"));
        widget3d->setStyleSheet(QString::fromUtf8("background-color: rgb(255, 255, 127);"));
        gridLayout = new QGridLayout(widget3d);
        gridLayout->setSpacing(0);
        gridLayout->setContentsMargins(0, 0, 0, 0);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        label = new fishLabel(widget3d);
        label->setObjectName(QString::fromUtf8("label"));
        label->setStyleSheet(QString::fromUtf8("background-color: rgb(170, 255, 255);"));

        gridLayout->addWidget(label, 0, 0, 1, 1);


        verticalLayout->addWidget(widget3d);


        retranslateUi(qmini3dgui);

        QMetaObject::connectSlotsByName(qmini3dgui);
    } // setupUi

    void retranslateUi(QWidget *qmini3dgui)
    {
        qmini3dgui->setWindowTitle(QApplication::translate("qmini3dgui", "qmini3dgui", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("qmini3dgui", "TextLabel", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class qmini3dgui: public Ui_qmini3dgui {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QMINI3DGUI_H
