#include "fishRenderer.h"
#include <qDebug>
#include "fishIncludes.h"


#define PI 3.1415926

fishRenderer::fishRenderer()
{
    m_vector = new fishVector;

    m_matrix = new fishMatrix;
    m_matrix->SetInput(m_vector);

    m_vertex = new fishVertex;
    m_vertex->SetInput(m_matrix);

    m_transform = new fishTransform;
    m_transform->SetInput(m_matrix);

    m_device = new fishDevice;
    m_device->SetInput(m_transform);
    m_device->render_state = RENDER_STATE_COLOR;

    m_camera = NULL;

    //qDebug()<<"hhhhhhhhh";
}

fishRenderer::~fishRenderer()
{

}

// 主渲染函数
void fishRenderer::device_render_trap(trapezoid_t *trap)
{
    scanline_t scanline;
    int j, top, bottom;
    top = (int)(trap->top + 0.5f);
    bottom = (int)(trap->bottom + 0.5f);
    for (j = top; j < bottom; j++)
    {
        if (j >= 0 && j < m_device->height)
        {
            trapezoid_edge_interp(trap, (float)j + 0.5f);
            trapezoid_init_scan_line(trap, &scanline, j);
            device_draw_scanline(&scanline);
        }
        if (j >= m_device->height) break;
    }
}

// 按照 Y 坐标计算出左右两条边纵坐标等于 Y 的顶点
void fishRenderer::trapezoid_edge_interp(trapezoid_t *trap, float y)
{
    float s1 = trap->left.v2.pos.y - trap->left.v1.pos.y;
    float s2 = trap->right.v2.pos.y - trap->right.v1.pos.y;
    float t1 = (y - trap->left.v1.pos.y) / s1;
    float t2 = (y - trap->right.v1.pos.y) / s2;
    m_vertex->vertex_interp(&trap->left.v, &trap->left.v1, &trap->left.v2, t1);
    m_vertex->vertex_interp(&trap->right.v, &trap->right.v1, &trap->right.v2, t2);
}

// 根据左右两边的端点，初始化计算出扫描线的起点和步长
void fishRenderer::trapezoid_init_scan_line(const trapezoid_t *trap, scanline_t *scanline, int y)
{
    float width = trap->right.v.pos.x - trap->left.v.pos.x;
    scanline->x = (int)(trap->left.v.pos.x + 0.5f);
    scanline->w = (int)(trap->right.v.pos.x + 0.5f) - scanline->x;
    scanline->y = y;
    scanline->v = trap->left.v;
    if (trap->left.v.pos.x >= trap->right.v.pos.x) scanline->w = 0;
    m_vertex->vertex_division(&scanline->step, &trap->left.v, &trap->right.v, width);
}

//=====================================================================
// 渲染实现
//=====================================================================

// 绘制扫描线
void fishRenderer::device_draw_scanline(scanline_t *scanline)
{
    IUINT32 *framebuffer = m_device->framebuffer[scanline->y];
    float *zbuffer = m_device->zbuffer[scanline->y];
    int x = scanline->x;
    int w = scanline->w;
    int width = m_device->width;
    int render_state = m_device->render_state;
    for (; w > 0; x++, w--)
    {
        if (x >= 0 && x < width)
        {
            float rhw = scanline->v.rhw;
            if (rhw >= zbuffer[x]) {
                float w = 1.0f / rhw;
                zbuffer[x] = rhw;
                if (render_state & RENDER_STATE_COLOR)
                {
                    float r = scanline->v.color.r * w;
                    float g = scanline->v.color.g * w;
                    float b = scanline->v.color.b * w;
                    int R = (int)(r * 255.0f);
                    int G = (int)(g * 255.0f);
                    int B = (int)(b * 255.0f);
                    R = m_matrix->CMID(R, 0, 255);
                    G = m_matrix->CMID(G, 0, 255);
                    B = m_matrix->CMID(B, 0, 255);

                    framebuffer[x] = (R << 16) | (G << 8) | (B);


                }
                if (render_state & RENDER_STATE_TEXTURE)
                {
                    float u = scanline->v.tc.u * w;
                    float v = scanline->v.tc.v * w;
                    IUINT32 cc = m_texture->fishDeviceexture_read(u, v);
                    framebuffer[x] = cc;
                }
            }
        }
        m_vertex->vertex_add(&scanline->v, &scanline->step);
        if (x >= width) break;
    }
}

// 设备初始化，fb为外部帧缓存，非 NULL 将引用外部帧缓存（每行 4字节对齐）
void fishRenderer::device_init(int width, int height, void *fb)
{
    int need = sizeof(void*) * (height * 2 + 1024) + width * height * 8;
    char *ptr = (char*)malloc(need + 64);
    char *framebuf, *zbuf;
    int j;
    assert(ptr);//判断指针是否为空
    m_device->framebuffer = (IUINT32**)ptr;
    m_device->zbuffer = (float**)(ptr + sizeof(void*) * height);
    ptr += sizeof(void*) * height * 2;
    m_device->texture = (IUINT32**)ptr;
    ptr += sizeof(void*) * 1024;
    framebuf = (char*)ptr;
    zbuf = (char*)ptr + width * height * 4;
    ptr += width * height * 8;
    if (fb != NULL) framebuf = (char*)fb;
    for (j = 0; j < height; j++)
    {
        m_device->framebuffer[j] = (IUINT32*)(framebuf + width * 4 * j);
        m_device->zbuffer[j] = (float*)(zbuf + width * 4 * j);
    }
    m_device->texture[0] = (IUINT32*)ptr;
    m_device->texture[1] = (IUINT32*)(ptr + 16);
    memset(m_device->texture[0], 0, 64);
    m_device->tex_width = 2;
    m_device->tex_height = 2;
    m_device->max_u = 1.0f;
    m_device->max_v = 1.0f;
    m_device->width = width;
    m_device->height = height;
    m_device->background = 0xc0c0c0;
    m_device->foreground = 0;
    m_transform->transform_init(width, height);
    m_device->render_state = RENDER_STATE_WIREFRAME;
}

// 删除设备
void fishRenderer::device_destroy()
{
    if (m_device->framebuffer)
        free(m_device->framebuffer);
    m_device->framebuffer = NULL;
    m_device->zbuffer = NULL;
    m_device->texture = NULL;
}

// 清空 framebuffer 和 zbuffer
void fishRenderer::device_clear(int mode)
{
    int y, x, height = m_device->height;
    for (y = 0; y < m_device->height; y++)
    {
        IUINT32 *dst = m_device->framebuffer[y];
        IUINT32 cc = (height - 1 - y) * 230 / (height - 1);
        cc = (cc << 16) | (cc << 8) | cc;
        if (mode == 0) cc = m_device->background;
        for (x = m_device->width; x > 0; dst++, x--) dst[0] = cc;
    }
    for (y = 0; y < m_device->height; y++)
    {
        float *dst = m_device->zbuffer[y];
        for (x = m_device->width; x > 0; dst++, x--) dst[0] = 0.0f;
    }
}

//===========================================================================
void fishRenderer::SetfishCamera(fishCamera *pcamera)
{
    m_camera = pcamera;
    m_camera->SetRenderer(this);
}

void fishRenderer::SetfishBlocks(fishBlocks *pblocks)
{
    m_blocks = pblocks;
    m_blocks->SetRenderer(this);
}

void fishRenderer::SetfishTexture(fishTexture *ptexture)
{
    m_texture = ptexture;
    m_texture->SetRenderer(this);
}

void fishRenderer::Update(float alpha)
{
    m_blocks->draw_cube(alpha);
}

void fishRenderer::Update(float x,float y,float z,float alpha)
{
	m_blocks->draw_cube(x,y,z,alpha);
}

