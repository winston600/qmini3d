#ifndef FISHMATRIX_H
#define FISHMATRIX_H

#include "fishStruct.h"
#include "fishVector.h"

class fishMatrix
{
public:
    fishMatrix();
    void SetInput(fishVector *pvector);
    fishVector *GetfishVector(){ return m_vector;}

    int CMID(int x, int min, int max);

    // c = a + b
    void matrix_add(matrix_t *c, const matrix_t *a, const matrix_t *b);
    // c = a - b
    void matrix_sub(matrix_t *c, const matrix_t *a, const matrix_t *b);
    // c = a * b
    void matrix_mul(matrix_t *c, const matrix_t *a, const matrix_t *b);
    // c = a * f
    void matrix_scale(matrix_t *c, const matrix_t *a, float f);
    // y = x * m
    void matrix_apply(vector_t *y, const vector_t *x, const matrix_t *m);
    //把参数矩阵变成4*4单位矩阵
    void matrix_set_identity(matrix_t *m);
    //4*4 0矩阵
    void matrix_set_zero(matrix_t *m);
    // 平移变换
    void matrix_set_translate(matrix_t *m, float x, float y, float z);
    // 缩放变换
    void matrix_set_scale(matrix_t *m, float x, float y, float z);
    // 旋转矩阵
    void matrix_set_rotate(matrix_t *m, float x, float y, float z, float theta);
    // D3DXMatrixPerspectiveFovLH
    void matrix_set_perspective(matrix_t *m, float fovy, float aspect, float zn, float zf);

private:
    fishVector *m_vector;

};

#endif // FISHMATRIX_H
