/********************************************************************************
** Form generated from reading UI file 'qmini3dsimple.ui'
**
** Created by: Qt User Interface Compiler version 4.8.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QMINI3DSIMPLE_H
#define UI_QMINI3DSIMPLE_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>
#include "fishlabel.h"

QT_BEGIN_NAMESPACE

class Ui_qmini3dsimple
{
public:
    QVBoxLayout *verticalLayout;
    fishLabel *label;

    void setupUi(QWidget *qmini3dsimple)
    {
        if (qmini3dsimple->objectName().isEmpty())
            qmini3dsimple->setObjectName(QString::fromUtf8("qmini3dsimple"));
        qmini3dsimple->resize(512, 512);
        verticalLayout = new QVBoxLayout(qmini3dsimple);
        verticalLayout->setSpacing(0);
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        label = new fishLabel(qmini3dsimple);
        label->setObjectName(QString::fromUtf8("label"));

        verticalLayout->addWidget(label);


        retranslateUi(qmini3dsimple);

        QMetaObject::connectSlotsByName(qmini3dsimple);
    } // setupUi

    void retranslateUi(QWidget *qmini3dsimple)
    {
        qmini3dsimple->setWindowTitle(QApplication::translate("qmini3dsimple", "qmini3dsimple", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("qmini3dsimple", "TextLabel", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class qmini3dsimple: public Ui_qmini3dsimple {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QMINI3DSIMPLE_H
