#include "fishLabel.h"
#include <qDebug>



fishLabel::fishLabel(QWidget *parent)
    :QLabel(parent)
{
    qDebug()<<"fishLabel::fishLabel";
    width = 512;
    height = 512;


    //开辟内存 存储图像指针
    screen_fb = new unsigned char[width*height*8];		// frame buffer
    memset(screen_fb, 0, width * height * 8);

    img = new QImage(screen_fb,width, height, QImage::Format_RGB32);

    this->SetLine(0,0,200,200);


}

fishLabel::~fishLabel()
{

}

void fishLabel::paintEvent(QPaintEvent *e)
{
    //绘制图像
    QPainter painter(this);
    int img_width = img->width();
    int img_height = img->height();
    painter.drawPixmap(0, 0, img_width, img_height, QPixmap::fromImage(*img));

    this->update();
}

void fishLabel::SetPoint(int x, int y)
{
    //2020年4月26日 11点04分
    int  lineNum_32 = 0;     //行数
    int  pixelsub = 0;    //像素下标
    uchar*  imagebits_32 = img->bits();         //获取图像首地址，32位图

    lineNum_32 = x * width * 4;                  //对于任意图像，这句没有问题

    imagebits_32[ lineNum_32 + y * 4 + 2] = 255;
    imagebits_32[ lineNum_32 + y * 4 + 1] = 0;
    imagebits_32[ lineNum_32 + y * 4 ] = 0;
}

void fishLabel::SetLine(int x1, int y1, int x2, int y2)
{
    int x, y, rem = 0;
    if (x1 == x2 && y1 == y2)
    {
        SetPoint(x1, y1);
    }
    else if (x1 == x2)
    {
        int inc = (y1 <= y2) ? 1 : -1;
        for (y = y1; y != y2; y += inc)
            SetPoint(x1, y);
        SetPoint(x2, y2);
    }
    else if (y1 == y2)
    {
        int inc = (x1 <= x2) ? 1 : -1;
        for (x = x1; x != x2; x += inc)
            SetPoint(x, y1);
        SetPoint(x2, y2);
    }
    else
    {
        int dx = (x1 < x2) ? x2 - x1 : x1 - x2;
        int dy = (y1 < y2) ? y2 - y1 : y1 - y2;
        if (dx >= dy)
        {
            if (x2 < x1) x = x1, y = y1, x1 = x2, y1 = y2, x2 = x, y2 = y;
            for (x = x1, y = y1; x <= x2; x++)
            {
                SetPoint( x, y );
                rem += dy;
                if (rem >= dx)
                {
                    rem -= dx;
                    y += (y2 >= y1) ? 1 : -1;
                    SetPoint( x, y );
                }
            }
            SetPoint( x2, y2 );
        }
        else
        {
            if (y2 < y1) x = x1, y = y1, x1 = x2, y1 = y2, x2 = x, y2 = y;
            for (x = x1, y = y1; y <= y2; y++)
            {
                SetPoint( x, y );
                rem += dx;
                if (rem >= dy)
                {
                    rem -= dy;
                    x += (x2 >= x1) ? 1 : -1;
                    SetPoint( x, y );
                }
            }
            SetPoint( x2, y2 );
        }
    }
}

/*
void fishLabel::SetColor(int x, int y)
{
    //2020年4月26日 11点04分
    int  lineNum_32 = 0;     //行数
    int  pixelsub = 0;    //像素下标
    uchar*  imagebits_32 = img->bits();         //获取图像首地址，32位图

    for(int i=0; i<height; ++i)
    {
        //按照通常的理解，我们会如下处理，取每行
        lineNum_32 = i * width * 4;                  //对于任意图像，这句没有问题
        // lineNum_24 = i * width * 3;             //？？当width不是4的整数倍时，这句取不到每行开头
        // lineNum_8 = i * width;                 //？？当width不是4的整数倍时，这句取不到每行开头

        for(int j=0; j<width; ++j)
        {
            int r_32 = imagebits_32[ lineNum_32 + j * 4 + 2];
            int g_32 = imagebits_32[ lineNum_32 + j * 4 + 1];
            int b_32 = imagebits_32[ lineNum_32 + j * 4 ];

            //qDebug()<<r_32<<g_32<<b_32;

            // int r_24 = imagebits_24[ lineNum_24 + j * 3];        //注意区别32位的图
            // int g_24 = imagebits_24[ lineNum_24 + j *3 + 1];
            // int b_24 = imagebits_24[ lineNum_24 + j * 3 + 2];

            // int gray_8 = imagebits_8[ lineNum_8 + j];

            //自己的操作
            imagebits_32[ lineNum_32 + j * 4 + 2] = 255;
            imagebits_32[ lineNum_32 + j * 4 + 1] = 0;
            imagebits_32[ lineNum_32 + j * 4 ] = 0;

        }
    }
}

*/

//
//int width=img->width();//图像宽
//int height=img->height();//图像高

//int bytePerLine=(width*24+31)/8;//图像每行字节对齐

//graydata=new unsigned char[bytePerLine*height];//存储处理后的数据

//unsigned char r,g,b;
//for (int i=0;i<height;i++)
//{
//    for (int j=0;j<width;j++)
//    {
//        r = *(data+2);
//        g = *(data+1);
//        b = *data;

//        graydata[i*bytePerLine+j*3]  =(r*30+g*59+b*11)/100;
//        graydata[i*bytePerLine+j*3+1]=(r*30+g*59+b*11)/100;
//        graydata[i*bytePerLine+j*3+2]=(r*30+g*59+b*11)/100;

//        data+=4;
//    }
//}

