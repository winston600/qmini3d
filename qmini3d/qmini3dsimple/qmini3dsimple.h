#ifndef QMINI3DSIMPLE_H
#define QMINI3DSIMPLE_H

#include <QWidget>

namespace Ui {
    class qmini3dsimple;
}

class qmini3dsimple : public QWidget
{
    Q_OBJECT

public:
    explicit qmini3dsimple(QWidget *parent = 0);
    ~qmini3dsimple();

private:
    Ui::qmini3dsimple *ui;
};

#endif // QMINI3DSIMPLE_H
