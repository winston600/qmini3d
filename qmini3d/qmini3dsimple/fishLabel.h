#pragma once

#include <QLabel>
#include <QMouseEvent>
#include <QPaintEvent>
#include <QKeyEvent>
#include <QPainter>
#include <QImage>
#include <qDebug.h>

class fishLabel : public QLabel
{
    Q_OBJECT

public:
    fishLabel(QWidget *parent);
    ~fishLabel();


protected:
    void paintEvent(QPaintEvent *e);
//    void keyPressEvent(QKeyEvent * event);
//    void mousePressEvent(QMouseEvent * event);
//    void mouseMoveEvent(QMouseEvent *e);


private:
    unsigned char *screen_fb;

    QImage *img;
    int height;
    int width;

    void SetPoint(int x,int y);
    void SetLine(int x1,int y1,int x2,int y2);


};
