#ifndef FISHCAMERA_H
#define FISHCAMERA_H

#include "fishIncludes.h"
#include "fishStruct.h"


class fishRenderer;
class fishCamera
{
public:
    fishCamera();
    void SetRenderer(fishRenderer *pRenderer);

    void matrixSetLookat(matrix_t *m, const fishVector *eye, const fishVector *at, const fishVector *up);
    void cameraAtZero(float x, float y, float z);

private:

    fishRenderer *m_renderer;

};

#endif // FISHCAMERA_H
