#ifndef FISHEDGE_H
#define FISHEDGE_H
#include "fishVertex.h"

class fishEdge
{
public:
    fishEdge();

    fishVertex v, v1, v2;
};

#endif // FISHEDGE_H
