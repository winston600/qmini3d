#ifndef FISHINTERACTORWIDGET_H
#define FISHINTERACTORWIDGET_H

#include <QWidget>
#include <QKeyEvent>
#include <QMouseEvent>

namespace Ui {
    class fishInteractorWidget;
}

class fishInteractorWidget : public QWidget
{
    Q_OBJECT

public:
    explicit fishInteractorWidget(QWidget *parent = 0);
    ~fishInteractorWidget();

protected:
    void keyPressEvent(QKeyEvent *event);
    void mousePressEvent(QMouseEvent * event);
    void mouseMoveEvent(QMouseEvent *event);

private:
    Ui::fishInteractorWidget *ui;

    float alpha;
    float pos;
    QPoint lastPos;
    int kbhit;
    int indicator;
    int states[2];
};

#endif // FISHINTERACTORWIDGET_H
