#include "fishInteractorWidget.h"
#include "ui_fishInteractorWidget.h"

#include "fishStruct.h"

fishInteractorWidget::fishInteractorWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::fishInteractorWidget)
{
    ui->setupUi(this);

    alpha = 1;
    pos = 3.5;

    kbhit = 0;
    indicator = 0;
    states[0] = RENDER_STATE_COLOR;
    states[1] = RENDER_STATE_WIREFRAME;
}

fishInteractorWidget::~fishInteractorWidget()
{
    delete ui;
}

void fishInteractorWidget::keyPressEvent(QKeyEvent *event)
{
    if (event->key() == Qt::Key_Left)
    {
        alpha += 0.1f;
        ui->volumeLabel->Setalpha(alpha);
    }

    if (event->key() == Qt::Key_Right)
    {
        alpha -= 0.1f;
        ui->volumeLabel->Setalpha(alpha);
    }

    if (event->key() == Qt::Key_Up)
    {
        pos -= 0.1f;
        ui->volumeLabel->Setpos(pos);
    }

    if (event->key() == Qt::Key_Down)
    {
        pos += 0.1f;
        ui->volumeLabel->Setpos(pos);
    }
    if (event->key() == Qt::Key_W)
    {
        if (++indicator >= 2)
            indicator = 0;
        ui->volumeLabel->m_renderer->GetfishDevice()->render_state = states[indicator];
    }

}

void fishInteractorWidget::mousePressEvent(QMouseEvent *event)
{
    lastPos = event->pos();
}

void fishInteractorWidget::mouseMoveEvent(QMouseEvent *event)
{
    int dx = event->x() - lastPos.x();
    int dy = event->y() - lastPos.y();

    switch (event->buttons())
    {
    case Qt::LeftButton:
        /*setXRotation(xRot + 2 * dy);
                setYRotation(yRot + 2 * dx);
                setZRotation(zRot + 2 * dx);*/

        /*dx = dx/sqrt(dx*dx+dy*dy);
                dy=dy / sqrt(dx*dx + dy*dy);*/
        //ui->volumeLabel->Setxyz(dx,dy);

        if (dx > 0)
        {
            alpha -= 0.03f;
            ui->volumeLabel->Setalpha(alpha);
        }
        else
        {
            alpha += 0.03f;
            ui->volumeLabel->Setalpha(alpha);
        }
        break;

    case Qt::RightButton:
        if (dy > 0)
        {
            pos -= 0.03f;
            ui->volumeLabel->Setpos(pos);
        }
        else
        {
            pos += 0.03f;
            ui->volumeLabel->Setpos(pos);
        }
        break;
    }
    lastPos = event->pos();
}

//void fishInteractorWidget::normalizeAngle(int *angle)
//{
//	while (*angle < 0)
//		*angle += 360 * 16;
//	while (*angle > 360 * 16)
//		*angle -= 360 * 16;
//}
//
//void fishInteractorWidget::setXRotation(int angle)
//{
//	normalizeAngle(&angle);
//	if (angle != xRot) {
//		xRot = angle;
//		emit xRotationChanged(angle);
//		neheWidget->setxRot(xRot);
//		neheWidget->updateGL();
//	}
//}
//
//void fishInteractorWidget::setYRotation(int angle)
//{
//	normalizeAngle(&angle);
//	if (angle != yRot) {
//		yRot = angle;
//		emit yRotationChanged(angle);
//		neheWidget->setyRot(yRot);
//		neheWidget->updateGL();
//	}
//}
//
//void fishInteractorWidget::setZRotation(int angle)
//{
//	normalizeAngle(&angle);
//	if (angle != zRot) {
//		zRot = angle;
//		emit zRotationChanged(angle);
//		neheWidget->setzRot(zRot);
//		neheWidget->updateGL();
//	}
//}
