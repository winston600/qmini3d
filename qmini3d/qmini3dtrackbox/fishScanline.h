#ifndef FISHSCANLINE_H
#define FISHSCANLINE_H

#include "fishVertex.h"

class fishScanline
{
public:
    fishScanline();

    fishVertex v, step;
    int x, y, w;
};

#endif // FISHSCANLINE_H
