#include "fishVector.h"

#include <math.h>

fishVector::fishVector()
{
}

// 计算插值：t 为 [0, 1] 之间的数值
float fishVector::interp(float x1, float x2, float t)
{
    return x1 + (x2 - x1) * t;
}

// | v |
float fishVector::vectorLength()
{
    float sq = this->x * this->x + this->y * this->y + this->z * this->z;
    return (float)sqrt(sq);
}

// z = x + y
void fishVector::vectorAdd(const fishVector *x, const fishVector *y)
{
    this->x = x->x + y->x;
    this->y = x->y + y->y;
    this->z = x->z + y->z;
    this->w = 1.0;
}

// z = x - y
void fishVector::vectorSub(const fishVector *x, const fishVector *y)
{
    this->x = x->x - y->x;
    this->y = x->y - y->y;
    this->z = x->z - y->z;
    this->w = 1.0;
}

// 矢量点乘
float fishVector::vectorDotproduct(const fishVector *x, const fishVector *y)
{
    return x->x * y->x + x->y * y->y + x->z * y->z;
}

// 矢量叉乘
void fishVector::vectorCrossproduct(const fishVector *x, const fishVector *y)
{
    float m1, m2, m3;
    m1 = x->y * y->z - x->z * y->y;
    m2 = x->z * y->x - x->x * y->z;
    m3 = x->x * y->y - x->y * y->x;
    this->x = m1;
    this->y = m2;
    this->z = m3;
    this->w = 1.0f;
}

// 矢量插值，t取值 [0, 1]
void fishVector::vectorInterp(const fishVector *x1, const fishVector *x2, float t)
{
    this->x = interp(x1->x, x2->x, t);
    this->y = interp(x1->y, x2->y, t);
    this->z = interp(x1->z, x2->z, t);
    this->w = 1.0f;
}
void fishVector::vectorInterp(fishVector *u, const fishVector *x1, const fishVector *x2, float t)
{
    u->x = interp(x1->x, x2->x, t);
    u->y = interp(x1->y, x2->y, t);
    u->z = interp(x1->z, x2->z, t);
    u->w = 1.0f;
}
// 矢量归一化
void fishVector::vectorNormalize()
{
    float length = this->vectorLength();
    if (length != 0.0f)
    {
        float inv = 1.0f / length;
        this->x *= inv;
        this->y *= inv;
        this->z *= inv;
    }
}

