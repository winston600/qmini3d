#include "fishTexture.h"


#include "fishRenderer.h"

fishTexture::fishTexture()
{
    m_renderer = NULL;
}

void fishTexture::initTexture()
{
    static IUINT32 texture[256][256];
    int i, j;
    for (j = 0; j < 256; j++)
    {
        for (i = 0; i < 256; i++)
        {
            int x = i / 32, y = j / 32;
            texture[j][i] = ((x + y) & 1) ? 0xffffff : 0x3fbcef;
        }
    }
    this->deviceSetTexture(texture, 256 * 4, 256, 256);
}
// 设置当前纹理
void fishTexture::deviceSetTexture(void *bits, long pitch, int w, int h)
{
    char *ptr = (char*)bits;
    int j;
    assert(w <= 1024 && h <= 1024);
    for (j = 0; j < h; ptr += pitch, j++) 	// 重新计算每行纹理的指针
        m_renderer->GetfishDevice()->texture[j] = (IUINT32*)ptr;
    m_renderer->GetfishDevice()->tex_width = w;
    m_renderer->GetfishDevice()->tex_height = h;
    m_renderer->GetfishDevice()->max_u = (float)(w - 1);
    m_renderer->GetfishDevice()->max_v = (float)(h - 1);
}
// 根据坐标读取纹理
IUINT32 fishTexture::fishdeviceextureRead(float u, float v)
{
    int x, y;
    u = u * m_renderer->GetfishDevice()->max_u;
    v = v * m_renderer->GetfishDevice()->max_v;
    x = (int)(u + 0.5f);
    y = (int)(v + 0.5f);
    x = m_renderer->GetfishMatrix()->CMID(x, 0, m_renderer->GetfishDevice()->tex_width - 1);
    y = m_renderer->GetfishMatrix()->CMID(y, 0, m_renderer->GetfishDevice()->tex_height - 1);
    return m_renderer->GetfishDevice()->texture[y][x];
}

void fishTexture::SetRenderer(fishRenderer *pRenderer)
{
     m_renderer = pRenderer;
}
