#include "fishCamera.h"
#include "fishRenderer.h"

fishCamera::fishCamera()
{
    m_renderer = NULL;
}

// ���������
void fishCamera::matrixSetLookat(matrix_t *m, const fishVector *eye, const fishVector *at, const fishVector *up)
{
    fishVector xaxis, yaxis, zaxis;

    //m_renderer->GetfishVector()->vectorSub(&zaxis, at, eye);
    zaxis.vectorSub(at,eye);

    //m_renderer->GetfishVector()->vectorNormalize(&zaxis);
    zaxis.vectorNormalize();

    //m_renderer->GetfishVector()->vectorCrossproduct(&xaxis, up, &zaxis);
    xaxis.vectorCrossproduct(up,&zaxis);

    //m_renderer->GetfishVector()->vectorNormalize(&xaxis);
    xaxis.vectorNormalize();

    //m_renderer->GetfishVector()->vectorCrossproduct(&yaxis, &zaxis, &xaxis);
    yaxis.vectorCrossproduct(&zaxis,&xaxis);

    m->m[0][0] = xaxis.x;
    m->m[1][0] = xaxis.y;
    m->m[2][0] = xaxis.z;
    m->m[3][0] = -m_renderer->GetfishVector()->vectorDotproduct(&xaxis, eye);

    m->m[0][1] = yaxis.x;
    m->m[1][1] = yaxis.y;
    m->m[2][1] = yaxis.z;
    m->m[3][1] = -m_renderer->GetfishVector()->vectorDotproduct(&yaxis, eye);

    m->m[0][2] = zaxis.x;
    m->m[1][2] = zaxis.y;
    m->m[2][2] = zaxis.z;
    m->m[3][2] = -m_renderer->GetfishVector()->vectorDotproduct(&zaxis, eye);

    m->m[0][3] = m->m[1][3] = m->m[2][3] = 0.0f;
    m->m[3][3] = 1.0f;
}

void fishCamera::cameraAtZero(float x, float y, float z)
{
    fishPoint eye;
    eye.x = x;
    eye.y = y;
    eye.z = z;
    eye.w = 1;

    fishPoint at;
    at.x = 0;
    at.y = 0;
    at.z = 0;
    at.w = 1;

    fishPoint up;
    up.x = 0;
    up.y = 0;
    up.z = 1;
    up.w = 1;

    this->matrixSetLookat(&m_renderer->GetfishDevice()->GetFishTransform()->view, &eye, &at, &up);
    m_renderer->GetfishTransform()->transform_update();
}

void fishCamera::SetRenderer(fishRenderer *pRenderer)
{
    m_renderer = pRenderer;
}

