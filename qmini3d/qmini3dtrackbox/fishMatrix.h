#ifndef FISHMATRIX_H
#define FISHMATRIX_H

#include "fishStruct.h"
#include "fishVector.h"


class fishMatrix
{
public:
    fishMatrix();
    void SetInput(fishVector *pvector);
    fishVector *GetfishVector(){ return m_vector;}

    int CMID(int x, int min, int max);

    // c = a + b
    void matrixAdd(matrix_t *c, const matrix_t *a, const matrix_t *b);
    // c = a - b
    void matrixSub(matrix_t *c, const matrix_t *a, const matrix_t *b);
    // c = a * b
    void matrixMul(matrix_t *c, const matrix_t *a, const matrix_t *b);
    // c = a * f
    void matrixScale(matrix_t *c, const matrix_t *a, float f);
    // y = x * m
    void matrixApply(fishVector *y, const fishVector *x, const matrix_t *m);
    //把参数矩阵变成4*4单位矩阵
    void matrixSetIdentity(matrix_t *m);
    //4*4 0矩阵
    void matrixSetZero(matrix_t *m);
    // 平移变换
    void matrixSetTranslate(matrix_t *m, float x, float y, float z);
    // 缩放变换
    void matrixSetScale(matrix_t *m, float x, float y, float z);
    // 旋转矩阵
    void matrixSetRotate(matrix_t *m, float x, float y, float z, float theta);
    // D3DXMatrixPerspectiveFovLH
    void matrixSetPerspective(matrix_t *m, float fovy, float aspect, float zn, float zf);

private:
    fishVector *m_vector;

};

#endif // FISHMATRIX_H
