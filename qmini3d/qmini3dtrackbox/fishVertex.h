#ifndef FISHVERTEX_H
#define FISHVERTEX_H


#include "fishVector.h"
#include "fishPoint.h"
#include "fishColor.h"
#include "fishTexcoord.h"


class fishMatrix;
class fishVertex
{
public:
    fishVertex();

    fishPoint pos;
    fishTexcoord tc;
    fishColor color;
    float rhw;

    void SetInput(fishMatrix *pmatrix);

    void vertexRhwInit(fishVertex *v);
    void vertexInterp(fishVertex *y, const fishVertex *x1, const fishVertex *x2, float t);
    void vertexDivision(fishVertex *y, const fishVertex *x1, const fishVertex *x2, float w);
    void vertexAdd(fishVertex *y, const fishVertex *x);

private:
    fishMatrix *m_matrix;
};

#endif // FISHVERTEX_H
