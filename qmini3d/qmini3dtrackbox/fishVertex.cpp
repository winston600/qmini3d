#include "fishVertex.h"

#include "fishIncludes.h"
#include "fishMatrix.h"

fishVertex::fishVertex()
{
    m_matrix = NULL;
}

void fishVertex::vertexRhwInit(fishVertex *v)
{
    float rhw = 1.0f / v->pos.w;
    v->rhw = rhw;
    v->tc.u *= rhw;
    v->tc.v *= rhw;
    v->color.r *= rhw;
    v->color.g *= rhw;
    v->color.b *= rhw;
}

void fishVertex::vertexInterp(fishVertex *y, const fishVertex *x1, const fishVertex *x2, float t)
{
    if(m_matrix == NULL)
    {
        std::cout<<"fishVertex::vertex_interp : m_matrix == NULL"<<std::endl;
        return;
    }
    m_matrix->GetfishVector()->vectorInterp(&y->pos, &x1->pos, &x2->pos, t);
    //y->pos = m_matrix->GetfishVector()->vectorInterp(x1->pos, x2->pos, t);

    y->tc.u = m_matrix->GetfishVector()->interp(x1->tc.u, x2->tc.u, t);
    y->tc.v = m_matrix->GetfishVector()->interp(x1->tc.v, x2->tc.v, t);
    y->color.r = m_matrix->GetfishVector()->interp(x1->color.r, x2->color.r, t);
    y->color.g = m_matrix->GetfishVector()->interp(x1->color.g, x2->color.g, t);
    y->color.b = m_matrix->GetfishVector()->interp(x1->color.b, x2->color.b, t);
    y->rhw = m_matrix->GetfishVector()->interp(x1->rhw, x2->rhw, t);
}

void fishVertex::vertexDivision(fishVertex *y, const fishVertex *x1, const fishVertex *x2, float w)
{
    float inv = 1.0f / w;
    y->pos.x = (x2->pos.x - x1->pos.x) * inv;
    y->pos.y = (x2->pos.y - x1->pos.y) * inv;
    y->pos.z = (x2->pos.z - x1->pos.z) * inv;
    y->pos.w = (x2->pos.w - x1->pos.w) * inv;
    y->tc.u = (x2->tc.u - x1->tc.u) * inv;
    y->tc.v = (x2->tc.v - x1->tc.v) * inv;
    y->color.r = (x2->color.r - x1->color.r) * inv;
    y->color.g = (x2->color.g - x1->color.g) * inv;
    y->color.b = (x2->color.b - x1->color.b) * inv;
    y->rhw = (x2->rhw - x1->rhw) * inv;
}

void fishVertex::vertexAdd(fishVertex *y, const fishVertex *x)
{
    y->pos.x += x->pos.x;
    y->pos.y += x->pos.y;
    y->pos.z += x->pos.z;
    y->pos.w += x->pos.w;
    y->rhw += x->rhw;
    y->tc.u += x->tc.u;
    y->tc.v += x->tc.v;
    y->color.r += x->color.r;
    y->color.g += x->color.g;
    y->color.b += x->color.b;
}

void fishVertex::SetInput(fishMatrix *pmatrix)
{
    m_matrix = pmatrix;
}
