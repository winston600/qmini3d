#include "fishTransform.h"
#include <stdio.h>

fishTransform::fishTransform()
{
     m_matrix = NULL;
}

// 矩阵更新，计算 transform = world * view * projection
void fishTransform::transform_update()
{
    matrix_t m;
    m_matrix->matrixMul(&m, &world, &view);
    m_matrix->matrixMul(&transform, &m, &projection);
}

// 初始化，设置屏幕长宽
void fishTransform::transform_init(int width, int height)
{
    float aspect = (float)width / ((float)height);
    m_matrix->matrixSetIdentity( &world);
    m_matrix->matrixSetIdentity( &view);
    m_matrix->matrixSetPerspective( &projection, 3.1415926f * 0.5f, aspect, 1.0f, 500.0f);
    this->w = (float)width;
    this->h = (float)height;
    transform_update();
}

// 将矢量 x 进行 project
void fishTransform::transform_apply(fishVector *y, const fishVector *x)
{
    m_matrix->matrixApply(y, x,  &transform);
}

// 检查齐次坐标同 cvv 的边界用于视锥裁剪
int fishTransform::transform_check_cvv(const fishVector *v)
{
    float w = v->w;
    int check = 0;
    if (v->z < 0.0f) check |= 1;
    if (v->z >  w) check |= 2;
    if (v->x < -w) check |= 4;
    if (v->x >  w) check |= 8;
    if (v->y < -w) check |= 16;
    if (v->y >  w) check |= 32;
    return check;
}

// 归一化，得到屏幕坐标
void fishTransform::transform_homogenize(fishVector *y, const fishVector *x)
{
    float rhw = 1.0f / x->w;
    y->x = (x->x * rhw + 1.0f) * this->w * 0.5f;
    y->y = (1.0f - x->y * rhw) * this->h * 0.5f;
    y->z = x->z * rhw;
    y->w = 1.0f;
}

void fishTransform::SetInput(fishMatrix *pmatrix)
{
    m_matrix = pmatrix;
}
