#ifndef FISHTEXTURE_H
#define FISHTEXTURE_H



#include "fishIncludes.h"

#include "fishStruct.h"
#include "fishDevice.h"


class fishRenderer;
class fishTexture
{
public:
    fishTexture();
    void SetRenderer(fishRenderer *pRenderer);

    void initTexture();
    // 设置当前纹理
    void deviceSetTexture(void *bits, long pitch, int w, int h);
    // 根据坐标读取纹理
    IUINT32 fishdeviceextureRead(float u, float v);


private:
    fishRenderer *m_renderer;
};

#endif // FISHTEXTURE_H
