#include "fishBlocks.h"
#include "fishIncludes.h"

#include "fishRenderer.h"

#include "fishCube.h"

fishBlocks::fishBlocks()
{
    m_renderer = NULL;
}

// 画点
void fishBlocks::devicePixel(int x, int y, IUINT32 color)
{
    if (((IUINT32)x) < (IUINT32)m_renderer->GetfishDevice()->width && ((IUINT32)y) < (IUINT32)m_renderer->GetfishDevice()->height)
    {
        m_renderer->GetfishDevice()->framebuffer[y][x] = color;
    }
}

// 绘制线段
void fishBlocks::deviceDrawLine(int x1, int y1, int x2, int y2, IUINT32 c)
{
    int x, y, rem = 0;
    if (x1 == x2 && y1 == y2)
    {
        devicePixel(x1, y1, c);
    }
    else if (x1 == x2)
    {
        int inc = (y1 <= y2) ? 1 : -1;
        for (y = y1; y != y2; y += inc) devicePixel(x1, y, c);
        devicePixel(x2, y2, c);
    }
    else if (y1 == y2)
    {
        int inc = (x1 <= x2) ? 1 : -1;
        for (x = x1; x != x2; x += inc) devicePixel(x, y1, c);
        devicePixel(x2, y2, c);
    }
    else {
        int dx = (x1 < x2) ? x2 - x1 : x1 - x2;
        int dy = (y1 < y2) ? y2 - y1 : y1 - y2;
        if (dx >= dy)
        {
            if (x2 < x1) x = x1, y = y1, x1 = x2, y1 = y2, x2 = x, y2 = y;
            for (x = x1, y = y1; x <= x2; x++)
            {
                devicePixel(x, y, c);
                rem += dy;
                if (rem >= dx)
                {
                    rem -= dx;
                    y += (y2 >= y1) ? 1 : -1;
                    devicePixel(x, y, c);
                }
            }
            devicePixel(x2, y2, c);
        }
        else
        {
            if (y2 < y1) x = x1, y = y1, x1 = x2, y1 = y2, x2 = x, y2 = y;
            for (x = x1, y = y1; y <= y2; y++)
            {
                devicePixel(x, y, c);
                rem += dx;
                if (rem >= dy)
                {
                    rem -= dy;
                    x += (x2 >= x1) ? 1 : -1;
                    devicePixel(x, y, c);
                }
            }
            devicePixel(x2, y2, c);
        }
    }
}

// 根据 render_state 绘制原始三角形
void fishBlocks::deviceDrawPrimitive(const fishVertex *v1,
                                         const fishVertex *v2, const fishVertex *v3)
{
    fishPoint p1, p2, p3, c1, c2, c3;
    int render_state = m_renderer->GetfishDevice()->render_state;

    // 按照 Transform 变化
    m_renderer->GetfishTransform()->transform_apply(&c1, &v1->pos);
    m_renderer->GetfishTransform()->transform_apply(&c2, &v2->pos);
    m_renderer->GetfishTransform()->transform_apply(&c3, &v3->pos);

    // 裁剪，注意此处可以完善为具体判断几个点在 cvv内以及同cvv相交平面的坐标比例
    // 进行进一步精细裁剪，将一个分解为几个完全处在 cvv内的三角形
    if (m_renderer->GetfishTransform()->transform_check_cvv(&c1) != 0) return;
    if (m_renderer->GetfishTransform()->transform_check_cvv(&c2) != 0) return;
    if (m_renderer->GetfishTransform()->transform_check_cvv(&c3) != 0) return;

    // 归一化
    m_renderer->GetfishTransform()->transform_homogenize(&p1, &c1);
    m_renderer->GetfishTransform()->transform_homogenize(&p2, &c2);
    m_renderer->GetfishTransform()->transform_homogenize(&p3, &c3);

    // 纹理或者色彩绘制
    if (render_state & (RENDER_STATE_TEXTURE | RENDER_STATE_COLOR))
    {
        fishVertex t1 = *v1, t2 = *v2, t3 = *v3;
        fishTrapezoid traps[2];
        int n;

        t1.pos = p1;
        t2.pos = p2;
        t3.pos = p3;
        t1.pos.w = c1.w;
        t2.pos.w = c2.w;
        t3.pos.w = c3.w;

        m_renderer->GetfishVertex()->vertexRhwInit(&t1);	// 初始化 w
        m_renderer->GetfishVertex()->vertexRhwInit(&t2);	// 初始化 w
        m_renderer->GetfishVertex()->vertexRhwInit(&t3);	// 初始化 w

        // 拆分三角形为0-2个梯形，并且返回可用梯形数量
        n = trapezoidInitTriangle(traps, &t1, &t2, &t3);

        if (n >= 1) m_renderer->deviceRenderTrap(&traps[0]);
        if (n >= 2) m_renderer->deviceRenderTrap(&traps[1]);
    }

    if (render_state & RENDER_STATE_WIREFRAME)
    {		// 线框绘制
        deviceDrawLine((int)p1.x, (int)p1.y, (int)p2.x, (int)p2.y, m_renderer->GetfishDevice()->foreground);
        deviceDrawLine((int)p1.x, (int)p1.y, (int)p3.x, (int)p3.y, m_renderer->GetfishDevice()->foreground);
        deviceDrawLine((int)p3.x, (int)p3.y, (int)p2.x, (int)p2.y, m_renderer->GetfishDevice()->foreground);
    }
}

// 根据三角形生成 0-2 个梯形，并且返回合法梯形的数量
int fishBlocks::trapezoidInitTriangle(fishTrapezoid *trap, const fishVertex *p1,
                                         const fishVertex *p2, const fishVertex *p3)
{
    const fishVertex *p;
    float k, x;

    if (p1->pos.y > p2->pos.y) p = p1, p1 = p2, p2 = p;
    if (p1->pos.y > p3->pos.y) p = p1, p1 = p3, p3 = p;
    if (p2->pos.y > p3->pos.y) p = p2, p2 = p3, p3 = p;

    if (p1->pos.y == p2->pos.y && p1->pos.y == p3->pos.y) return 0;
    if (p1->pos.x == p2->pos.x && p1->pos.x == p3->pos.x) return 0;

    if (p1->pos.y == p2->pos.y)
    {	// triangle down
        if (p1->pos.x > p2->pos.x) p = p1, p1 = p2, p2 = p;
        trap[0].top = p1->pos.y;
        trap[0].bottom = p3->pos.y;
        trap[0].left.v1 = *p1;
        trap[0].left.v2 = *p3;
        trap[0].right.v1 = *p2;
        trap[0].right.v2 = *p3;
        return (trap[0].top < trap[0].bottom) ? 1 : 0;
    }

    if (p2->pos.y == p3->pos.y)
    {	// triangle up
        if (p2->pos.x > p3->pos.x) p = p2, p2 = p3, p3 = p;
        trap[0].top = p1->pos.y;
        trap[0].bottom = p3->pos.y;
        trap[0].left.v1 = *p1;
        trap[0].left.v2 = *p2;
        trap[0].right.v1 = *p1;
        trap[0].right.v2 = *p3;
        return (trap[0].top < trap[0].bottom) ? 1 : 0;
    }

    trap[0].top = p1->pos.y;
    trap[0].bottom = p2->pos.y;
    trap[1].top = p2->pos.y;
    trap[1].bottom = p3->pos.y;

    k = (p3->pos.y - p1->pos.y) / (p2->pos.y - p1->pos.y);
    x = p1->pos.x + (p2->pos.x - p1->pos.x) * k;

    if (x <= p3->pos.x)
    {		// triangle left
        trap[0].left.v1 = *p1;
        trap[0].left.v2 = *p2;
        trap[0].right.v1 = *p1;
        trap[0].right.v2 = *p3;
        trap[1].left.v1 = *p2;
        trap[1].left.v2 = *p3;
        trap[1].right.v1 = *p1;
        trap[1].right.v2 = *p3;
    }
    else
    {					// triangle right
        trap[0].left.v1 = *p1;
        trap[0].left.v2 = *p3;
        trap[0].right.v1 = *p1;
        trap[0].right.v2 = *p2;
        trap[1].left.v1 = *p1;
        trap[1].left.v2 = *p3;
        trap[1].right.v1 = *p2;
        trap[1].right.v2 = *p3;
    }

    return 2;
}

//面
void fishBlocks::drawPlane(int a, int b, int c, int d)
{
    fishVertex p1 = fishCube::GetfishCube()->cube_mesh[a];
    fishVertex p2 = fishCube::GetfishCube()->cube_mesh[b];
    fishVertex p3 = fishCube::GetfishCube()->cube_mesh[c];
    fishVertex p4 = fishCube::GetfishCube()->cube_mesh[d];

    p1.tc.u = 0, p1.tc.v = 0, p2.tc.u = 0, p2.tc.v = 1;
    p3.tc.u = 1, p3.tc.v = 1, p4.tc.u = 1, p4.tc.v = 0;
    this->deviceDrawPrimitive(&p1, &p2, &p3);
    this->deviceDrawPrimitive(&p3, &p4, &p1);
}
//cube
void fishBlocks::drawCube(float theta)
{
    matrix_t m;
    m_renderer->GetfishMatrix()->matrixSetRotate(&m, -1, -0.5, 1, theta);
    m_renderer->GetfishDevice()->GetFishTransform()->world = m;
    m_renderer->GetfishTransform()->transform_update();
    drawPlane(0, 1, 2, 3);
    drawPlane(4, 5, 6, 7);
    drawPlane(0, 4, 5, 1);
    drawPlane(1, 5, 6, 2);
    drawPlane(2, 6, 7, 3);
    drawPlane(3, 7, 4, 0);

    //    qDebug()<<"fishRenderer::draw_cube";
    //    std::cout<<"fishRenderer::draw_cube done"<<std::endl;
}
//=================================================================
void fishBlocks::SetRenderer(fishRenderer *pRenderer)
{
    m_renderer = pRenderer;
}

void fishBlocks::Update()
{
}




