#ifndef FISHBLOCKS_H
#define FISHBLOCKS_H

#include "fishStruct.h"
#include "fishDevice.h"

class fishRenderer;

class fishBlocks
{
public:
    fishBlocks();
    void SetRenderer(fishRenderer *pRenderer);
    void Update();

    // 画点
    void devicePixel(int x, int y, IUINT32 color);
    // 绘制线段
    void deviceDrawLine(int x1, int y1, int x2, int y2, IUINT32 c);
    // 根据 render_state 绘制原始三角形
    void deviceDrawPrimitive(const fishVertex *v1,
                                            const fishVertex *v2, const fishVertex *v3);
    // 根据三角形生成 0-2 个梯形，并且返回合法梯形的数量
    int trapezoidInitTriangle(fishTrapezoid *trap, const fishVertex *p1,
                                             const fishVertex *p2, const fishVertex *p3);

    //面
    void drawPlane(int a, int b, int c, int d);
    //cube
    void drawCube(float theta);

private:
    fishRenderer *m_renderer;
};

#endif // FISHBLOCKS_H
