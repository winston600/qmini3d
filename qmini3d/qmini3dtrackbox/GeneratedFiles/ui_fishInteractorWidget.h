/********************************************************************************
** Form generated from reading UI file 'fishInteractorWidget.ui'
**
** Created by: Qt User Interface Compiler version 4.8.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FISHINTERACTORWIDGET_H
#define UI_FISHINTERACTORWIDGET_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>
#include "fishLabel.h"

QT_BEGIN_NAMESPACE

class Ui_fishInteractorWidget
{
public:
    QVBoxLayout *verticalLayout;
    fishLabel *volumeLabel;

    void setupUi(QWidget *fishInteractorWidget)
    {
        if (fishInteractorWidget->objectName().isEmpty())
            fishInteractorWidget->setObjectName(QString::fromUtf8("fishInteractorWidget"));
        fishInteractorWidget->resize(512, 512);
        fishInteractorWidget->setMinimumSize(QSize(512, 512));
        verticalLayout = new QVBoxLayout(fishInteractorWidget);
        verticalLayout->setSpacing(0);
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        volumeLabel = new fishLabel(fishInteractorWidget);
        volumeLabel->setObjectName(QString::fromUtf8("volumeLabel"));
        volumeLabel->setMinimumSize(QSize(512, 512));

        verticalLayout->addWidget(volumeLabel);


        retranslateUi(fishInteractorWidget);

        QMetaObject::connectSlotsByName(fishInteractorWidget);
    } // setupUi

    void retranslateUi(QWidget *fishInteractorWidget)
    {
        fishInteractorWidget->setWindowTitle(QApplication::translate("fishInteractorWidget", "Form", 0, QApplication::UnicodeUTF8));
        volumeLabel->setText(QApplication::translate("fishInteractorWidget", "TextLabel", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class fishInteractorWidget: public Ui_fishInteractorWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FISHINTERACTORWIDGET_H
