#ifndef QMINI3DGUI_H
#define QMINI3DGUI_H

#include <QWidget>


namespace Ui {
    class qmini3dgui;
}

class qmini3dgui : public QWidget
{
    Q_OBJECT

public:
    explicit qmini3dgui(QWidget *parent = 0);
    ~qmini3dgui();
protected:

private:
    Ui::qmini3dgui *ui;


};

#endif // QMINI3DGUI_H
