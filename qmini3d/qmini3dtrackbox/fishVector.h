#ifndef FISHVECTOR_H
#define FISHVECTOR_H


class fishVector
{
public:
    fishVector();

    float x, y, z, w;

    // 计算插值：t 为 [0, 1] 之间的数值
    float interp(float x1, float x2, float t);
    // | v |
    float vectorLength();
    // z = x + y
    void vectorAdd(const fishVector *x, const fishVector *y);
    // z = x - y
    void vectorSub(const fishVector *x, const fishVector *y);
    // 矢量点乘
    float vectorDotproduct(const fishVector *x, const fishVector *y);
    // 矢量叉乘
    void vectorCrossproduct(const fishVector *x, const fishVector *y);
    // 矢量插值，t取值 [0, 1]
    void vectorInterp(const fishVector *x1, const fishVector *x2, float t);
    void vectorInterp(fishVector *u, const fishVector *x1, const fishVector *x2, float t);
    // 矢量归一化
    void vectorNormalize();

};

#endif // FISHVECTOR_H
