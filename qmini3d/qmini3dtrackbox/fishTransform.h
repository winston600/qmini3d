#ifndef FISHTRANSFORM_H
#define FISHTRANSFORM_H

#include "fishStruct.h"
#include "fishMatrix.h"
//=====================================================================
// 坐标变换
//=====================================================================
class fishTransform
{
public:
    fishTransform();
    void SetInput(fishMatrix *pmatrix);

    // 矩阵更新，计算 transform = world * view * projection
    void transform_update();
    // 初始化，设置屏幕长宽
    void transform_init(int width, int height);
    // 将矢量 x 进行 project
    void transform_apply(fishVector *y, const fishVector *x);
    // 检查齐次坐标同 cvv 的边界用于视锥裁剪
    int transform_check_cvv(const fishVector *v);
    // 归一化，得到屏幕坐标
    void transform_homogenize(fishVector *y, const fishVector *x);


public:
    matrix_t world;         // 世界坐标变换
    matrix_t view;          // 摄影机坐标变换
    matrix_t projection;    // 投影变换
    matrix_t transform;     // transform = world * view * projection
    float w, h;             // 屏幕大小

private:
    fishMatrix *m_matrix;
};

#endif // FISHTRANSFORM_H
