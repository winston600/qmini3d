/********************************************************************************
** Form generated from reading UI file 'qminiLine.ui'
**
** Created by: Qt User Interface Compiler version 4.8.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_QMINILINE_H
#define UI_QMINILINE_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_qminiLine
{
public:

    void setupUi(QWidget *qminiLine)
    {
        if (qminiLine->objectName().isEmpty())
            qminiLine->setObjectName(QString::fromUtf8("qminiLine"));
        qminiLine->resize(694, 584);

        retranslateUi(qminiLine);

        QMetaObject::connectSlotsByName(qminiLine);
    } // setupUi

    void retranslateUi(QWidget *qminiLine)
    {
        qminiLine->setWindowTitle(QApplication::translate("qminiLine", "qminiLine", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class qminiLine: public Ui_qminiLine {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_QMINILINE_H
