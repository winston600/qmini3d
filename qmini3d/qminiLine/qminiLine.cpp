#include "qminiLine.h"

qminiLine::qminiLine(QWidget *parent)
	: QWidget(parent), angle(0), scale(1.0), lastPos(QPoint()) 
{
	ui.setupUi(this);

	// 初始化三角形的顶点
	updateTriangle();
}

qminiLine::~qminiLine()
{

}

void qminiLine::paintEvent(QPaintEvent *event)  
{
	Q_UNUSED(event); // 避免未使用参数警告

	QPainter painter(this);

	//{//画一条线
	//	// 设置线条颜色和宽度
	//	painter.setPen(QPen(Qt::blue, 2));

	//	// 绘制一条从 (50, 50) 到 (200, 200) 的线
	//	painter.drawLine(50, 50, 200, 200);
	//}

	//{//画等边三角形
	//	// 设置三角形颜色
	//	painter.setBrush(Qt::cyan); // 设置填充颜色为青色
	//	painter.setPen(Qt::blue); // 设置边框颜色为蓝色

	//	// 计算等边三角形的三个顶点
	//	int x1 = 200; // 顶点1(x, y)
	//	int y1 = 50;

	//	int x2 = 120; // 顶点2(x, y)
	//	int y2 = 230;

	//	int x3 = 280; // 顶点3(x, y)
	//	int y3 = 230;

	//	// 创建一个多边形来表示三角形
	//	QPolygon triangle;
	//	triangle << QPoint(x1, y1) << QPoint(x2, y2) << QPoint(x3, y3);

	//	// 绘制三角形
	//	painter.drawPolygon(triangle);
	//}

	{//能旋转的三角形
		painter.setRenderHint(QPainter::Antialiasing); // 开启抗锯齿

		// 设置三角形颜色
		painter.setBrush(Qt::cyan);
		painter.setPen(Qt::blue);

		// 保存当前状态
		painter.save();

		// 将原点移动到窗口中心，并旋转和缩放
		painter.translate(width() / 2, height() / 2);
		painter.rotate(angle); // 旋转指定角度
		painter.scale(scale, scale); // 缩放三角形

		// 绘制三角形
		painter.drawPolygon(triangle);

		// 恢复状态
		painter.restore();
	}
}

void qminiLine::mousePressEvent(QMouseEvent *event)  
{
	if (event->button() == Qt::LeftButton) {
		lastPos = event->pos(); // 记录鼠标按下的位置
	} else if (event->button() == Qt::RightButton) {
		lastPos = event->pos(); // 记录右键按下的位置
	}
}

void qminiLine::mouseMoveEvent(QMouseEvent *event)  
{
	if (event->buttons() & Qt::LeftButton) {
		// 计算鼠标移动的距离用于旋转
		int deltaX = event->pos().x() - lastPos.x();
		angle += deltaX; // 更新旋转角度
		lastPos = event->pos(); // 更新最后位置
		update(); // 请求重绘
	} else if (event->buttons() & Qt::RightButton) {
		// 计算鼠标移动的距离用于缩放
		int deltaY = event->pos().y() - lastPos.y();
		scale += deltaY * 0.01; // 更新缩放比例（每像素0.01的变化）
		scale = qMax(scale, 0.1); // 限制最小缩放比例为0.1
		lastPos = event->pos(); // 更新最后位置
		update(); // 请求重绘
	}
}

void qminiLine::updateTriangle() 
{
	// 计算等边三角形的三个顶点
	int x1 = 0;
	int y1 = -100;

	int x2 = -87; // 120 * cos(60)
	int y2 = 50;  // 120 * sin(60)

	int x3 = 87;  // 120 * cos(-60)
	int y3 = 50;  // 120 * sin(-60)

	triangle.clear();
	triangle << QPoint(x1, y1) << QPoint(x2, y2) << QPoint(x3, y3);
}
