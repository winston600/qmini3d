#ifndef QMINILINE_H
#define QMINILINE_H

#include <QWidget>
#include "ui_qminiLine.h"
#include <QPainter>
#include <QPolygon>
#include <QMouseEvent>
#include <cmath>

class qminiLine : public QWidget
{
	Q_OBJECT

public:
	qminiLine(QWidget *parent = 0);
	~qminiLine();

private:
	Ui::qminiLine ui;

	double angle; // 旋转角度
	double scale; // 缩放比例
	QPoint lastPos; // 上一次鼠标位置
	QPolygon triangle; // 三角形多边形

	void updateTriangle();

protected:
	void paintEvent(QPaintEvent *event) override ;
	void mousePressEvent(QMouseEvent *event) override ;
	void mouseMoveEvent(QMouseEvent *event) override ;
};

#endif // QMINILINE_H
