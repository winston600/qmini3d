#ifndef FISHTXTREADER_H
#define FISHTXTREADER_H

#include <fstream>
#include <iostream>
#include <sstream>       //istringstream 必须包含这个头文件
#include <vector>
#include <string>
#include <iomanip>      //cout格式化打印输出
#include <cstdlib>

//
using namespace std;

typedef struct {
    double x;
    double y;
    double z;
}fishpoint1;

typedef struct {
    fishpoint1 point;
    double diameter;
}fishcenterpoint;


class fishTxtReader
{
public:
    fishTxtReader();

    void SetFlieName(const char* pflie);
    void Update();

    vector<fishcenterpoint>  Getoutput();
private:
    std::vector<std::string> split(const std::string& s,char delimiter);

    const char* file_temp;

    vector<fishcenterpoint> fishcenter_point;
};

#endif // FISHTXTREADER_H
