#ifndef fishTextReader_H
#define fishTextReader_H

#include <fstream>
#include <iostream>
#include <sstream>       //istringstream 必须包含这个头文件
#include <vector>
#include <string>
#include <iomanip>      //cout格式化打印输出
#include <cstdlib>

//
using namespace std;

class fishpoint
{
public:
    double x;
    double y;
    double z;
    double w;

    double u;
    double v;

    double r;
    double g;
    double b;

    double h;

};


class fishTextReader
{
public:
    fishTextReader();

    void SetFlieName(const char* pflie);
    void Update();

    vector<fishpoint>  Getoutput();
private:
    double charToDouble(const char *p);
    double intToDouble(int x);

    std::vector<std::string> split(const std::string& s,char delimiter);

    const char* file_temp;

    vector<fishpoint> fishpoint_vector;
};

#endif // fishTextReader_H
