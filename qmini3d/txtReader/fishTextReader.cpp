#include "fishTextReader.h"

fishTextReader::fishTextReader()
{
    file_temp = "";
}


double fishTextReader::charToDouble(const char *p)
{
    return atof(p);
}

double fishTextReader::intToDouble(int x)
{
    return (double)x;
}
//字符分割函数
std::vector<std::string> fishTextReader::split(const std::string& s,char delimiter)
{
    std::vector<std::string> tokens;
    std::string token;
    std::istringstream tokenStream(s);
    while (std::getline(tokenStream, token, delimiter))
    {
        tokens.push_back(token);
    }
    return tokens;
}

void fishTextReader::SetFlieName(const char* pflie)
{
    file_temp = pflie;
}

void fishTextReader::Update()
{
    //读取
    fstream readFile(file_temp);
    if (!readFile.is_open())
        cout << "open file failure" << endl;

    vector<string> fileall_raw;//创建一个vector<string>对象
    string  raw; //保存读入的每一行
    while(getline(readFile,raw))//会自动把\n换行符去掉
    {
        fileall_raw.push_back(raw);
    }
    cout << "共有行数：" << fileall_raw.size() << endl;

    vector<string> fileall_column;
    fileall_column = split(fileall_raw.at(1), ',');
    cout << "共有列数：" << fileall_column.size() << endl;

    //转换
    for (int i = 0; i < fileall_raw.size(); i++)
    {
        vector<string> fileall_column;
        fileall_column = split(fileall_raw.at(i), ',');

        const char *x = fileall_column[0].c_str();//string to char
        const char *y = fileall_column[1].c_str();
        const char *z = fileall_column[2].c_str();
        const char *w = fileall_column[3].c_str();
        const char *u = fileall_column[4].c_str();
        const char *v = fileall_column[5].c_str();
        const char *r = fileall_column[6].c_str();
        const char *g = fileall_column[7].c_str();
        const char *b = fileall_column[8].c_str();
        const char *h = fileall_column[9].c_str();

        fishpoint point_tmp;
        point_tmp.x = charToDouble(x);//char to double
        point_tmp.y = charToDouble(y);
        point_tmp.z = charToDouble(z);
        point_tmp.w = charToDouble(w);
        point_tmp.u = charToDouble(u);
        point_tmp.u = charToDouble(u);
        point_tmp.v = charToDouble(v);

        point_tmp.r = charToDouble(r);
        point_tmp.g = charToDouble(g);
        point_tmp.b = charToDouble(b);

        point_tmp.h = charToDouble(h);

        fishpoint_vector.push_back(point_tmp);
    }

    //打印
//    for (int i = 0; i < fishpoint_vector.size(); i++)
//    {
//        cout<< "(x,y,z,w):" <<setfill(' ')<<setw(10)<<fishpoint_vector.at(i).x<<"||"
//            <<setfill(' ')<<setw(10)<< fishpoint_vector.at(i).y<<"||"
//           <<setfill(' ')<<setw(10) << fishpoint_vector.at(i).z<<"||"
//          <<setfill(' ')<<setw(10) << fishpoint_vector.at(i).w<<"||"
//         <<setfill(' ')<<setw(10)<< fishpoint_vector.at(i).u << endl;
//    }//

    readFile.close();
}

vector<fishpoint> fishTextReader::Getoutput()
{
    return fishpoint_vector;
}
