#include "fishTxtReader.h"

fishTxtReader::fishTxtReader()
{
    file_temp = "";
}


//字符分割函数
std::vector<std::string> fishTxtReader::split(const std::string& s,char delimiter)
{
    std::vector<std::string> tokens;
    std::string token;
    std::istringstream tokenStream(s);
    while (std::getline(tokenStream, token, delimiter))
    {
        tokens.push_back(token);
    }
    return tokens;
}

void fishTxtReader::SetFlieName(const char* pflie)
{
    file_temp = pflie;
}

void fishTxtReader::Update()
{
    //读取
    fstream readFile(file_temp);
    if (!readFile.is_open())
        cout << "open file failure" << endl;

    vector<string> fileall_raw;//创建一个vector<string>对象
    string  raw; //保存读入的每一行
    while(getline(readFile,raw))//会自动把\n换行符去掉
    {
        fileall_raw.push_back(raw);
    }
    cout << "共有行数：" << fileall_raw.size() << endl;
    cout << "1：" << fileall_raw.at(1) << endl;


    vector<string> fileall_column;
    fileall_column = split(fileall_raw.at(1), ' ');
    cout << "共有列数：" << fileall_column.size() << endl;



    //转换
    for (int i = 0; i < fileall_raw.size(); i++)
    {
        vector<string> fileall_column;
        fileall_column = split(fileall_raw.at(i), ' ');
        const char *x = fileall_column[3].c_str();//string to char
        const char *y = fileall_column[4].c_str();
        const char *z = fileall_column[5].c_str();
        const char *dia = fileall_column[1].c_str();

        fishpoint1 point_tmp;
        point_tmp.x = atof(x);//char to double
        point_tmp.y = atof(y);
        point_tmp.z = atof(z);

        fishcenterpoint centerpoint_tmp;
        centerpoint_tmp.point = point_tmp;
        centerpoint_tmp.diameter = atof(dia);

        fishcenter_point.push_back(centerpoint_tmp);
    }

//    //打印
//    for (int i = 0; i < fishcenter_point.size(); i++)
//    {
//        cout<< "(x,y,z) d:" <<setfill(' ')<<setw(10)<<fishcenter_point.at(i).point.x<<"||"
//            <<setfill(' ')<<setw(10)<< fishcenter_point.at(i).point.y<<"||"
//           <<setfill(' ')<<setw(10) << fishcenter_point.at(i).point.z<<"||"
//          <<setfill(' ')<<setw(10)<< fishcenter_point.at(i).diameter << endl;
//    }

    readFile .close();
}

vector<fishcenterpoint> fishTxtReader::Getoutput()
{
    return fishcenter_point;
}
