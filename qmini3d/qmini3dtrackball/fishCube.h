#pragma once

#include <qDebug>
#include <QObject>

#include "fishIncludes.h"

#include "fishLight.h"
#include "fishVertex.h"
#include "fishVector4.h"


class fishCube : public QObject
{
    Q_OBJECT

public:
    static fishCube *GetfishCube();
    ~fishCube();

    vector<fishVertex> cube_mesh;//用于对外显示的网格

private:
    static fishCube *fish_cube;
    fishCube(QObject *parent = 0);
};

