#pragma once

#include <QLabel>
#include <QPainter>
#include <QLabel>
#include <QPoint>
#include <QColor>
#include <QPaintEvent>
#include <QImage>
#include <QPixmap>

#include "fishDevice.h"
#include "fishCamera.h"



//enum ShowType { DCM2D, DCM3D };
class fishWindowLabel : public QLabel
{
    Q_OBJECT
public:
    fishWindowLabel(QWidget *parent);
    ~fishWindowLabel();

    fishDevice *m_device;

protected:
    void paintEvent(QPaintEvent *event);

private:
    void paintcolorbar(bool type);//type=0 �ܾ���type=1 FFR
    //3d
    unsigned char *screen_fb;
    QImage *img;
};
