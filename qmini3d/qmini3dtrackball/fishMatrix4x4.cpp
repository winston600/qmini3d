#include "fishMatrix4x4.h"

fishMatrix4x4::fishMatrix4x4(){
    IdentityMatrix();
}
//----------------------------------------------------------------------------
fishMatrix4x4::~fishMatrix4x4()
{
}
//----------------------------------------------------------------------------
fishMatrix4x4::fishMatrix4x4(const fishMatrix4x4 &m)
{
    ele[0] = m.ele[0];
    ele[1] = m.ele[1];
    ele[2] = m.ele[2];
    ele[3] = m.ele[3];
    ele[4] = m.ele[4];
    ele[5] = m.ele[5];
    ele[6] = m.ele[6];
    ele[7] = m.ele[7];
    ele[8] = m.ele[8];
    ele[9] = m.ele[9];
    ele[10] = m.ele[10];
    ele[11] = m.ele[11];
    ele[12] = m.ele[12];
    ele[13] = m.ele[13];
    ele[14] = m.ele[14];
    ele[15] = m.ele[15];
}
//----------------------------------------------------------------------------
fishMatrix4x4::fishMatrix4x4(float e0, float e1, float e2, float e3,
                             float e4, float e5, float e6, float e7,
                             float e8, float e9, float e10, float e11,
                             float e12, float e13, float e14, float e15)
{
    ele[0] = e0;
    ele[1] = e1;
    ele[2] = e2;
    ele[3] = e3;
    ele[4] = e4;
    ele[5] = e5;
    ele[6] = e6;
    ele[7] = e7;
    ele[8] = e8;
    ele[9] = e9;
    ele[10] = e10;
    ele[11] = e11;
    ele[12] = e12;
    ele[13] = e13;
    ele[14] = e14;
    ele[15] = e15;
}
//----------------------------------------------------------------------------
void fishMatrix4x4::Transpose()
{
    float tmp;

    tmp = ele[1];
    ele[1] = ele[4];
    ele[4] = tmp;

    tmp = ele[2];
    ele[2] = ele[8];
    ele[8] = tmp;

    tmp = ele[3];
    ele[3] = ele[12];
    ele[12] = tmp;

    tmp = ele[6];
    ele[6] = ele[9];
    ele[9] = tmp;

    tmp = ele[7];
    ele[7] = ele[13];
    ele[13] = tmp;

    tmp = ele[11];
    ele[11] = ele[14];
    ele[14] = tmp;
}
//----------------------------------------------------------------------------
#define Det3x3(a, b, c, d, e, f, g, h, i) (a*e*i + b*f*g + c*d*h - c*e*g - a*f*h - b*d*i)
float fishMatrix4x4::Inverse()
{
    float a1, a2, a3, a4, b1, b2, b3, b4;
    float c1, c2, c3, c4, d1, d2, d3, d4;

    a1 = ele[0];
    b1 = ele[1];
    c1 = ele[2];
    d1 = ele[3];

    a2 = ele[4];
    b2 = ele[5];
    c2 = ele[6];
    d2 = ele[7];

    a3 = ele[8];
    b3 = ele[9];
    c3 = ele[10];
    d3 = ele[11];

    a4 = ele[12];
    b4 = ele[13];
    c4 = ele[14];
    d4 = ele[15];

    float det1 = Det3x3(b2, b3, b4, c2, c3, c4, d2, d3, d4);
    float det2 = Det3x3(a2, a3, a4, c2, c3, c4, d2, d3, d4);
    float det3 = Det3x3(a2, a3, a4, b2, b3, b4, d2, d3, d4);
    float det4 = Det3x3(a2, a3, a4, b2, b3, b4, c2, c3, c4);
    float det = a1*det1 - b1*det2 + c1*det3 - d1*det4;
    if (det == 0.0f)    return 0.0f;

    float invdet = 1.0f / det;

    ele[0] = det1*invdet;
    ele[4] = -det2*invdet;
    ele[8] = det3*invdet;
    ele[12] = -det4*invdet;

    ele[1] = -Det3x3(b1, b3, b4, c1, c3, c4, d1, d3, d4)*invdet;
    ele[5] = Det3x3(a1, a3, a4, c1, c3, c4, d1, d3, d4)*invdet;
    ele[9] = -Det3x3(a1, a3, a4, b1, b3, b4, d1, d3, d4)*invdet;
    ele[13] = Det3x3(a1, a3, a4, b1, b3, b4, c1, c3, c4)*invdet;

    ele[2] = Det3x3(b1, b2, b4, c1, c2, c4, d1, d2, d4)*invdet;
    ele[6] = -Det3x3(a1, a2, a4, c1, c2, c4, d1, d2, d4)*invdet;
    ele[10] = Det3x3(a1, a2, a4, b1, b2, b4, d1, d2, d4)*invdet;
    ele[14] = -Det3x3(a1, a2, a4, b1, b2, b4, c1, c2, c4)*invdet;

    ele[3] = -Det3x3(b1, b2, b3, c1, c2, c3, d1, d2, d3)*invdet;
    ele[7] = Det3x3(a1, a2, a3, c1, c2, c3, d1, d2, d3)*invdet;
    ele[11] = -Det3x3(a1, a2, a3, b1, b2, b3, d1, d2, d3)*invdet;
    ele[15] = Det3x3(a1, a2, a3, b1, b2, b3, c1, c2, c3)*invdet;

    return det;
}
//平移------------------------------------------------------------------------
void fishMatrix4x4::TranslateMatrix(float tx, float ty, float tz)
{
    this->IdentityMatrix();
    ele[12] = tx; ele[13] = ty; ele[14] = tz;
}
//缩放------------------------------------------------------------------------
void fishMatrix4x4::ScaleMatrix(float sx, float sy, float sz)
{
    this->IdentityMatrix();
    ele[0] = sx; ele[5] = sy; ele[10] = sz;
}
//旋转------------------------------------------------------------------------
void fishMatrix4x4::RotateMatrix(float theta, float x, float y, float z)
{
    this->IdentityMatrix();
    float qsin = (float)sin(theta * 0.5f);
    float qcos = (float)cos(theta * 0.5f);
    fishVector4 vec;
    vec.set( x, y, z);
    float w = qcos;
    vec.normalize();
    x = vec.x * qsin;
    y = vec.y * qsin;
    z = vec.z * qsin;
    ele[0] = 1 - 2 * y * y - 2 * z * z;
    ele[4] = 2 * x * y - 2 * w * z;
    ele[8] = 2 * x * z + 2 * w * y;
    ele[1] = 2 * x * y + 2 * w * z;
    ele[5] = 1 - 2 * x * x - 2 * z * z;
    ele[9] = 2 * y * z - 2 * w * x;
    ele[2] = 2 * x * z - 2 * w * y;
    ele[6] = 2 * y * z + 2 * w * x;
    ele[10] = 1 - 2 * x * x - 2 * y * y;
    ele[3] = ele[7] = ele[11] = 0.0f;
    ele[12] = ele[13] = ele[14] = 0.0f;
    ele[15] = 1.0f;
}
//投影矩阵--------------------------------------------------------------------
void fishMatrix4x4::PerspectiveMatrix(float fovy, float aspect, float zn, float zf)
{
    this->ZeroMatrix();
    float fax = 1.0f / (float)tan(fovy * 0.5f);
    ele[0] = (float)(fax / aspect);
    ele[5] = (float)(fax);
    ele[10] = zf / (zf - zn);
    ele[14] = -zn * zf / (zf - zn);
    ele[11] = 1;
}
