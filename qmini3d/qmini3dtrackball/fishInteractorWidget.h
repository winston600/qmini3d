#pragma once


#include <QWidget>
#include "ui_fishInteractorWidget.h"

#include <QMouseEvent>
#include <QPainter>


class fishInteractorWidget : public QWidget
{
    Q_OBJECT

public:
    fishInteractorWidget(QWidget *parent = NULL);
    ~fishInteractorWidget();

protected:
    void keyPressEvent(QKeyEvent *event);
    void mousePressEvent(QMouseEvent *event);        //����
    void mouseMoveEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);

private:
    Ui::fishInteractorWidget ui;
    float pos;
    QPoint lastPos;
    int indicator;
    int states[2];
    bool moveflag;
    void RotateY(float angle);
    void RotateZ(float angle);
};
