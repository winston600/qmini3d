#pragma once

#include "fishMath.h"

class fishVector4
{
public:
    float x, y, z, w;

    //构造函数
    fishVector4():x(0), y(0), z(0), w(1) {};
    fishVector4(const fishVector4 &v):x(v.x),y(v.y),z(v.z),w(v.w){}
    fishVector4(float x_para, float y_para, float z_para, float w_para = 1):x(x_para), y(y_para), z(z_para), w(w_para) {}
    ~fishVector4();

    void zero();//置为零向量
    void normalize();//归一化
    void set(float xaxis, float yaxis, float zaxis, float w_para = 1);//设置数值

    fishVector4& operator = (const fishVector4& A);//重载“=”
    fishVector4 operator-() const;//重载一元“-”
    fishVector4 operator+(const fishVector4 &a) const;//重载“+”
    fishVector4 operator-(const fishVector4 &a)const;//重载“-”
    float operator *(const fishVector4 &a) const;//重载“*”//点乘
    bool operator ==(const fishVector4 &a)const;//重载“==”
    bool operator !=(const fishVector4 &a) const;//重载“!=”

    fishVector4 &operator +=(const fishVector4 &a);//重载自反运算符
    fishVector4 &operator -=(const fishVector4 &a);
    fishVector4 &operator *=(float a);
    fishVector4 &operator /=(float a);

    fishVector4 operator*(float a) const;//与标量的乘法
    fishVector4 operator/(float a) const;//与标量的除法

    //求向量模---------------------------------------------------------------------------
    float vectorMag(const fishVector4 &a);
    //计算两个向量叉乘----------------------------------------------------------------------------
    fishVector4 crossProduct(const fishVector4 &a, const fishVector4 &b);
    //实现标量左乘----------------------------------------------------------------------------
    friend fishVector4 operator *(float k, const fishVector4 &v);
    //两个向量间距离----------------------------------------------------------------------------
    float distance(const fishVector4 &a, const fishVector4 &b);
    //矢量插值，t取值 [0, 1]----------------------------------------------------------------------------
    void vectorInterp(fishVector4 &z, const fishVector4 &x1, const fishVector4 &x2, float t);

};

typedef fishVector4 fishPoint;


///////////////////////////////////////////////////////
//
//非成员函数
//
///////////////////////////////////////////////////////
//求向量模---------------------------------------------------------------------------
inline float vectorMag(const fishVector4 &a) {
        return sqrt(a.x*a.x + a.y*a.y + a.z*a.z);
}
//计算两个向量叉乘----------------------------------------------------------------------------
inline fishVector4 crossProduct(const fishVector4 &a, const fishVector4 &b) {
        return fishVector4(
                a.y*b.z - a.z*b.y,
                a.z*b.x - a.x*b.z,
                a.x*b.y - a.y*b.x
        );
}
//实现标量左乘----------------------------------------------------------------------------
inline fishVector4 operator *(float k, const fishVector4 &v) {
        return fishVector4(k*v.x, k*v.y, k*v.z);
}
//两个向量间距离----------------------------------------------------------------------------
inline float distance(const fishVector4 &a, const fishVector4 &b) {
        float dx = a.x - b.x;
        float dy = a.y - b.y;
        float dz = a.z - b.z;
        return sqrt(dx*dx + dy*dy + dz*dz);
}
//矢量插值，t取值 [0, 1]----------------------------------------------------------------------------
inline void vectorInterp(fishVector4 &z, const fishVector4 &x1, const fishVector4 &x2, float t) {

        z.x = fishMath::interp(x1.x, x2.x, t);
        z.y = fishMath::interp(x1.y, x2.y, t);
        z.z = fishMath::interp(x1.z, x2.z, t);
        z.w = 1.0f;


}













