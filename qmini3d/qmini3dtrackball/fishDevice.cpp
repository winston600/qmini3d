#include "fishDevice.h"
#include "fishIncludes.h"



#include "fishCube.h"

fishDevice::fishDevice()
{
    camera = new fishCamera;
    light = new fishLight;
    mixflag = 0;
}
//----------------------------------------------------------------------------
fishDevice::~fishDevice()
{
}
//画点----------------------------------------------------------------------------
void fishDevice::devicePixel(int x, int y, IUINT32 color)
{
    if (((IUINT32)x) < (IUINT32)this->width && ((IUINT32)y) < (IUINT32)this->height)
    {
        this->framebuffer[y][x] = color;
    }
}
//绘制线段----------------------------------------------------------------------------
void fishDevice::deviceDrawLine(int x1, int y1, int x2, int y2, IUINT32 c)
{
    int x, y, rem = 0;
    if (x1 == x2 && y1 == y2)
    {
        devicePixel(x1, y1, c);
    }
    else if (x1 == x2)
    {
        int inc = (y1 <= y2) ? 1 : -1;
        for (y = y1; y != y2; y += inc) devicePixel(x1, y, c);
        devicePixel(x2, y2, c);
    }
    else if (y1 == y2)
    {
        int inc = (x1 <= x2) ? 1 : -1;
        for (x = x1; x != x2; x += inc) devicePixel( x, y1, c);
        devicePixel(x2, y2, c);
    }
    else
    {
        int dx = (x1 < x2) ? x2 - x1 : x1 - x2;
        int dy = (y1 < y2) ? y2 - y1 : y1 - y2;
        if (dx >= dy)
        {
            if (x2 < x1) x = x1, y = y1, x1 = x2, y1 = y2, x2 = x, y2 = y;
            for (x = x1, y = y1; x <= x2; x++)
            {
                devicePixel(x, y, c);
                rem += dy;
                if (rem >= dx)
                {
                    rem -= dx;
                    y += (y2 >= y1) ? 1 : -1;
                    devicePixel(x, y, c);
                }
            }
            devicePixel(x2, y2, c);
        }
        else
        {
            if (y2 < y1) x = x1, y = y1, x1 = x2, y1 = y2, x2 = x, y2 = y;
            for (x = x1, y = y1; y <= y2; y++)
            {
                devicePixel(x, y, c);
                rem += dx;
                if (rem >= dy)
                {
                    rem -= dy;
                    x += (x2 >= x1) ? 1 : -1;
                    devicePixel( x, y, c);
                }
            }
            devicePixel(x2, y2, c);
        }
    }
}
//根据坐标读取纹理----------------------------------------------------------------------------
IUINT32 fishDevice::deviceTextureRead(float u, float v) const
{
    int x, y;
    u = u * this->max_u;
    v = v * this->max_v;
    x = (int)(u + 0.5f);
    y = (int)(v + 0.5f);
    x = fishMath::CMID(x, 0, this->tex_width - 1);
    y = fishMath::CMID(y, 0, this->tex_height - 1);
    return this->texture[y][x];
}
//按照 Y 坐标计算出左右两条边纵坐标等于 Y 的顶点----------------------------------------------------------------------------
void fishDevice::trapezoidEdgeInterp(trapezoid_t *trap, float y)
{
    float s1 = trap->left.v2.pos.y - trap->left.v1.pos.y;
    float s2 = trap->right.v2.pos.y - trap->right.v1.pos.y;
    float t1 = (y - trap->left.v1.pos.y) / s1;
    float t2 = (y - trap->right.v1.pos.y) / s2;
    vertexInterp(trap->left.v, trap->left.v1, trap->left.v2, t1);
    vertexInterp(trap->right.v, trap->right.v1, trap->right.v2, t2);
}
//根据左右两边的端点，初始化计算出扫描线的起点和步长----------------------------------------------------------------------------
void fishDevice::trapezoidInitScanLine(const trapezoid_t *trap, scanline_t *scanline, int y)
{
    float width = trap->right.v.pos.x - trap->left.v.pos.x;
    scanline->x = (int)(trap->left.v.pos.x + 0.5f);
    scanline->w = (int)(trap->right.v.pos.x + 0.5f) - scanline->x;
    scanline->y = y;
    scanline->v = trap->left.v;
    if (trap->left.v.pos.x >= trap->right.v.pos.x) scanline->w = 0;
    vertexDivision(scanline->step, trap->left.v, trap->right.v, width);
}
//绘制扫描线----------------------------------------------------------------------------
void fishDevice::deviceDrawScanline(scanline_t *scanline)
{
    if (mixflag == 0)
    {
        IUINT32 *framebuffer = this->framebuffer[scanline->y];
        float *zbuffer = this->zbuffer[scanline->y];
        int x = scanline->x;
        int w = scanline->w;
        int width = this->width;
        int render_state = this->render_state;
        for (; w > 0; x++, w--)
        {
            if (x >= 0 && x < width)
            {
                float rhw = scanline->v.rhw;
                //如果混合关闭 则直接判断深度 并写入帧缓存
                if (rhw >= zbuffer[x])
                {
                    float w = 1.0f / rhw;
                    zbuffer[x] = rhw;
                    if (render_state & RENDER_STATE_COLOR)
                    {
                        float r = scanline->v.color.r * w;
                        float g = scanline->v.color.g * w;
                        float b = scanline->v.color.b * w;
                        int R = (int)(r * 255.0f);
                        int G = (int)(g * 255.0f);
                        int B = (int)(b * 255.0f);
                        R = fishMath::CMID(R, 0, 255);
                        G = fishMath::CMID(G, 0, 255);
                        B = fishMath::CMID(B, 0, 255);
                        framebuffer[x] = (R << 16) | (G << 8) | (B);
                    }
                    if (render_state & RENDER_STATE_TEXTURE)
                    {
                        float u = scanline->v.tc.u * w;
                        float v = scanline->v.tc.v * w;
                        IUINT32 cc = deviceTextureRead(u, v);
                        framebuffer[x] = cc;
                    }
                }
            }
            scanline->v = scanline->v + scanline->step;//20190717?
            if (x >= width) break;
        }
    }
    else
    {
        deviceDrawTransparentScanline(scanline);
    }
}
//主渲染函数----------------------------------------------------------------------------
void fishDevice::deviceRenderTrap(trapezoid_t *trap)
{
    scanline_t scanline;
    int j, top, bottom;
    top = (int)(trap->top + 0.5f);
    bottom = (int)(trap->bottom + 0.5f);
    for (j = top; j < bottom; j++)
    {
        if (j >= 0 && j < this->height)
        {
            trapezoidEdgeInterp(trap, (float)j + 0.5f);
            trapezoidInitScanLine(trap, &scanline, j);

            deviceDrawScanline(&scanline);

        }
        if (j >= this->height) break;
    }
}
//绘制面----------------------------------------------------------------------------

void fishDevice::drawPlane(int a, int b, int c, int d)
{
    fishVertex p1 = fishCube::GetfishCube()->cube_mesh.at(a);
    fishVertex p2 = fishCube::GetfishCube()->cube_mesh.at(b);
    fishVertex p3 = fishCube::GetfishCube()->cube_mesh.at(c);
    fishVertex p4 = fishCube::GetfishCube()->cube_mesh.at(d);

    p1.tc.u = 0, p1.tc.v = 0, p2.tc.u = 0, p2.tc.v = 1;
    p3.tc.u = 1, p3.tc.v = 1, p4.tc.u = 1, p4.tc.v = 0;

    this->deviceDrawPrimitive( &p1, &p2, &p3);
    this->deviceDrawPrimitive( &p3, &p4, &p1);
}
//绘制高亮框----------------------------------------------------------------------------
void fishDevice::drawHighlightplane(int a, int b, int c, int d,int flag)
{
    //    fishVertex p1 = fishCube::GetfishCube()->meshrect.at(a),
    //            p2 = fishCube::GetfishCube()->meshrect.at(b),
    //            p3 = fishCube::GetfishCube()->meshrect.at(c),
    //            p4 = fishCube::GetfishCube()->meshrect.at(d);
    //    p1.tc.u = 0, p1.tc.v = 0, p2.tc.u = 0, p2.tc.v = 1;
    //    p3.tc.u = 1, p3.tc.v = 1, p4.tc.u = 1, p4.tc.v = 0;
    //    deviceDrawPrimitive(&p1, &p2, &p3,flag);
    //    deviceDrawPrimitive(&p3, &p4, &p1,flag);
}
//根据 render_state 绘制原始三角形----------------------------------------------------------------------------
void fishDevice::deviceDrawPrimitive(fishVertex *v1, fishVertex *v2, fishVertex *v3)
{
    fishPoint p1, p2, p3, c1, c2, c3;
    int render_state = this->render_state;

    // 按照 Transform 变化
    c1 = v1->pos*(this->transform.transform);
    c2 = v2->pos*(this->transform.transform);
    c3 = v3->pos*(this->transform.transform);

    // 裁剪，注意此处可以完善为具体判断几个点在 cvv内以及同cvv相交平面的坐标比例
    // 进行进一步精细裁剪，将一个分解为几个完全处在 cvv内的三角形
    if (transformCheckCvv(c1) != 0) return;
    if (transformCheckCvv(c2) != 0) return;
    if (transformCheckCvv(c3) != 0) return;//20190724

    // 归一化
    this->transform.transformHomogenize(p1, c1);
    this->transform.transformHomogenize(p2, c2);
    this->transform.transformHomogenize(p3, c3);

    // 纹理或者色彩绘制
    if (render_state & (RENDER_STATE_TEXTURE | RENDER_STATE_COLOR))
    {
        fishVertex t1 = *v1, t2 = *v2, t3 = *v3;
        trapezoid_t traps[2];
        int n;
        t1.pos = p1;
        t2.pos = p2;
        t3.pos = p3;
        t1.pos.w = c1.w;
        t2.pos.w = c2.w;
        t3.pos.w = c3.w;
        t1.vertexRhwInit();	// 初始化 w
        t2.vertexRhwInit();	// 初始化 w
        t3.vertexRhwInit();	// 初始化 w

        // 拆分三角形为0-2个梯形，并且返回可用梯形数量
        n = trapezoidInitTriangle(traps, &t1, &t2, &t3);
        if (n >= 1) deviceRenderTrap( &traps[0]);
        if (n >= 2) deviceRenderTrap(&traps[1]);
    }

    if (render_state & RENDER_STATE_WIREFRAME)
    {		// 线框绘制
        deviceDrawLine((int)p1.x, (int)p1.y, (int)p2.x, (int)p2.y, this->modellines);
        deviceDrawLine((int)p1.x, (int)p1.y, (int)p3.x, (int)p3.y, this->modellines);
        deviceDrawLine((int)p3.x, (int)p3.y, (int)p2.x, (int)p2.y, this->modellines);
    }
}
//根据三角形生成 0-2 个梯形，并且返回合法梯形的数量----------------------------------------------------------------------------
int fishDevice::trapezoidInitTriangle(trapezoid_t *trap, fishVertex *p1,
                                      fishVertex *p2, fishVertex *p3)
{
    fishVertex *p;
    float k, x;
    if (p1->pos.y > p2->pos.y) p = p1, p1 = p2, p2 = p;
    if (p1->pos.y > p3->pos.y) p = p1, p1 = p3, p3 = p;
    if (p2->pos.y > p3->pos.y) p = p2, p2 = p3, p3 = p;
    if (p1->pos.y == p2->pos.y && p1->pos.y == p3->pos.y) return 0;
    if (p1->pos.x == p2->pos.x && p1->pos.x == p3->pos.x) return 0;
    if (p1->pos.y == p2->pos.y)
    {	// triangle down
        if (p1->pos.x > p2->pos.x) p = p1, p1 = p2, p2 = p;
        trap[0].top = p1->pos.y;
        trap[0].bottom = p3->pos.y;
        trap[0].left.v1 = *p1;
        trap[0].left.v2 = *p3;
        trap[0].right.v1 = *p2;
        trap[0].right.v2 = *p3;
        return (trap[0].top < trap[0].bottom) ? 1 : 0;
    }
    if (p2->pos.y == p3->pos.y)
    {	// triangle up
        if (p2->pos.x > p3->pos.x) p = p2, p2 = p3, p3 = p;
        trap[0].top = p1->pos.y;
        trap[0].bottom = p3->pos.y;
        trap[0].left.v1 = *p1;
        trap[0].left.v2 = *p2;
        trap[0].right.v1 = *p1;
        trap[0].right.v2 = *p3;
        return (trap[0].top < trap[0].bottom) ? 1 : 0;
    }

    trap[0].top = p1->pos.y;
    trap[0].bottom = p2->pos.y;
    trap[1].top = p2->pos.y;
    trap[1].bottom = p3->pos.y;

    k = (p3->pos.y - p1->pos.y) / (p2->pos.y - p1->pos.y);
    x = p1->pos.x + (p2->pos.x - p1->pos.x) * k;

    if (x <= p3->pos.x)
    {		// triangle left
        trap[0].left.v1 = *p1;
        trap[0].left.v2 = *p2;
        trap[0].right.v1 = *p1;
        trap[0].right.v2 = *p3;
        trap[1].left.v1 = *p2;
        trap[1].left.v2 = *p3;
        trap[1].right.v1 = *p1;
        trap[1].right.v2 = *p3;
    }
    else
    {					// triangle right
        trap[0].left.v1 = *p1;
        trap[0].left.v2 = *p3;
        trap[0].right.v1 = *p1;
        trap[0].right.v2 = *p2;
        trap[1].left.v1 = *p1;
        trap[1].left.v2 = *p3;
        trap[1].right.v1 = *p2;
        trap[1].right.v2 = *p3;
    }

    return 2;
}
//绘制网格----------------------------------------------------------------------------
void fishDevice::drawCube()
{
    fishMatrix4x4 world_matrix;
    world_matrix.RotateMatrix(1, -1, -0.5, 1);
    this->transform.world = world_matrix;
    this->transform.transformUpdate();

    drawPlane(0, 1, 2, 3);
    drawPlane(4, 5, 6, 7);
    drawPlane(0, 4, 5, 1);
    drawPlane(1, 5, 6, 2);
    drawPlane(2, 6, 7, 3);
    drawPlane(3, 7, 4, 0);
}

//设置摄像机 计算视点矩阵----------------------------------------------------------------------------
void fishDevice::calViewmatrix(fishMatrix4x4 &m, const fishCamera *camera_para)
{
    m.ele[0] = camera_para->uaxis.x;
    m.ele[4] = camera_para->uaxis.y;
    m.ele[8] = camera_para->uaxis.z;
    m.ele[12] = -(camera_para->uaxis*camera_para->eye);

    m.ele[1] = camera_para->vaxis.x;
    m.ele[5] = camera_para->vaxis.y;
    m.ele[9] = camera_para->vaxis.z;
    m.ele[13] = -(camera_para->vaxis*camera_para->eye);

    m.ele[2] = camera_para->naxis.x;
    m.ele[6] = camera_para->naxis.y;
    m.ele[10] = camera_para->naxis.z;
    m.ele[14] = -(camera_para->naxis*camera_para->eye);

    m.ele[3] = m.ele[7] = m.ele[11] = 0.0f;
    m.ele[15] = 1.0f;
}
//设备初始化，fb为外部帧缓存，非 NULL 将引用外部帧缓存（每行 4字节对齐）----------------------------------------------------------------------------
void fishDevice::deviceInit(int width, int height, void *fb,int renderstate)
{
    int need = sizeof(void*) * (height * 2 + 1024) + width * height * 8;
    char *ptr = (char*)malloc(need + 64);
    char *framebuf, *zbuf;
    int j;
    assert(ptr);//判断指针是否为空
    this->framebuffer = (IUINT32**)ptr;
    this->zbuffer = (float**)(ptr + sizeof(void*) * height);
    ptr += sizeof(void*) * height * 2;
    this->texture = (IUINT32**)ptr;
    ptr += sizeof(void*) * 1024;
    framebuf = (char*)ptr;
    zbuf = (char*)ptr + width * height * 4;
    ptr += width * height * 8;
    if (fb != NULL) framebuf = (char*)fb;
    for (j = 0; j < height; j++)
    {
        this->framebuffer[j] = (IUINT32*)(framebuf + width * 4 * j);
        this->zbuffer[j] = (float*)(zbuf + width * 4 * j);
    }
    this->texture[0] = (IUINT32*)ptr;
    this->texture[1] = (IUINT32*)(ptr + 16);
    memset(this->texture[0], 0, 64);
    this->tex_width = 2;
    this->tex_height = 2;
    this->max_u = 1.0f;
    this->max_v = 1.0f;
    this->width = width;
    this->height = height;
    this->background = 0xc0c0c0;
    this->foreground = 0xFF0000;
    this->modellines = 0x008080;
    this->transform.transformInit(width, height);
    this->render_state = renderstate;

    initTexture();
    cameraAtZero();
    initzbuffer();
}
//删除设备----------------------------------------------------------------------------
void fishDevice::deviceDestroy()
{
    if (this->framebuffer)
        free(this->framebuffer);
    this->framebuffer = NULL;
    this->zbuffer = NULL;
    this->texture = NULL;
}
//设置当前纹理----------------------------------------------------------------------------
void fishDevice::deviceSetTexture(void *bits, long pitch, int w, int h)
{
    char *ptr = (char*)bits;
    int j;
    assert(w <= 1024 && h <= 1024);
    for (j = 0; j < h; ptr += pitch, j++) 	// 重新计算每行纹理的指针
        this->texture[j] = (IUINT32*)ptr;
    this->tex_width = w;
    this->tex_height = h;
    this->max_u = (float)(w - 1);
    this->max_v = (float)(h - 1);
}
//清空 framebuffer 和 zbuffer 画背景----------------------------------------------------------------------------
void fishDevice::deviceClear(int mode)
{
    int y, x;
    for (y = 0; y < height; y++)
    {
        IUINT32 *dst = framebuffer[y];
        for (x = width; x > 0; dst++, x--) dst[0] = 0.0f;
    }
    for (y = 0; y < height; y++)
    {
        float *dst = zbuffer[y];
        for (x = width; x > 0; dst++, x--) dst[0] = -500.0f;
    }
    initzbuffer();
}
//相机初始化----------------------------------------------------------------------------
void fishDevice::cameraAtZero()
{
    calViewmatrix(this->transform.view, camera);
    this->transform.transformUpdate();
}
//刷新显示----------------------------------------------------------------------------
void fishDevice::refreshDisplay()
{
    light->SetLightpos(camera->eye);//让灯光跟着相机的位置变动
    light->calLight();
    calViewmatrix(this->transform.view, camera);
    this->transform.transformUpdate();
    drawCube();
}
//----------------------------------------------------------------------------
void fishDevice::initTexture()
{
    static IUINT32 texture[256][256];
    int i, j;
    for (j = 0; j < 256; j++)
    {
        for (i = 0; i < 256; i++)
        {
            int x = i / 32, y = j / 32;
            texture[j][i] = ((x + y) & 1) ? 0xffffff : 0x3fbcef;
        }
    }
    deviceSetTexture( texture, 256 * 4, 256, 256);
}
//检查齐次坐标同 cvv 的边界用于视锥裁剪----------------------------------------------------------------------------
int fishDevice::transformCheckCvv(const fishVector4 &v)
{
    float w = v.w;
    int check = 0;
    if (v.z < 0.0f) check |= 1;
    if (v.z >  w) check |= 2;
    if (v.x < -w) check |= 4;
    if (v.x >  w) check |= 8;
    if (v.y < -w) check |= 16;
    if (v.y >  w) check |= 32;
    return check;
}
//----------------------------------------------------------------------------
void fishDevice::deviceDrawTransparentScanline(scanline_t *scanline)
{
    IUINT32 *framebuffer = this->framebuffer[scanline->y];
    float *zbuffer = this->zbuffer[scanline->y];
    int x = scanline->x;
    int w = scanline->w;
    int width = this->width;
    int render_state = this->render_state;
    //如果混合打开
    //1、pass1:写入zbufferA
    for (; w > 0; x++, w--)
    {
        if (x >= 0 && x < width)
        {
            float rhw = scanline->v.rhw;
            //如果深度缓存标志关 依旧判断zbuffer 大于zbuffer 则画透明图 更新zbuffer
            if (rhw >= zbuffer[x] )
            {
                float w = 1.0f / rhw;
                zbuffer[x] = rhw;
                if (render_state & RENDER_STATE_COLOR)
                {
                    /*	if (scanline->v.color.a==1)
     {
     float r = scanline->v.color.r * w;
     float g = scanline->v.color.g * w;
     float b = scanline->v.color.b * w;
     int R = (int)(r * 255.0f);
     int G = (int)(g * 255.0f);
     int B = (int)(b * 255.0f);
     R = fishMath::CMID(R, 0, 255);
     G = fishMath::CMID(G, 0, 255);
     B = fishMath::CMID(B, 0, 255);
     framebuffer[x] = (R << 16) | (G << 8) | (B);
     }
     else
     {*/
                    IUINT32 colorx = framebuffer[x];
                    int b2 = colorx & 255;
                    int g2 = (colorx >> 8) & 255;
                    int r2 = (colorx >> 16) & 255;
                    float r = scanline->v.color.r * w;
                    float g = scanline->v.color.g * w;
                    float b = scanline->v.color.b * w;
                    float a = scanline->v.color.a;
                    int R = (int)(r * 255.0f);
                    int G = (int)(g * 255.0f);
                    int B = (int)(b * 255.0f);
                    R = R*a + r2*(1 - a);
                    G = G*a + g2*(1 - a);
                    B = B*a + b2*(1 - a);
                    R = fishMath::CMID(R, 0, 255);
                    G = fishMath::CMID(G, 0, 255);
                    B = fishMath::CMID(B, 0, 255);
                    framebuffer[x] = (R << 16) | (G << 8) | (B);
                    /*}*/

                }
                if (render_state & RENDER_STATE_TEXTURE)
                {
                    float u = scanline->v.tc.u * w;
                    float v = scanline->v.tc.v * w;
                    IUINT32 cc = deviceTextureRead(u, v);
                    framebuffer[x] = cc;
                }
            }
        }
        scanline->v = scanline->v + scanline->step;//20190717?
        if (x >= width) break;
    }

}
//----------------------------------------------------------------------------
void fishDevice::deviceDrawScanlineZbuffera(scanline_t *scanline)
{
    //写入第一层深度缓存  ：深度缓存A
    float *zbuffer = this->zbuffer[scanline->y];
    int x = scanline->x;
    //int y= scanline->y;
    int w = scanline->w;
    int width = this->width;
    for (; w > 0; x++, w--) {
        if (x >= 0 && x < width) {
            float rhw = scanline->v.rhw;
            if (rhw >= zbuffer[x] )
            {
                zbuffer[x] = rhw;
            }
        }
        scanline->v = scanline->v + scanline->step;//20190717?
        if (x >= width) break;
    }
}
//----------------------------------------------------------------------------
void fishDevice::deviceDrawScanlineZbufferb(scanline_t *scanline)
{
    //写入第二层深度缓存  ：深度缓存B
    float *zbufferb = this->zbufferb[scanline->y];
    float *zbuffer = this->zbuffer[scanline->y];
    int x = scanline->x;
    int w = scanline->w;
    int width = this->width;
    for (; w > 0; x++, w--) {
        if (x >= 0 && x < width) {
            float rhw = scanline->v.rhw;
            if (rhw >= zbufferb[x] && rhw < zbuffer[x])
            {
                float w = 1.0f / rhw;
                zbufferb[x] = rhw;
            }
        }
        scanline->v = scanline->v + scanline->step;//20190717?
        if (x >= width) break;
    }
}
//----------------------------------------------------------------------------
void fishDevice::deviceDrawScanlineZbufferc(scanline_t *scanline)
{
    //写入第三层深度缓存  ：深度缓存C
    float *zbufferb = this->zbufferb[scanline->y];
    float *zbufferc = this->zbufferc[scanline->y];
    int x = scanline->x;
    int w = scanline->w;
    int width = this->width;
    for (; w > 0; x++, w--) {
        if (x >= 0 && x < width) {
            float rhw = scanline->v.rhw;
            if (rhw >= zbufferc[x] && rhw < zbufferb[x])
            {
                float w = 1.0f / rhw;
                zbufferc[x] = rhw;
            }
        }
        scanline->v = scanline->v + scanline->step;//20190717?
        if (x >= width) break;
    }
}
//----------------------------------------------------------------------------
void fishDevice::deviceDrawScanlineZbufferd(scanline_t *scanline)
{
    //写入第二层深度缓存  ：深度缓存D
    float *zbufferd = this->zbufferd[scanline->y];
    float *zbufferc = this->zbufferc[scanline->y];
    int x = scanline->x;
    int w = scanline->w;
    int width = this->width;
    for (; w > 0; x++, w--) {
        if (x >= 0 && x < width) {
            float rhw = scanline->v.rhw;
            if (rhw >= zbufferd[x] && rhw < zbufferc[x])
            {
                float w = 1.0f / rhw;
                zbufferd[x] = rhw;
            }
        }
        scanline->v = scanline->v + scanline->step;//20190717?
        if (x >= width) break;
    }
}
//----------------------------------------------------------------------------
void fishDevice::deviceDrawScanlineViazbufferd(scanline_t *scanline)
{
    //通过深度缓存D画图
    IUINT32 *framebuffer = this->framebuffer[scanline->y];
    float *zbufferd = this->zbufferd[scanline->y];
    int x = scanline->x;
    int w = scanline->w;
    int width = this->width;

    for (; w > 0; x++, w--) {
        if (x >= 0 && x < width) {
            float rhw = scanline->v.rhw;
            //等于zbufferb 则画图
            if (abs(rhw - zbufferd[x])<0.000001f)
            {
                float w = 1.0f / rhw;
                IUINT32 colorx = framebuffer[x];
                int b2 = colorx & 255;
                int g2 = (colorx >> 8) & 255;
                int r2 = (colorx >> 16) & 255;
                float r = scanline->v.color.r * w;
                float g = scanline->v.color.g * w;
                float b = scanline->v.color.b * w;
                float a = scanline->v.color.a;
                int R = (int)(r * 255.0f);
                int G = (int)(g * 255.0f);
                int B = (int)(b * 255.0f);
                R = R*a + r2*(1 - a);
                G = G*a + g2*(1 - a);
                B = B*a + b2*(1 - a);
                R = fishMath::CMID(R, 0, 255);
                G = fishMath::CMID(G, 0, 255);
                B = fishMath::CMID(B, 0, 255);
                framebuffer[x] = (R << 16) | (G << 8) | (B);
            }
        }
        scanline->v = scanline->v + scanline->step;
        if (x >= width) break;
    }
}
//----------------------------------------------------------------------------
void fishDevice::deviceDrawScanlineViazbufferc(scanline_t *scanline)
{
    //通过深度缓存B画图
    IUINT32 *framebuffer = this->framebuffer[scanline->y];
    float *zbufferc = this->zbufferc[scanline->y];
    int x = scanline->x;
    int w = scanline->w;
    int width = this->width;

    for (; w > 0; x++, w--) {
        if (x >= 0 && x < width) {
            float rhw = scanline->v.rhw;
            //等于zbufferb 则画图
            if (abs(rhw - zbufferc[x])<0.000001f)
            {
                float w = 1.0f / rhw;
                IUINT32 colorx = framebuffer[x];
                int b2 = colorx & 255;
                int g2 = (colorx >> 8) & 255;
                int r2 = (colorx >> 16) & 255;
                float r = scanline->v.color.r * w;
                float g = scanline->v.color.g * w;
                float b = scanline->v.color.b * w;
                float a = scanline->v.color.a;
                int R = (int)(r * 255.0f);
                int G = (int)(g * 255.0f);
                int B = (int)(b * 255.0f);
                R = R*a + r2*(1 - a);
                G = G*a + g2*(1 - a);
                B = B*a + b2*(1 - a);
                R = fishMath::CMID(R, 0, 255);
                G = fishMath::CMID(G, 0, 255);
                B = fishMath::CMID(B, 0, 255);
                framebuffer[x] = (R << 16) | (G << 8) | (B);
            }
        }
        scanline->v = scanline->v + scanline->step;
        if (x >= width) break;
    }
}
//----------------------------------------------------------------------------
void fishDevice::deviceDrawScanlineViazbufferb(scanline_t *scanline)
{
    //通过深度缓存B画图
    IUINT32 *framebuffer = this->framebuffer[scanline->y];
    float *zbufferb = this->zbufferb[scanline->y];
    int x = scanline->x;
    int w = scanline->w;
    int width = this->width;

    for (; w > 0; x++, w--) {
        if (x >= 0 && x < width) {
            float rhw = scanline->v.rhw;
            //等于zbufferb 则画图
            if (abs(rhw -zbufferb[x])<0.000001f)
            {
                float w = 1.0f / rhw;
                IUINT32 colorx = framebuffer[x];
                int b2 = colorx & 255;
                int g2 = (colorx >> 8) & 255;
                int r2 = (colorx >> 16) & 255;
                float r = scanline->v.color.r * w;
                float g = scanline->v.color.g * w;
                float b = scanline->v.color.b * w;
                float a = scanline->v.color.a;
                int R = (int)(r * 255.0f);
                int G = (int)(g * 255.0f);
                int B = (int)(b * 255.0f);
                R = R*a + r2*(1 - a);
                G = G*a + g2*(1 - a);
                B = B*a + b2*(1 - a);
                R = fishMath::CMID(R, 0, 255);
                G = fishMath::CMID(G, 0, 255);
                B = fishMath::CMID(B, 0, 255);
                framebuffer[x] = (R << 16) | (G << 8) | (B);
            }
        }
        scanline->v = scanline->v + scanline->step;
        if (x >= width) break;
    }
}
//----------------------------------------------------------------------------
void fishDevice::deviceDrawScanlineViazbuffera(scanline_t *scanline)
{
    //通过深度缓存A画图
    IUINT32 *framebuffer = this->framebuffer[scanline->y];
    float *zbuffer = this->zbuffer[scanline->y];
    int x = scanline->x;
    int w = scanline->w;
    int width = this->width;

    for (; w > 0; x++, w--) {
        if (x >= 0 && x < width) {
            float rhw = scanline->v.rhw;
            //等于zbufferb 则画图
            if (abs(rhw - zbuffer[x])<0.000001f)
            {
                float w = 1.0f / rhw;
                IUINT32 colorx = framebuffer[x];
                int b2 = colorx & 255;
                int g2 = (colorx >> 8) & 255;
                int r2 = (colorx >> 16) & 255;
                float r = scanline->v.color.r * w;
                float g = scanline->v.color.g * w;
                float b = scanline->v.color.b * w;
                float a = scanline->v.color.a;
                int R = (int)(r * 255.0f);
                int G = (int)(g * 255.0f);
                int B = (int)(b * 255.0f);
                R = R*a + r2*(1 - a);
                G = G*a + g2*(1 - a);
                B = B*a + b2*(1 - a);
                R = fishMath::CMID(R, 0, 255);
                G = fishMath::CMID(G, 0, 255);
                B = fishMath::CMID(B, 0, 255);
                framebuffer[x] = (R << 16) | (G << 8) | (B);
            }
        }
        scanline->v = scanline->v + scanline->step;
        if (x >= width) break;
    }
}
//----------------------------------------------------------------------------
void fishDevice::initzbuffer()
{
    for (int i = 0; i < height; i++)
    {
        for (int j = 0; j < width; j++)
        {
            zbufferb[i][j] = -500.0f;
            zbufferc[i][j] = -500.0f;
            zbufferd[i][j] = -500.0f;
        }
    }
}
//----------------------------------------------------------------------------
//----------------------------------------------------------------------------


