#include "fishTransform.h"

fishTransform::fishTransform()
{
}

fishTransform::~fishTransform()
{
}

//矩阵更新，计算 transform = world * view * projection---------------------------------------
void fishTransform::transformUpdate()
{
    fishMatrix4x4 m;
    m = world*view;
    transform = m*projection;
}
//初始化，设置屏幕长宽------------------------------------------------------------------------
void fishTransform::transformInit(int width, int height)
{
    float aspect = (float)width / ((float)height);
    world.IdentityMatrix();
    view.IdentityMatrix();
    projection.PerspectiveMatrix(3.1415926f * 0.5f, aspect, 1.0f, 500.0f);
    w = (float)width;
    h = (float)height;
    transformUpdate();
}
//归一化，得到屏幕坐标------------------------------------------------------------------------
void fishTransform::transformHomogenize(fishVector4 &y, const fishVector4 &x) const
{
    float rhw = 1.0f / x.w;
    y.x = (x.x * rhw + 1.0f) * w * 0.5f;
    y.y = (1.0f - x.y * rhw) * h * 0.5f;
    y.z = x.z * rhw;
    y.w = 1.0f;
}

