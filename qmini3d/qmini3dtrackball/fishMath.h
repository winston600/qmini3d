#pragma once
#include "math.h"

class fishMath 
{

public:
    // Description:
    // Cross product of two 3-vectors. Result (a x b) is stored in z.
    static void Cross2(const float x[3], const float y[3], float z[3]);

    // Description:
    // Cross product of two 3-vectors. Result (a x b) is stored in z. (double-precision
    // version)
    static void Cross2(const double x[3], const double y[3], double z[3]);

    // Description:
    // Normalize (in place) a 3-vector. Returns norm of vector.
    static float Normalize(float x[3]);

    // Description:
    // Normalize (in place) a 3-vector. Returns norm of vector
    // (double-precision version).
    static double Normalize(double x[3]);

    // Description:
    // Compute the norm of 3-vector.
    static float Norm(const float x[3])
    {
        return static_cast<float> (sqrt(x[0] * x[0] + x[1] * x[1] + x[2] * x[2]));
    };

    // Description:
    // Compute the norm of 3-vector (double-precision version).
    static double Norm(const double x[3])
    {
        return sqrt(x[0] * x[0] + x[1] * x[1] + x[2] * x[2]);
    };

    //将x限制在范围min到max内
    static int CMID(int x, int min, int max) { return (x < min) ? min : ((x > max) ? max : x); }

    // 计算插值：t 为 [0, 1] 之间的数值
    static float interp(float x1, float x2, float t) { return x1 + (x2 - x1) * t; }
};

//----------------------------------------------------------------------------
// Cross product of two 3-vectors. Result (a x b) is stored in z[3].
inline void fishMath::Cross2(const float x[3], const float y[3], float z[3])
{
    float Zx = x[1] * y[2] - x[2] * y[1];
    float Zy = x[2] * y[0] - x[0] * y[2];
    float Zz = x[0] * y[1] - x[1] * y[0];
    z[0] = Zx; z[1] = Zy; z[2] = Zz;
}

//----------------------------------------------------------------------------
// Cross product of two 3-vectors. Result (a x b) is stored in z[3].
inline void fishMath::Cross2(const double x[3], const double y[3], double z[3])
{
    double Zx = x[1] * y[2] - x[2] * y[1];
    double Zy = x[2] * y[0] - x[0] * y[2];
    double Zz = x[0] * y[1] - x[1] * y[0];
    z[0] = Zx; z[1] = Zy; z[2] = Zz;
}

//----------------------------------------------------------------------------
inline float fishMath::Normalize(float x[3])
{
    float den;
    if ((den = fishMath::Norm(x)) != 0.0)
    {
        for (int i = 0; i < 3; i++)
        {
            x[i] /= den;
        }
    }
    return den;
}

//----------------------------------------------------------------------------
inline double fishMath::Normalize(double x[3])
{
    double den;
    if ((den = fishMath::Norm(x)) != 0.0)
    {
        for (int i = 0; i < 3; i++)
        {
            x[i] /= den;
        }
    }
    return den;
}



