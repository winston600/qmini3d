#pragma once
#include "fishVector4.h"

class fishMatrix4x4
{
public:
    float ele[16];
    /* 4x4矩阵
  ele[0]	ele[1]	ele[2]	ele[3]
  ele[4]	ele[5]	ele[6]	ele[7]
  ele[8]	ele[9]	ele[10]	ele[11]
  ele[12]	ele[13]	ele[14]	ele[15]
 */
    //构造函数
    fishMatrix4x4();
    fishMatrix4x4(const fishMatrix4x4 &m);
    fishMatrix4x4(float e0, float e1, float e2, float e3,
                  float e4, float e5, float e6, float e7,
                  float e8, float e9, float e10, float e11,
                  float e12, float e13, float e14, float e15);
    ~fishMatrix4x4();

    //重载“=”
    fishMatrix4x4& operator = (const fishMatrix4x4 &m);

    //重载自反运算符
    fishMatrix4x4& operator *= (const fishMatrix4x4 &);//与向量的乘法
    fishMatrix4x4& operator *= (float);//与标量相乘
    fishMatrix4x4& operator += (const fishMatrix4x4 &);
    fishMatrix4x4& operator -= (const fishMatrix4x4 &);

    //获取头指针
    operator const float* () const { return ele; }
    operator float* () { return ele; }

    void IdentityMatrix(); //单位矩阵
    void ZeroMatrix();     //零矩阵
    void Transpose();	   //转置
    float Inverse();       //逆

    void TranslateMatrix(float tx, float ty, float tz);       //平移
    void ScaleMatrix(float sx, float sy, float sz);           //缩放
    void RotateMatrix(float angle, float x, float y, float z);//旋转（轴角对转化为旋转矩阵）
    void PerspectiveMatrix(float fovy, float aspect, float zn, float zf);//投影矩阵（fovy：视场角，aspect:视口宽高比，zn:近裁剪平面到视点的距离，zf:远裁剪平面到视点的距离）
};
///////////////////////////////////////////////////////
//
//非成员函数
//
///////////////////////////////////////////////////////
inline fishMatrix4x4 operator * (const fishMatrix4x4 &A, const fishMatrix4x4 &B) {
    fishMatrix4x4 Res;
    Res.ele[0] = B.ele[0] * A.ele[0] + B.ele[4] * A.ele[1] + B.ele[8] * A.ele[2] + B.ele[12] * A.ele[3];
    Res.ele[1] = B.ele[1] * A.ele[0] + B.ele[5] * A.ele[1] + B.ele[9] * A.ele[2] + B.ele[13] * A.ele[3];
    Res.ele[2] = B.ele[2] * A.ele[0] + B.ele[6] * A.ele[1] + B.ele[10] * A.ele[2] + B.ele[14] * A.ele[3];
    Res.ele[3] = B.ele[3] * A.ele[0] + B.ele[7] * A.ele[1] + B.ele[11] * A.ele[2] + B.ele[15] * A.ele[3];

    Res.ele[4] = B.ele[0] * A.ele[4] + B.ele[4] * A.ele[5] + B.ele[8] * A.ele[6] + B.ele[12] * A.ele[7];
    Res.ele[5] = B.ele[1] * A.ele[4] + B.ele[5] * A.ele[5] + B.ele[9] * A.ele[6] + B.ele[13] * A.ele[7];
    Res.ele[6] = B.ele[2] * A.ele[4] + B.ele[6] * A.ele[5] + B.ele[10] * A.ele[6] + B.ele[14] * A.ele[7];
    Res.ele[7] = B.ele[3] * A.ele[4] + B.ele[7] * A.ele[5] + B.ele[11] * A.ele[6] + B.ele[15] * A.ele[7];

    Res.ele[8] = B.ele[0] * A.ele[8] + B.ele[4] * A.ele[9] + B.ele[8] * A.ele[10] + B.ele[12] * A.ele[11];
    Res.ele[9] = B.ele[1] * A.ele[8] + B.ele[5] * A.ele[9] + B.ele[9] * A.ele[10] + B.ele[13] * A.ele[11];
    Res.ele[10] = B.ele[2] * A.ele[8] + B.ele[6] * A.ele[9] + B.ele[10] * A.ele[10] + B.ele[14] * A.ele[11];
    Res.ele[11] = B.ele[3] * A.ele[8] + B.ele[7] * A.ele[9] + B.ele[11] * A.ele[10] + B.ele[15] * A.ele[11];

    Res.ele[12] = B.ele[0] * A.ele[12] + B.ele[4] * A.ele[13] + B.ele[8] * A.ele[14] + B.ele[12] * A.ele[15];
    Res.ele[13] = B.ele[1] * A.ele[12] + B.ele[5] * A.ele[13] + B.ele[9] * A.ele[14] + B.ele[13] * A.ele[15];
    Res.ele[14] = B.ele[2] * A.ele[12] + B.ele[6] * A.ele[13] + B.ele[10] * A.ele[14] + B.ele[14] * A.ele[15];
    Res.ele[15] = B.ele[3] * A.ele[12] + B.ele[7] * A.ele[13] + B.ele[11] * A.ele[14] + B.ele[15] * A.ele[15];
    return Res;
}
//----------------------------------------------------------------------------
inline fishMatrix4x4 operator * (const fishMatrix4x4 &A, float s) {
    fishMatrix4x4 Res;
    Res.ele[0] = A.ele[0] * s;
    Res.ele[1] = A.ele[1] * s;
    Res.ele[2] = A.ele[2] * s;
    Res.ele[3] = A.ele[3] * s;
    Res.ele[4] = A.ele[4] * s;
    Res.ele[5] = A.ele[5] * s;
    Res.ele[6] = A.ele[6] * s;
    Res.ele[7] = A.ele[7] * s;
    Res.ele[8] = A.ele[8] * s;
    Res.ele[9] = A.ele[9] * s;
    Res.ele[10] = A.ele[10] * s;
    Res.ele[11] = A.ele[11] * s;
    Res.ele[12] = A.ele[12] * s;
    Res.ele[13] = A.ele[13] * s;
    Res.ele[14] = A.ele[14] * s;
    Res.ele[15] = A.ele[15] * s;
    return Res;
}
//----------------------------------------------------------------------------
inline fishMatrix4x4 operator * (float s, const fishMatrix4x4 &A) {
    fishMatrix4x4 Res;
    Res.ele[0] = A.ele[0] * s;
    Res.ele[1] = A.ele[1] * s;
    Res.ele[2] = A.ele[2] * s;
    Res.ele[3] = A.ele[3] * s;
    Res.ele[4] = A.ele[4] * s;
    Res.ele[5] = A.ele[5] * s;
    Res.ele[6] = A.ele[6] * s;
    Res.ele[7] = A.ele[7] * s;
    Res.ele[8] = A.ele[8] * s;
    Res.ele[9] = A.ele[9] * s;
    Res.ele[10] = A.ele[10] * s;
    Res.ele[11] = A.ele[11] * s;
    Res.ele[12] = A.ele[12] * s;
    Res.ele[13] = A.ele[13] * s;
    Res.ele[14] = A.ele[14] * s;
    Res.ele[15] = A.ele[15] * s;
    return Res;
}
//----------------------------------------------------------------------------
inline fishMatrix4x4 operator + (const fishMatrix4x4 &A, const fishMatrix4x4 &B) {
    fishMatrix4x4 Res;
    Res.ele[0] = A.ele[0] + B.ele[0];
    Res.ele[1] = A.ele[1] + B.ele[1];
    Res.ele[2] = A.ele[2] + B.ele[2];
    Res.ele[3] = A.ele[3] + B.ele[3];
    Res.ele[4] = A.ele[4] + B.ele[4];
    Res.ele[5] = A.ele[5] + B.ele[5];
    Res.ele[6] = A.ele[6] + B.ele[6];
    Res.ele[7] = A.ele[7] + B.ele[7];
    Res.ele[8] = A.ele[8] + B.ele[8];
    Res.ele[9] = A.ele[9] + B.ele[9];
    Res.ele[10] = A.ele[10] + B.ele[10];
    Res.ele[11] = A.ele[11] + B.ele[11];
    Res.ele[12] = A.ele[12] + B.ele[12];
    Res.ele[13] = A.ele[13] + B.ele[13];
    Res.ele[14] = A.ele[14] + B.ele[14];
    Res.ele[15] = A.ele[15] + B.ele[15];
    return Res;
}
//----------------------------------------------------------------------------
inline fishMatrix4x4 operator - (const fishMatrix4x4 &A, const fishMatrix4x4 &B) {
    fishMatrix4x4 Res;
    Res.ele[0] = A.ele[0] - B.ele[0];
    Res.ele[1] = A.ele[1] - B.ele[1];
    Res.ele[2] = A.ele[2] - B.ele[2];
    Res.ele[3] = A.ele[3] - B.ele[3];
    Res.ele[4] = A.ele[4] - B.ele[4];
    Res.ele[5] = A.ele[5] - B.ele[5];
    Res.ele[6] = A.ele[6] - B.ele[6];
    Res.ele[7] = A.ele[7] - B.ele[7];
    Res.ele[8] = A.ele[8] - B.ele[8];
    Res.ele[9] = A.ele[9] - B.ele[9];
    Res.ele[10] = A.ele[10] - B.ele[10];
    Res.ele[11] = A.ele[11] - B.ele[11];
    Res.ele[12] = A.ele[12] - B.ele[12];
    Res.ele[13] = A.ele[13] - B.ele[13];
    Res.ele[14] = A.ele[14] - B.ele[14];
    Res.ele[15] = A.ele[15] - B.ele[15];
    return Res;
}
//----------------------------------------------------------------------------
inline fishMatrix4x4 operator - (const fishMatrix4x4 &A) {
    fishMatrix4x4 Res;
    Res.ele[0] = -A.ele[0];
    Res.ele[1] = -A.ele[1];
    Res.ele[2] = -A.ele[2];
    Res.ele[3] = -A.ele[3];
    Res.ele[4] = -A.ele[4];
    Res.ele[5] = -A.ele[5];
    Res.ele[6] = -A.ele[6];
    Res.ele[7] = -A.ele[7];
    Res.ele[8] = -A.ele[8];
    Res.ele[9] = -A.ele[9];
    Res.ele[10] = -A.ele[10];
    Res.ele[11] = -A.ele[11];
    Res.ele[12] = -A.ele[12];
    Res.ele[13] = -A.ele[13];
    Res.ele[14] = -A.ele[14];
    Res.ele[15] = -A.ele[15];
    return Res;
}
//左乘向量---------------------------------------------------------------------
inline fishVector4 operator * (const fishVector4& Vec, const fishMatrix4x4& Mat) {
    fishVector4 Res;
    Res.x = Mat.ele[0] * Vec.x + Mat.ele[4] * Vec.y + Mat.ele[8] * Vec.z + Mat.ele[12] * Vec.w;
    Res.y = Mat.ele[1] * Vec.x + Mat.ele[5] * Vec.y + Mat.ele[9] * Vec.z + Mat.ele[13] * Vec.w;
    Res.z = Mat.ele[2] * Vec.x + Mat.ele[6] * Vec.y + Mat.ele[10] * Vec.z + Mat.ele[14] * Vec.w;
    Res.w = Mat.ele[3] * Vec.x + Mat.ele[7] * Vec.y + Mat.ele[11] * Vec.z + Mat.ele[15] * Vec.w;
    return Res;
}
//右乘向量----------------------------------------------------------------------
inline fishVector4 operator * (const fishMatrix4x4& Mat, const fishVector4& Vec) {
    fishVector4 Res;
    Res.x = Mat.ele[0] * Vec.x + Mat.ele[1] * Vec.y + Mat.ele[2] * Vec.z + Mat.ele[3] * Vec.w;
    Res.y = Mat.ele[4] * Vec.x + Mat.ele[5] * Vec.y + Mat.ele[6] * Vec.z + Mat.ele[7] * Vec.w;
    Res.z = Mat.ele[8] * Vec.x + Mat.ele[9] * Vec.y + Mat.ele[10] * Vec.z + Mat.ele[11] * Vec.w;
    Res.w = Mat.ele[12] * Vec.x + Mat.ele[13] * Vec.y + Mat.ele[14] * Vec.z + Mat.ele[15] * Vec.w;
    return Res;
}

///////////////////////////////////////////////////////
//
//成员函数
//
///////////////////////////////////////////////////////

inline fishMatrix4x4& fishMatrix4x4::operator = (const fishMatrix4x4 &m){
    ele[0] = m.ele[0];
    ele[1] = m.ele[1];
    ele[2] = m.ele[2];
    ele[3] = m.ele[3];
    ele[4] = m.ele[4];
    ele[5] = m.ele[5];
    ele[6] = m.ele[6];
    ele[7] = m.ele[7];
    ele[8] = m.ele[8];
    ele[9] = m.ele[9];
    ele[10] = m.ele[10];
    ele[11] = m.ele[11];
    ele[12] = m.ele[12];
    ele[13] = m.ele[13];
    ele[14] = m.ele[14];
    ele[15] = m.ele[15];
    return *this;
}
//----------------------------------------------------------------------------
inline void fishMatrix4x4::IdentityMatrix(){
    ele[0] = ele[5] = ele[10] = ele[15] = 1.0f;
    ele[1] = ele[2] = ele[3] = ele[4] = 0.0f;
    ele[6] = ele[7] = ele[8] = ele[9] = 0.0f;
    ele[11] = ele[12] = ele[13] = ele[14] = 0.0f;
}
//----------------------------------------------------------------------------
inline void fishMatrix4x4::ZeroMatrix() {
    ele[0] = ele[5] = ele[10] = ele[15] = 0.0f;
    ele[1] = ele[2] = ele[3] = ele[4] = 0.0f;
    ele[6] = ele[7] = ele[8] = ele[9] = 0.0f;
    ele[11] = ele[12] = ele[13] = ele[14] = 0.0f;
}
//----------------------------------------------------------------------------
inline fishMatrix4x4& fishMatrix4x4::operator *= (const fishMatrix4x4 &B){
    *this = (*this)*B;
    return *this;
}
//----------------------------------------------------------------------------
inline fishMatrix4x4& fishMatrix4x4::operator *= (float s){
    ele[0] *= s;
    ele[1] *= s;
    ele[2] *= s;
    ele[3] *= s;
    ele[4] *= s;
    ele[5] *= s;
    ele[6] *= s;
    ele[7] *= s;
    ele[8] *= s;
    ele[9] *= s;
    ele[10] *= s;
    ele[11] *= s;
    ele[12] *= s;
    ele[13] *= s;
    ele[14] *= s;
    ele[15] *= s;
    return *this;
}
//----------------------------------------------------------------------------
inline fishMatrix4x4 & fishMatrix4x4::operator += (const fishMatrix4x4 &B){
    ele[0] += B.ele[0];
    ele[1] += B.ele[1];
    ele[2] += B.ele[2];
    ele[3] += B.ele[3];
    ele[4] += B.ele[4];
    ele[5] += B.ele[5];
    ele[6] += B.ele[6];
    ele[7] += B.ele[7];
    ele[8] += B.ele[8];
    ele[9] += B.ele[9];
    ele[10] += B.ele[10];
    ele[11] += B.ele[11];
    ele[12] += B.ele[12];
    ele[13] += B.ele[13];
    ele[14] += B.ele[14];
    ele[15] += B.ele[15];
    return *this;
}
//----------------------------------------------------------------------------
inline fishMatrix4x4 & fishMatrix4x4::operator -= (const fishMatrix4x4 &B){
    ele[0] -= B.ele[0];
    ele[1] -= B.ele[1];
    ele[2] -= B.ele[2];
    ele[3] -= B.ele[3];
    ele[4] -= B.ele[4];
    ele[5] -= B.ele[5];
    ele[6] -= B.ele[6];
    ele[7] -= B.ele[7];
    ele[8] -= B.ele[8];
    ele[9] -= B.ele[9];
    ele[10] -= B.ele[10];
    ele[11] -= B.ele[11];
    ele[12] -= B.ele[12];
    ele[13] -= B.ele[13];
    ele[14] -= B.ele[14];
    ele[15] -= B.ele[15];
    return *this;
}





