#pragma once
#include "fishVector4.h"


class fishCamera
{
public:
    fishCamera();
    ~fishCamera();

    fishVector4 uaxis, vaxis, naxis, eye, look, up;
    float getDist();

    //相机的六个自由度
    void roll(float angle);//相机roll
    void yaw(float angle);//相机yaw
    void pitch(float angle);//相机pitch
    void slide(float du, float dv, float dn);//相机平移
    void caluvn();// 根据eye, look, up重新计算uvn
};

