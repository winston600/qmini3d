#pragma once
#include "fishVertex.h"
#include "fishCube.h"

#include <fishIncludes.h>

using namespace std;

class fishLight
{
public:
    fishLight();
    ~fishLight();

    void SetLightpos(const fishPoint &lightpos_para);
    void calLight();//给mesh增加灯光效果，修改mesh的color

private: 
    fishPoint lightpos;  //光源位置
    fishPoint lightpos_norm;//光源位置归一化
};

