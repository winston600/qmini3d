#include "fishCamera.h"

#include <fishIncludes.h>

fishCamera::fishCamera()
{
    float pos = 3;//20190614 winston 控制大小

    eye.set(pos, 0, 0);
    look.set(0, 0, 0);
    up.set(0, 0, 1);

    uaxis.set(0, 1, 0);
    vaxis.set(0, 0, 1);
    naxis.set(-1, 0, 0);
}
//-----------------------------------------------
fishCamera::~fishCamera()
{
}
//-----------------------------------------------
void fishCamera::roll(float angle)
{
    float cs = cos(angle*PI / 180);
    float sn = sin(angle*PI / 180);
    fishVector4 t(uaxis);
    fishVector4 s(vaxis);
    uaxis.set(cs*t.x - sn*s.x, cs*t.y - sn*s.y, cs*t.z - sn*s.z);
    vaxis.set(sn*t.x + cs*s.x, sn*t.y + cs*s.y, sn*t.z + cs*s.z);
}
//-----------------------------------------------
void fishCamera::yaw(float angle)
{
    float cs = cos(angle*PI / 180);
    float sn = sin(angle*PI / 180);
    fishVector4 t(naxis);
    fishVector4 s(uaxis);
    naxis.set(cs*t.x - sn*s.x, cs*t.y - sn*s.y, cs*t.z - sn*s.z);
    uaxis.set(sn*t.x + cs*s.x, sn*t.y + cs*s.y, sn*t.z + cs*s.z);
}
//-----------------------------------------------
void fishCamera::pitch(float angle)
{
    float cs = cos(angle*PI / 180);
    float sn = sin(angle*PI / 180);
    fishVector4 t(vaxis);
    fishVector4 s(naxis);
    vaxis.set(cs*t.x - sn*s.x, cs*t.y - sn*s.y, cs*t.z - sn*s.z);
    naxis.set(sn*t.x + cs*s.x, sn*t.y + cs*s.y, sn*t.z + cs*s.z);
}
//摄像机绕三个轴平移的计算函数----------------------
void fishCamera::slide(float du, float dv, float dn)
{
    eye.x += du*uaxis.x + dv*vaxis.x + dn*naxis.x;
    eye.y += du*uaxis.y + dv*vaxis.y + dn*naxis.y;
    eye.z += du*uaxis.z + dv*vaxis.z + dn*naxis.z;
    look.x += du*uaxis.x + dv*vaxis.x + dn*naxis.x;
    look.y += du*uaxis.y + dv*vaxis.y + dn*naxis.y;
    look.z += du*uaxis.z + dv*vaxis.z + dn*naxis.z;
}
// 根据eye, look, up重新计算uvn--------------------
void fishCamera::caluvn()
{
    naxis = look - eye;
    naxis.normalize();
    uaxis = crossProduct(up, naxis);
    uaxis.normalize();
    vaxis = crossProduct(naxis, uaxis);
    uaxis.x = -uaxis.x;
    uaxis.y = -uaxis.y;
    uaxis.z = -uaxis.z;
}
//获取eye到坐标原点的距离--------------------------
float fishCamera::getDist()
{
    float dist = pow(eye.x, 2) + pow(eye.y, 2) + pow(eye.z, 2);
    return pow(dist, 0.5);
}
