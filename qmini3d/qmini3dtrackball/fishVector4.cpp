#include "fishVector4.h"

fishVector4::~fishVector4()
{
}

//置为零向量---------------------------------------------------------------------------
void fishVector4::zero()
{
    x = y = z = 0.0f;
}
//归一化---------------------------------------------------------------------------
void fishVector4::normalize()
{
    float magSq = x*x + y*y + z*z;
    if (magSq > 0.0f)
    {//检查除0
        float a = 1.0f / sqrtf(magSq);
        x *= a;
        y *= a;
        z *= a;
    }
};
//设置数值---------------------------------------------------------------------------
void fishVector4::set(float xaxis, float yaxis, float zaxis, float w_para )
{
    x = xaxis; y = yaxis; z = zaxis; w = w_para;
};
//重载“=”---------------------------------------------------------------------------
fishVector4& fishVector4::operator = (const fishVector4& A)
{
    x = A.x; y = A.y; z = A.z; w = A.w;
    return *this;
}
//重载一元“-”---------------------------------------------------------------------------
fishVector4 fishVector4::operator-() const
{
    return fishVector4(-x, -y, -z, w);
}
//重载“+”---------------------------------------------------------------------------
fishVector4 fishVector4::operator+(const fishVector4 &a) const
{
    return fishVector4(x + a.x, y + a.y, z + a.z, w);
}
//重载“-”---------------------------------------------------------------------------
fishVector4 fishVector4::operator-(const fishVector4 &a)const
{
    return fishVector4(x - a.x, y - a.y, z - a.z, w);
}
//重载“*”//点乘---------------------------------------------------------------------
float fishVector4::operator *(const fishVector4 &a) const
{
    return x*a.x + y*a.y + z*a.z;
}
//重载“==”---------------------------------------------------------------------------
bool fishVector4::operator ==(const fishVector4 &a)const
{
    return x == a.x&&y == a.y&&z == a.z&&w == a.w;
}
//重载“!=”---------------------------------------------------------------------------
bool fishVector4::operator !=(const fishVector4 &a) const
{
    return x != a.x || y != a.y || z != a.z || w != a.w;
}
//重载自反运算符---------------------------------------------------------------------------
fishVector4& fishVector4::operator +=(const fishVector4 &a)
{
    x += a.x; y += a.y; z += a.z;
    return *this;
}
//---------------------------------------------------------------------------------
fishVector4& fishVector4::operator -=(const fishVector4 &a)
{
    x -= a.x; y -= a.y; z -= a.z;
    return *this;
}
//--------------------------------------------------------------------------------
fishVector4& fishVector4::operator *=(float a)
{
    x *= a; y *= a; z *= a;
    return *this;
}
//----------------------------------------------------------------------------------
fishVector4& fishVector4::operator /=(float a)
{
    float oneOverA = 1.0f / a;
    x *= oneOverA; y *= oneOverA; z *= oneOverA;
    return *this;
}
//与标量的乘法------------------------------------------------------------------------
fishVector4 fishVector4::operator*(float a) const
{
    return fishVector4(x*a, y*a, z*a, w);
}
//与标量的除法-----------------------------------------------------------------------
fishVector4 fishVector4::operator/(float a) const
{
    float oneOverA = 1.0f / a;//这里没有对除以0进行检查
    return fishVector4(x*oneOverA, y*oneOverA, z*oneOverA, w);
}


