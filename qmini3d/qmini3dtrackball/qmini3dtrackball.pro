#-------------------------------------------------
#
# Project created by QtCreator 2020-04-20T14:56:11
#
#-------------------------------------------------

QT       += core gui

TARGET = qmini3dtrackball2
TEMPLATE = app


SOURCES += main.cpp\
    qmini3dgui.cpp \
    fishColor.cpp \
    fishMath.cpp \
    fishVector4.cpp \
    fishMatrix4x4.cpp \
    fishTransform.cpp \
    fishCamera.cpp \
    fishVertex.cpp \
    fishLight.cpp \
    fishDevice.cpp \
    fishWindowLabel.cpp \
    fishInteractorWidget.cpp \
    fishCube.cpp

HEADERS  += \
    qmini3dgui.h \
    fishIncludes.h \
    fishColor.h \
    fishMath.h \
    fishVector4.h \
    fishMatrix4x4.h \
    fishTransform.h \
    fishCamera.h \
    fishVertex.h \
    fishLight.h \
    fishDevice.h \
    fishWindowLabel.h \
    fishInteractorWidget.h \
    fishCube.h

FORMS    += \
    qmini3dgui.ui \
    fishInteractorWidget.ui

OTHER_FILES += \
    ��ע.txt
