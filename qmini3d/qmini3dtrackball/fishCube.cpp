#include "fishCube.h"


fishCube::fishCube(QObject *parent)
    : QObject(parent)
{
    //一个立方体
    //-----------------------------------------------
    fishVertex cube_mesh1;
    cube_mesh1.pos.x = 1;
    cube_mesh1.pos.y = -1;
    cube_mesh1.pos.z = 1;
    cube_mesh1.pos.w = 1;

    cube_mesh1.tc.u = 0;
    cube_mesh1.tc.v = 0;

    cube_mesh1.color.r = 1.0;
    cube_mesh1.color.g = 0.2;
    cube_mesh1.color.b = 0.2;
    cube_mesh1.color.a = 1;

    cube_mesh1.rhw = 1;

    cube_mesh.push_back(cube_mesh1);

    //-----------------------------------------------
    fishVertex cube_mesh2;
    cube_mesh2.pos.x = -1;
    cube_mesh2.pos.y = -1;
    cube_mesh2.pos.z = 1;
    cube_mesh2.pos.w = 1;

    cube_mesh2.tc.u = 0;
    cube_mesh2.tc.v = 1;

    cube_mesh2.color.r = 0.2;
    cube_mesh2.color.g = 1.0;
    cube_mesh2.color.b = 0.2;
    cube_mesh2.color.a = 1;

    cube_mesh2.rhw = 1;

    cube_mesh.push_back(cube_mesh2);
    //-----------------------------------------------
    fishVertex cube_mesh3;
    cube_mesh3.pos.x = -1;
    cube_mesh3.pos.y = 1;
    cube_mesh3.pos.z = 1;
    cube_mesh3.pos.w = 1;

    cube_mesh3.tc.u = 1;
    cube_mesh3.tc.v = 1;

    cube_mesh3.color.r = 0.2;
    cube_mesh3.color.g = 0.2;
    cube_mesh3.color.b = 1.0;
    cube_mesh3.color.a = 1;

    cube_mesh3.rhw = 1;

    cube_mesh.push_back(cube_mesh3);
    //-----------------------------------------------
    fishVertex cube_mesh4;
    cube_mesh4.pos.x = 1;
    cube_mesh4.pos.y = 1;
    cube_mesh4.pos.z = 1;
    cube_mesh4.pos.w = 1;

    cube_mesh4.tc.u = 1;
    cube_mesh4.tc.v = 0;

    cube_mesh4.color.r = 1.0;
    cube_mesh4.color.g = 0.2;
    cube_mesh4.color.b = 1.0;
    cube_mesh4.color.a = 1;

    cube_mesh4.rhw = 1;

    cube_mesh.push_back(cube_mesh4);
    //-----------------------------------------------
    fishVertex cube_mesh5;
    cube_mesh5.pos.x = 1;
    cube_mesh5.pos.y = -1;
    cube_mesh5.pos.z = -1;
    cube_mesh5.pos.w = 1;

    cube_mesh5.tc.u = 0;
    cube_mesh5.tc.v = 0;

    cube_mesh5.color.r = 1.0;
    cube_mesh5.color.g = 1.0;
    cube_mesh5.color.b = 0.2;
    cube_mesh5.color.a = 1;

    cube_mesh5.rhw = 1;

    cube_mesh.push_back(cube_mesh5);
    //-----------------------------------------------
    fishVertex cube_mesh6;
    cube_mesh6.pos.x = -1;
    cube_mesh6.pos.y = -1;
    cube_mesh6.pos.z = -1;
    cube_mesh6.pos.w = 1;

    cube_mesh6.tc.u = 0;
    cube_mesh6.tc.v = 1;

    cube_mesh6.color.r = 0.2;
    cube_mesh6.color.g = 1.0;
    cube_mesh6.color.b = 1.0;
    cube_mesh6.color.a = 1;

    cube_mesh6.rhw = 1;

    cube_mesh.push_back(cube_mesh6);
    //-----------------------------------------------
    fishVertex cube_mesh7;
    cube_mesh7.pos.x = -1;
    cube_mesh7.pos.y = 1;
    cube_mesh7.pos.z = -1;
    cube_mesh7.pos.w = 1;

    cube_mesh7.tc.u = 1;
    cube_mesh7.tc.v = 1;

    cube_mesh7.color.r = 1.0;
    cube_mesh7.color.g = 0.3;
    cube_mesh7.color.b = 0.3;
    cube_mesh7.color.a = 1;

    cube_mesh7.rhw = 1;
    cube_mesh.push_back(cube_mesh7);
    //-----------------------------------------------
    fishVertex cube_mesh8;
    cube_mesh8.pos.x = 1;
    cube_mesh8.pos.y = 1;
    cube_mesh8.pos.z = -1;
    cube_mesh8.pos.w = 1;

    cube_mesh8.tc.u = 1;
    cube_mesh8.tc.v = 0;

    cube_mesh8.color.r = 0.2;
    cube_mesh8.color.g = 1.0;
    cube_mesh8.color.b = 0.3;
    cube_mesh7.color.a = 1;

    cube_mesh8.rhw = 1;
    cube_mesh.push_back(cube_mesh8);
}
//-----------------------------------------------
fishCube::~fishCube()
{
}
//-----------------------------------------------
fishCube *fishCube::fish_cube = 0;
//-----------------------------------------------
fishCube *fishCube::GetfishCube()
{
    if (fish_cube == 0)
    {
        fish_cube = new fishCube();
        qDebug()<<"DataControl created";
    }
    return fish_cube;
}







