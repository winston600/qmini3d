#pragma once
#include "fishMatrix4x4.h"

class fishTransform
{
public:
    fishTransform();
    ~fishTransform();

    void transformUpdate();//矩阵更新，计算 transform = world * view * projection
    void transformInit(int width,int height);//初始化，设置屏幕长宽
    void transformHomogenize(fishVector4 &y,const fishVector4 &x) const;// 归一化，得到屏幕坐标


    fishMatrix4x4 world;         // 世界坐标变换
    fishMatrix4x4 view;          // 摄影机坐标变换
    fishMatrix4x4 projection;    // 投影变换
    fishMatrix4x4 transform;     // transform = world * view * projection
    float w, h;					 // 屏幕大小

};







