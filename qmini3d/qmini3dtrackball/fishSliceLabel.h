#pragma once
#include "CommonClass.h"
#include <qpainter.h>
#include "DataSource.h"


#include <QLabel>

class fishSliceLabel : public QLabel
{
	Q_OBJECT

public:
	fishSliceLabel(QObject *parent);
	~fishSliceLabel();

	int img_series;
	int img_slice;
	int img_width;
	int img_height;

protected:
	void paintEvent(QPaintEvent *event);
};
