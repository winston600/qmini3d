#include "fishColor.h"

fishColor::fishColor()
{
    r = 0;
    g = 0;
    b = 0;
    a = 1;
}


fishColor::~fishColor()
{
}

void fishColor::SetColor(float r_para, float g_para, float b_para, float a_para)
{
    r = r_para;
    g = g_para;
    b = b_para;
    a = a_para;
}
