#pragma

#include "fishIncludes.h"

//#include "fishCommonDefine.h"
#include "fishTransform.h"
#include "fishVertex.h"
#include "fishCamera.h"
#include "fishLight.h"

typedef unsigned int IUINT32;
//=====================================================================
// 渲染设备
//=====================================================================

class fishDevice
{
public:
    fishDevice();
    ~fishDevice();
    /*设备初始化*/
    void deviceInit(int width, int height, void *fb, int renderstate = RENDER_STATE_WIREFRAME);
    /*清空framebuffer和zbuffer 画背景*/
    void deviceClear(int mode);
    /*刷新显示*/
    void refreshDisplay();
    /*更改render_state*/
    void SetRenderstate(int renderstate) { render_state=renderstate; }
    /*相机*/
    fishCamera *camera;

private:
    /*绘制网格*/
    void drawCube();
    /*相机初始化*/
    void cameraAtZero();
    /*删除设备*/
    void deviceDestroy();
    /*画点*/
    void devicePixel(int x, int y, IUINT32 color);
    /*画线*/
    void deviceDrawLine(int x1, int y1, int x2, int y2, IUINT32 c);
    /*按照 Y 坐标计算出左右两条边纵坐标等于 Y 的顶点*/
    void trapezoidEdgeInterp(trapezoid_t *trap, float y);
    /*根据左右两边的端点，初始化计算出扫描线的起点和步长*/
    void trapezoidInitScanLine(const trapezoid_t *trap, scanline_t *scanline, int y);
    /*绘制扫描线*/
    void deviceDrawScanline(scanline_t *scanline);
    /*绘制扫描线_透明物体*/
    void deviceDrawTransparentScanline(scanline_t *scanline);
    /*绘制扫描线_写入zbufferA*/
    void deviceDrawScanlineZbuffera(scanline_t *scanline);
    /*绘制扫描线_写入zbufferB*/
    void deviceDrawScanlineZbufferb(scanline_t *scanline);
    /*绘制扫描线_写入zbufferC*/
    void deviceDrawScanlineZbufferc(scanline_t *scanline);
    /*绘制扫描线_写入zbufferD*/
    void deviceDrawScanlineZbufferd(scanline_t *scanline);
    /*绘制扫描线_按照zbufferD*/
    void deviceDrawScanlineViazbufferd(scanline_t *scanline);
    /*绘制扫描线_按照zbufferC*/
    void deviceDrawScanlineViazbufferc(scanline_t *scanline);
    /*绘制扫描线_按照zbufferB*/
    void deviceDrawScanlineViazbufferb(scanline_t *scanline);
    /*绘制扫描线_按照zbufferA*/
    void deviceDrawScanlineViazbuffera(scanline_t *scanline);


    /*根据坐标读取纹理*/
    IUINT32 deviceTextureRead(float u, float v)const;
    /*主渲染函数*/
    void deviceRenderTrap(trapezoid_t *trap);
    /*绘制面*/
    void drawPlane(int a, int b, int c, int d);

    /*绘制高亮框*/
    void drawHighlightplane(int a, int b, int c, int d,int flag);
    /*绘制原始三角形*/
    void deviceDrawPrimitive(fishVertex *v1, fishVertex *v2, fishVertex *v3);
    /*根据三角形生成 0 - 2 个梯形，并且返回合法梯形的数量*/
    int trapezoidInitTriangle(trapezoid_t *trap, fishVertex *p1,
                                fishVertex *p2, fishVertex *p3);
    /*设置当前纹理*/
    void deviceSetTexture(void *bits, long pitch, int w, int h);
    /*初始化纹理*/
    void initTexture();
    /*计算视点矩阵*/
    void calViewmatrix(fishMatrix4x4 &m, const fishCamera *camera_para);
    /*检查齐次坐标同 cvv 的边界用于视锥裁剪*/
    int transformCheckCvv(const fishVector4 &v);
    /* 初始化B C D缓存*/
    void initzbuffer();

    /*变量*/
    fishTransform transform;      // 坐标变换器
    int width;                  // 窗口宽度
    int height;                 // 窗口高度
    IUINT32 **framebuffer;      // 像素缓存：framebuffer[y] 代表第 y行
    float **zbuffer;            // 深度缓存：zbuffer[y] 为第 y行指针
    IUINT32 **texture;          // 纹理：同样是每行索引
    int tex_width;              // 纹理宽度
    int tex_height;             // 纹理高度
    float max_u;                // 纹理最大宽度：tex_width - 1
    float max_v;                // 纹理最大高度：tex_height - 1
    int render_state;           // 渲染状态
    IUINT32 background;         // 背景颜色
    IUINT32 foreground;         // 线框颜色   中心线
    IUINT32 modellines;          //模型线框颜色

    /*灯光*/
    fishLight *light;

    /*混合开关*/
    bool mixflag;

    /*深度缓存B*/
    float zbufferb[2048][2048];
    /*深度缓存C*/
    float zbufferc[2048][2048];
    /*深度缓存D*/
    float zbufferd[2048][2048];

};

