#include "fishVertex.h"
#include <qDebug.h>

fishVertex::fishVertex()
{
}
fishVertex::~fishVertex()
{
}
//---------------------------------------------------------------------------
void fishVertex::vertexRhwInit()
{
    float rhw2 = 1.0f / pos.w;
    rhw = rhw2;
    tc.u *= rhw2;
    tc.v *= rhw2;
    color.r *= rhw2;
    color.g *= rhw2;
    color.b *= rhw2;
    color.a = color.a;
}
//---------------------------------------------------------------------------
fishVertex fishVertex::operator +(const fishVertex &x)const
{
    fishVertex a;
    a.pos.x= pos.x + x.pos.x;
    a.pos.y= pos.y + x.pos.y;
    a.pos.z= pos.z + x.pos.z;
    a.pos.w= pos.w + x.pos.w;
    a.rhw  = rhw   + x.rhw;
    a.tc.u = tc.u  + x.tc.u;
    a.tc.v = tc.v  + x.tc.v;
    a.color.r = color.r + x.color.r;
    a.color.g = color.g + x.color.g;
    a.color.b = color.b + x.color.b;
    a.color.a = color.a>x.color.a? x.color.a: color.a;
    return a;
}
