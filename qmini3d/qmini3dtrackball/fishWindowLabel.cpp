#include "fishWindowLabel.h"


fishWindowLabel::fishWindowLabel(QWidget *parent)
    : QLabel(parent)
{
    std::cout<<"fishWindowLabel::fishWindowLabel"<<std::endl;
    //3d
    //开辟内存 存储图像指针

    screen_fb = new unsigned char[512 * 512 * 8];		// frame buffer
    memset(screen_fb, 0, 512 * 512 * 8);

    m_device = new fishDevice;

    img = new QImage(screen_fb, 512, 512, QImage::Format_RGB32);
    m_device->deviceInit( 512, 512, screen_fb, RENDER_STATE_COLOR);
}

fishWindowLabel::~fishWindowLabel()
{
    delete m_device;
}

void fishWindowLabel::paintEvent(QPaintEvent *event)
{
    m_device->deviceClear(1);
    m_device->refreshDisplay();
    //绘制图像
    QPainter painter(this);

    int img_width = img->width();
    int img_height = img->height();
    painter.drawPixmap(0, 0, img_width, img_height, QPixmap::fromImage(*img));

//    double height_label=this->height();
//    double width_label=this->width();

//    painter.drawPixmap(abs(height_label- width_label)/2, 0, min(height_label, width_label), min(height_label, width_label), QPixmap::fromImage(*img));

   // paintcolorbar(0);

    this->update();

}

void fishWindowLabel::paintcolorbar(bool type)
{
    QPainter painter(this);
    //------设置颜色条---------//
    //画colorbar
    QColor color;
    QRect section;
    float colorBarLength = 600.0;//设置颜色条的长度

    //---------设置边框--------------//
    //刻度值的绘制可以自己设计，使用drawText函数即可,刻度的绘制可以使用drawLine函数
    painter.setPen(Qt::white);

    //painter.drawRect(200, 50, 20, colorBarLength);
    painter.setFont(QFont(QString::fromLocal8Bit("arial"), 10, -1, true));
    painter.drawText(200, 80, QString("Radious"));

    //------设置为jet颜色条---------//
    float tempLength = colorBarLength / 4;

    for (int i = 0; i<tempLength; i++)// jet
    {
        double t = i / tempLength;
        double g = fishMath::interp(0.0f,1.0f,t);
        color.setRgbF(1.0f, g, 0.0f);
        section.setRect(200, colorBarLength + 100 - i * 1, 20, 1);
        painter.fillRect(section, color);
    }
    for (int i = tempLength; i<2*tempLength; i++)// jet
    {
        double t =( i- tempLength) / tempLength;
        double r = fishMath::interp(1.0f, 0.0f, t);
        color.setRgbF(r, 1.0f, 0.0f);
        section.setRect(200, colorBarLength + 100 - i * 1, 20, 1);
        painter.fillRect(section, color);
    }
    for (int i = 2*tempLength ; i<3*tempLength ; i++)// jet
    {
        double t = (i - 2*tempLength) / tempLength;
        double b = fishMath::interp(0.0f, 1.0f, t);
        color.setRgbF(0.0f,1.0f,b);
        section.setRect(200, colorBarLength + 100 - i * 1, 20, 1);
        painter.fillRect(section, color);
    }

    for (int i = 3*tempLength; i<4*tempLength; i++)// jet
    {
        double t = (i - 3 * tempLength) / tempLength;
        double g = fishMath::interp(1.0f, 0.0f, t);
        color.setRgbF(0.0f,g,1.0f);
        section.setRect(200, colorBarLength + 100 - i * 1, 20, 1);
        painter.fillRect(section, color);
    }

    if (type==0) {
//        float max , min, max_1 , max_2 , max_3 , max_4 ;
//        vector<double> dia = DataSource::GetDataSource()->GetVessel3DDiameter(DataSource::GetDataSource()->GetVessel3D());
//        max = *max_element(dia.begin(), dia.end());
//        min = *min_element(dia.begin(), dia.end());
//        max_1 = fishMath::interp(max, min, 0.2);
//        max_2 = fishMath::interp(max, min, 0.4);
//        max_3 = fishMath::interp(max, min, 0.6);
//        max_4 = fishMath::interp(max, min, 0.8);
//        int tmp1 = max * 1000;
//        int tmp2 = max_1 * 1000;
//        int tmp3 = max_2 * 1000;
//        int tmp4 = max_3 * 1000;
//        int tmp5 = max_4 * 1000;
//        int tmp6 = min * 1000;
//        max = float(tmp1) / 1000.0f;
//        max_1 = float(tmp2) / 1000.0f;
//        max_2 = float(tmp3) / 1000.0f;
//        max_3 = float(tmp4) / 1000.0f;
//        max_4 = float(tmp5) / 1000.0f;
//        min = float(tmp6) / 1000.0f;

//        painter.drawText(230, 110, QString::number(max));
//        painter.drawText(230, 110 + (colorBarLength - 10) / 5, QString::number(max_1));
//        painter.drawText(230, 110 + 2 * (colorBarLength - 10) / 5, QString::number(max_2));
//        painter.drawText(230, 110 + 3 * (colorBarLength - 10) / 5, QString::number(max_3));
//        painter.drawText(230, 110 + 4 * (colorBarLength - 10) / 5, QString::number(max_4));
//        painter.drawText(230, colorBarLength + 110, QString::number(min));
    }
    else if (type == 1) {
        painter.drawText(230, 110, "1.00");
        painter.drawText(230, 110 + (colorBarLength - 10) / 5, "0.95");
        painter.drawText(230, 110 + 2 * (colorBarLength - 10) / 5, "0.90");
        painter.drawText(230, 110 + 3 * (colorBarLength - 10) / 5, "0.85");
        painter.drawText(230, 110 + 4 * (colorBarLength - 10) / 5, "0.80");
        painter.drawText(230, colorBarLength + 110, "0.75");
    }



}










