#include <QtGui/QApplication>
#include "qmini3dgui.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    qmini3dgui w;
    w.show();

    return a.exec();
}
