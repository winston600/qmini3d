#pragma once
#include "fishVector4.h"



#include "fishMath.h"
#include "fishColor.h"

typedef struct { float u, v; } texcoord_t;//纹理坐标

class fishVertex
{
public:
    fishVertex();
    ~fishVertex();

    fishPoint pos;
    texcoord_t tc;
    fishColor color;
    float rhw;

    void vertexRhwInit();//顶点初始化
    fishVertex operator +(const fishVertex &x1) const;//重载“+”
}; 

///////////////////////////////////////////////////////
//
//非成员函数
//
///////////////////////////////////////////////////////
//顶点插值---------------------------------------------------------------------------
inline void vertexInterp(fishVertex &y,const fishVertex &x1, const fishVertex &x2, float t)
{
    vectorInterp(y.pos, x1.pos, x2.pos, t);
    y.tc.u = fishMath::interp(x1.tc.u, x2.tc.u, t);
    y.tc.v = fishMath::interp(x1.tc.v, x2.tc.v, t);
    y.color.r = fishMath::interp(x1.color.r, x2.color.r, t);
    y.color.g = fishMath::interp(x1.color.g, x2.color.g, t);
    y.color.b = fishMath::interp(x1.color.b, x2.color.b, t);
    y.color.a= fishMath::interp(x1.color.a, x2.color.a, t);
    y.rhw = fishMath::interp(x1.rhw, x2.rhw, t);
}
//---------------------------------------------------------------------------
inline void vertexDivision(fishVertex &y, const fishVertex &x1, const fishVertex &x2, float w)
{
    float inv = 1.0f / w;
    y.pos.x = (x2.pos.x - x1.pos.x) * inv;
    y.pos.y = (x2.pos.y - x1.pos.y) * inv;
    y.pos.z = (x2.pos.z - x1.pos.z) * inv;
    y.pos.w = (x2.pos.w - x1.pos.w) * inv;
    y.tc.u = (x2.tc.u - x1.tc.u) * inv;
    y.tc.v = (x2.tc.v - x1.tc.v) * inv;
    y.color.r = (x2.color.r - x1.color.r) * inv;
    y.color.g = (x2.color.g - x1.color.g) * inv;
    y.color.b = (x2.color.b - x1.color.b) * inv;
    y.color.a = x1.color.a;
    y.rhw = (x2.rhw - x1.rhw) * inv;
}
//---------------------------------------------------------------------------
typedef struct { fishVertex v, v1, v2; } edge_t;//边缘
typedef struct { float top, bottom; edge_t left, right; } trapezoid_t;//梯形
typedef struct { fishVertex v, step; int x, y, w; } scanline_t;//扫描线
