#include "fishLight.h"




fishLight::fishLight()
{
    lightpos.set(50, 0, 0);  //光源位置
    lightpos_norm = lightpos;//光源位置归一化
    lightpos_norm.normalize();
}
//-----------------------------------------------
fishLight::~fishLight()
{
}
//-----------------------------------------------
void fishLight::calLight()
{
    double AmbAmount = 0.5;//场景中环境光的强弱程度(0 to 1)
    double point_num = fishCube::GetfishCube()->cube_mesh.size();// WINSTON
    //清空所有的颜色
    //fishCube::GetfishCube()->Reset();

    //重新上色
    for (int i = 0; i < point_num - 3; i++)
    {
        //计算第（32*i+j）个点p0的灯光修正后的颜色
        //----------1.计算p0点的法向量--------------
        /* 网格三角形三个顶点
                     p0----p1
                     |
                     |
                     p2
                     */
        fishPoint p0, p1, p2;
        p0 = fishCube::GetfishCube()->cube_mesh.at(i).pos;
        p1 = fishCube::GetfishCube()->cube_mesh.at(i + 1).pos;
        p2 = fishCube::GetfishCube()->cube_mesh.at(i + 2).pos;

        //获取三角形顶点的法向量vec
        //两个向量叉乘并归一化
        fishVector4 vec0, vec1, vec;
        vec0 = p2 - p0;
        vec1 = p1 - p0;
        vec = crossProduct(vec0, vec1);
        vec.normalize();

        double c0, c1, c2, dist;//c0常数衰减因子,c1线性衰减因子,c2二次衰减因子,dist定点与光源的距离
        double  f;

        c0 = 0.65; c1 = 0.00002; c2 = 0.000001;
        dist = sqrt((p0.x - lightpos.x)*(p0.x - lightpos.x)
                    + (p0.y - lightpos.y)*(p0.y - lightpos.y)
                    + (p0.z - lightpos.z)*(p0.z - lightpos.z));
        f = 1.0 / (c0 + c1*dist + c2*dist * dist);
        f = f < 1.0f ? f : 1.0f;

        double coslight = lightpos_norm*vec;
        coslight = (coslight<0.0f) ? 0.0f : coslight;

        double r_c = fishCube::GetfishCube()->cube_mesh.at(i).color.r;
        double g_c = fishCube::GetfishCube()->cube_mesh.at(i + 1).color.g;
        double b_c = fishCube::GetfishCube()->cube_mesh.at(i + 2).color.b;

        //修正颜色
        r_c = r_c*AmbAmount + r_c*coslight*0.5;
        g_c = g_c*AmbAmount + g_c*coslight*0.5;
        b_c = b_c*AmbAmount + b_c*coslight*0.5;

        //限制在1.0以内
        r_c < 1.0f ? r_c : 1.0f;
        g_c < 1.0f ? g_c : 1.0f;
        b_c < 1.0f ? b_c : 1.0f;

        fishCube::GetfishCube()->cube_mesh.at(i + 2).color.r = r_c;
        fishCube::GetfishCube()->cube_mesh.at(i + 2).color.g = g_c;
        fishCube::GetfishCube()->cube_mesh.at(i + 2).color.b = b_c;

    }
}
//-----------------------------------------------
void fishLight::SetLightpos(const fishPoint &lightpos_para)
{
    lightpos = lightpos_para;
    lightpos_norm = lightpos_para;
    lightpos_norm.normalize();
}

